import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GeneralFirstPage } from './general-first';

@NgModule({
  declarations: [
    GeneralFirstPage,
  ],
  imports: [
    IonicPageModule.forChild(GeneralFirstPage),
  ],
})
export class GeneralFirstPageModule {}
