


import { AngularFireAuth } from 'angularfire2/auth';
import { GeneralHomePage } from './../general-home/general-home';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { NativeStorage } from '../../../node_modules/@ionic-native/native-storage';
import {Observable} from 'Rxjs/rx';
import { Subscription } from "rxjs/Subscription";
/**
 * Generated class for the GeneralFirstPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-general-first',
  templateUrl: 'general-first.html',
})
export class GeneralFirstPage implements OnInit {
  homeMap=GeneralHomePage
  name:string
  test:string;
  task:any
  observableVar: Subscription;
  loadMain:any
  regisDateTime;
  laseLoginDateTime;
  ln
  listBook = [];
  constructor(public navCtrl: NavController, 
              public navParams: NavParams , 
              public anth : AngularFireAuth,
              public db : AngularFireDatabase,
              public loadCtl : LoadingController,
              public screenOrientation : ScreenOrientation,
              public translate: TranslateService,
              public nativeStorage: NativeStorage) {
                
              this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);


  }


  ionViewDidEnter(){

    
    this.observableVar = Observable.interval(3000).subscribe(()=>{
                          this.nativeStorage.getItem('ln')
                .then(
                ((data) =>{
                 
                  if(data == "en"){

                  }else{
                  }
                }) 
                ,
            
                error => console.error(error)
                );
              });
              
  }

  ionViewDidLeave(){

    this.observableVar.unsubscribe();
  }
ngOnInit(){

  this.nativeStorage.getItem('ln')
  .then(
    ((data) => {

      if(data == "en"){
        this.loadMain = this.loadCtl.create({
          content: "Please wait..."
        })
        this.loadMain.present();
      }else{
        this.loadMain = this.loadCtl.create({
          content: "โปรดสักครู่..."
        })
        this.loadMain.present();
      }

    })
  ),error => console.error(error)


  this.anth.authState.subscribe(data=>{

    this.db.object('/data_user/'+data['uid']).valueChanges().subscribe(res=>{
      this.name = res['displayname']
      this.regisDateTime = this.Unix_timestamp(res['create']/1000000);
      this.laseLoginDateTime = this.Unix_timestamp(res['lastUsed']/1000000)
      if(this.name){
        this.loadMain.dismiss();
      }
    });

    this.db.object('/').valueChanges().subscribe(dataBook=>{
      this.listBook = [];
      var key = Object.keys(dataBook);
      for(var i = 0 ; i<key.length;i++){
        if(key[i] != "data" && key[i] != "data_user"){
          var keyZone = Object.keys(dataBook[key[i]]);
          for(var j = 0; j < keyZone.length;j++){
            if(keyZone[j]!= "A_Total Zone" &&  keyZone[j]!= "lat" &&  keyZone[j]!= "long" &&  keyZone[j]!= "nameEN" 
                &&  keyZone[j]!= "nameTH" &&  keyZone[j]!= "opening-time" &&  keyZone[j]!= "price-range"  &&  keyZone[j]!= "image-zone" ){

                  var keyLot = (Object.keys(dataBook[key[i]][keyZone[j]]))

                  for(var k = 0; k < keyLot.length; k++){
                    if(keyLot[k] != "A_Available" &&  keyLot[k] != "A_Total Lot" &&  keyLot[k] != "Name" &&  keyLot[k] != "URL"){
                      var checkKeyUid = ((Object.keys(dataBook[key[i]][keyZone[j]][keyLot[k]])))
                      for(var l = 0 ; l < checkKeyUid.length; l++){
                        if(checkKeyUid[l] == "uid"){
                          if(dataBook[key[i]][keyZone[j]][keyLot[k]]['uid'] == data['uid']){
                            var raw = {};
                            raw['nameTH'] = dataBook[key[i]]['nameTH'];
                            raw['nameEN'] = dataBook[key[i]]['nameEN'];
                            raw['startTime'] = dataBook[key[i]][keyZone[j]][keyLot[k]]['startDateTime']
                            raw['endTime'] = dataBook[key[i]][keyZone[j]][keyLot[k]]['endDateTime']
                            this.listBook.push(raw);

                          }
                        }
                      }
                    }
                  }
            }
          }
        }

      }

    })
   
  })

  
  
}

Unix_timestamp(t)
{
var dt = new Date(t*1000);

var dat = "0"+dt.getDate();
var mm = "0"+dt.getMonth();
var y = dt.getFullYear();

var hr = dt.getHours();
var m = "0" + dt.getMinutes();
var s = "0" + dt.getSeconds();
return dat.substr(-2) +'/' + mm.substr(-2) + '/' + y +' ' + hr+ ':' + m.substr(-2) + ':' + s.substr(-2);  
}


//  timeConverter(t) {     
//   var a = new Date(t * 1000);
//   var today = new Date();
//   var yesterday = new Date(Date.now() - 86400000);
//   var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
//   var year = a.getFullYear();
//   var month = months[a.getMonth()];
//   var date = a.getDate();
//   var hour = a.getHours();
//   var min = a.getMinutes();
//   if (a.setHours(0,0,0,0) == today.setHours(0,0,0,0))
//       return 'today, ' + hour + ':' + min;
//   else if (a.setHours(0,0,0,0) == yesterday.setHours(0,0,0,0))
//       return 'yesterday, ' + hour + ':' + min;
//   else if (year == today.getFullYear())
//       return date + ' ' + month + ', ' + hour + ':' + min;
//   else
//       return date + ' ' + month + ' ' + year + ', ' + hour + ':' + min;
// }
}
