import { NativeStorage } from '@ionic-native/native-storage';
import { AngularFireAuth } from 'angularfire2/auth';
import { GeneralZoneConfirmPage } from './../general-zone-confirm/general-zone-confirm';
import { AngularFireDatabase } from 'angularfire2/database';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, Content, Platform } from 'ionic-angular';
import { ScreenOrientation } from '@ionic-native/screen-orientation';


import * as d3 from 'd3-array';

import * as D3 from 'd3-selection';
import * as D3Zoom from 'd3-zoom';

import * as ss from 'd3-random';

/**
 * Generated class for the GeneralViewZonePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-general-view-zone',
  templateUrl: 'general-view-zone.html',
})
export class GeneralViewZonePage implements OnInit {

  title: string;
  nameMap: string;
  showName: string
  @ViewChild('canvas') canvasEl: ElementRef;
  @ViewChild(Content) content: Content

  private _CANVAS: any;

  private _CONTEXT: any;

  public load;

  ratio = 0;
  public listData = {};

  checkDraw = false
  uid;
  ln
  x = 0;
  y = 0;
  k = 1;

  checkLoadImg = false;

  canvasW
  canvasH
  screenW
  screenH
  imgPosX
  imgPosY

  imgH
  imgW

  checkFirstDraw = false;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public loadCtl: LoadingController,
    public alertCtl: AlertController,
    public afDb: AngularFireDatabase,
    public platform: Platform,
    public screenOrientation: ScreenOrientation,
    public anth: AngularFireAuth,
    public nativeStorage: NativeStorage) {


    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
  }



  dataManagement(data) {
    var keyMain = Object.keys(data);
    this.listData = {};
    var listRaw = [];
    this.listData = [];
    for (var i = 0; i < keyMain.length; i++) {
      if (keyMain[i] != 'A_Available' && keyMain[i] != 'A_Total Lot' && keyMain[i] != 'Name' && keyMain[i] != "nameTH" && keyMain[i] != "nameEN") {
        var dataDict = {};
        dataDict['nameCH'] = keyMain[i];
        dataDict['status'] = data[keyMain[i]]['Park Status']
        dataDict['priceStatus'] = data[keyMain[i]]['priceStatus']
        dataDict['uid'] = data[keyMain[i]]['uid']
        dataDict['statusReserve'] = data[keyMain[i]]['Reserve Status']
        dataDict['type'] = data[keyMain[i]]['Reserveable']
        dataDict['x'] = data[keyMain[i]]['PosX1']
        dataDict['y'] = data[keyMain[i]]['PosY1']
        dataDict['chh'] = data[keyMain[i]]['CHH']
        dataDict['chw'] = data[keyMain[i]]['CHW']
        dataDict['angle'] = data[keyMain[i]]['angle']

        dataDict['accessX1'] = data[keyMain[i]]['AccessX1']
        dataDict['accessY1'] = data[keyMain[i]]['AccessY1']
        dataDict['accessX2'] = data[keyMain[i]]['AccessX2']
        dataDict['accessY2'] = data[keyMain[i]]['AccessY2']


        listRaw.push(dataDict);
      }
    }
    this.listData['url'] = data['URL'];
    this.listData['data'] = listRaw;


    this._CANVAS = D3.select("canvas").call(D3Zoom.zoom().scaleExtent([0.5, 8]).on("zoom", () => {
      var transform = D3.event.transform;

      console.log(JSON.stringify(transform))
      this._CONTEXT.save();

      this.x = transform.x;
      this.y = transform.y;
      this.k = transform.k;

      this._CONTEXT.clearRect(0, 0, this.screenW, this.screenH);

      this._CONTEXT.translate(transform.x, transform.y);
      this._CONTEXT.scale(transform.k, transform.k);


      this.setupCanvas();
      this._CONTEXT.restore();

    }))

    if (this.title && this.listData) {

      if (this.checkDraw == false) {
        this.checkDraw = true;
        var img = new Image();
        img.onload = () => {
          console.log("HHHHHHH " + img.height + " WWWWW " + img.width)
        }
        img.src = this.listData['url'];


        this._CANVAS = this.canvasEl.nativeElement;
        var imageW = 1440;
        var imageH = 2940;

        this.screenW = this.platform.width();
        this.screenH = this.platform.height() - ((20 / 100) * this.platform.height());


        var n1 = this.getMax(this.getMax(imageW, imageH), this.getMax(this.screenW, this.screenH))
        var n2 = this.getMin(this.getMax(imageW, imageH), this.getMax(this.screenW, this.screenH))

        var ratioW = (n1) / (n2)


        var n2 = this.getMax(this.getMin(imageW, imageH), this.getMin(this.screenW, this.screenH))
        var n3 = this.getMin(this.getMin(imageW, imageH), this.getMin(this.screenW, this.screenH))

        var ratioH = (n2) / (n3)

        var imageScale = this.getMax(ratioW, ratioH);

        this.canvasW = imageW / imageScale
        this.canvasH = imageH / imageScale

        this._CANVAS.width = this.screenW;
        this._CANVAS.height = this.screenH;
        this._CANVAS.addEventListener("mousedown", () => {








          for (var i = 0, len = this.listData['data'].length; i < len; i++) {

            var w = (this.listData['data'][i].chw / (img.width / this.canvasW));
            var h = (this.listData['data'][i].chh / (img.height / this.canvasH));

            var x = (this.listData['data'][i].accessX1 / (img.width / this.canvasW));
            var y = (this.listData['data'][i].accessY1 / (img.height / this.canvasH));

            // var left = this.listData['data'][i].accessX1 , right = this.listData['data'][i].accessX1+this.listData['data'][i].chw;
            // var top = this.listData['data'][i].accessY1 , bottom = this.listData['data'][i].accessY1+this.listData['data'][i].chh;

            var left ,  right  , top , bottom
            left = x 
            right = x + w
            top = y
            bottom = y + h



          

            if(this.k == 1 && this.x == 0 &&  this.y == 0){
              left = left + this.imgPosX, 
              right = right + this.imgPosX;
  
              top = top + this.imgPosY, 
              bottom = bottom + this.imgPosY;
            }else if(this.k == 1 && this.x != 0 &&  this.y != 0){
              left = left + this.imgPosX + this.x, 
              right = right + this.imgPosX + this.x;
  
              top = top + this.imgPosY + this.y, 
              bottom = bottom + this.imgPosY + this.y;
            }else{

              
              left = ((left   +  this.imgPosX ) * this.k)  + this.x 
              right = ((right    + this.imgPosX) * this.k)  + this.x 
  
              top = ((top  +  this.imgPosY) * this.k) + this.y 
              bottom = ((bottom  +  this.imgPosY) * this.k) + this.y 
             
            } 
           

            if (right >= event['offsetX'] && left <= event['offsetX'] && bottom >= event['offsetY'] && top <= event['offsetY']) {


                // CH: this.listData['data'][i]['nameCH'],
                // nameParking: this.title,
                // status: this.listData['data'][i]['status'],
                // zone: this.nameMap

                // this.alertCtl.create({
                //         title: "Data",
                //         message: this.listData['data'][i]['nameCH']
                //       }).present();

              if (this.listData['data'][i]['status'] == 1 || this.listData['data'][i]['statusReserve'] == 1) {


                if (this.listData['data'][i]['uid'] != this.uid) {
                  this.alertCtl.create({
                    title: "จองไม่สำเร็จ",
                    message: "ช่องจอดรถไม่สามารถจองได้ เนื่องจากช่องจอดรถมีคนจองแล้ว"
                  }).present();
                } else {
                  if (this.listData['data'][i]['status'] != 1 && this.listData['data'][i]['priceStatus'] == 0 ) {
                    this.navCtrl.push(GeneralZoneConfirmPage,
                      {
                        CH: this.listData['data'][i]['nameCH'],
                        nameParking: this.title,
                        status: this.listData['data'][i]['status'],
                        zone: this.nameMap
                      })
                  }

                }

              } else {

                if (this.listData['data'][i]['type'] != 1) {
                  this.alertCtl.create({
                    title: "จองไม่สำเร็จ",
                    message: "ช่องจอดรถไม่สามารถจองได้ คุณสามารถจองได้เฉพาะช่องจอดที่มีสัญลักษณ์ R"
                  }).present();
                } else {
                  this.navCtrl.push(GeneralZoneConfirmPage,
                    {
                      CH: this.listData['data'][i]['nameCH'],
                      nameParking: this.title,
                      status: this.listData['data'][i]['status'],
                      zone: this.nameMap
                    })
                }

              }

              break;

            }
          }


        }, false)

        this.imgPosX = this._CANVAS.width / 2 - this.canvasW / 2;
        this.imgPosY = this._CANVAS.height / 2 - this.canvasH / 2;
      } else {
        this._CANVAS = this.canvasEl.nativeElement;
        var imageW = 1440;
        var imageH = 2940;

        this.screenW = this.platform.width();
        this.screenH = this.platform.height() - ((20 / 100) * this.platform.height());


        var n1 = this.getMax(this.getMax(imageW, imageH), this.getMax(this.screenW, this.screenH))
        var n2 = this.getMin(this.getMax(imageW, imageH), this.getMax(this.screenW, this.screenH))

        var ratioW = (n1) / (n2)


        var n2 = this.getMax(this.getMin(imageW, imageH), this.getMin(this.screenW, this.screenH))
        var n3 = this.getMin(this.getMin(imageW, imageH), this.getMin(this.screenW, this.screenH))

        var ratioH = (n2) / (n3)

        var imageScale = this.getMax(ratioW, ratioH);

        this.canvasW = imageW / imageScale
        this.canvasH = imageH / imageScale

        this._CANVAS.width = this.screenW;
        this._CANVAS.height = this.screenH;

        this.imgPosX = this._CANVAS.width / 2 - this.canvasW / 2;
        this.imgPosY = this._CANVAS.height / 2 - this.canvasH / 2;
        this.zoomReset();
      }


      // this.initialiseCanvas();

      this._CONTEXT = this._CANVAS.getContext('2d');


      this.setupCanvas();
    } else {
      this.alertCtl.create({
        title: "Error",
        message: "Error"
      }).present();
    }
  }
  ngOnInit() {

    this.nativeStorage.getItem('ln')
      .then(
        ((data) => {

          if (data == "en") {
            this.load = this.loadCtl.create({
              content: "Please wait..."
            })
            this.load.present();

            this.anth.authState.subscribe(data => {
              this.uid = data['uid']

            });

            this.title = this.navParams.get('nameZone');
            this.nameMap = this.navParams.get('nameMap');
            this.showName = this.navParams.get('showName')

            this.afDb.object('/' + this.nameMap + '/' + this.title).valueChanges().subscribe(res => {

              console.log(JSON.stringify(res))
              this.dataManagement(res)
              // this.zoomReset();

            });

          } else {
            this.load = this.loadCtl.create({
              content: "โปรดสักครู่..."
            })
            this.load.present();

            this.anth.authState.subscribe(data => {
              this.uid = data['uid']

            });

            this.title = this.navParams.get('nameZone');
            this.nameMap = this.navParams.get('nameMap');
            this.showName = this.navParams.get('showName')

            this.afDb.object('/' + this.nameMap + '/' + this.title).valueChanges().subscribe(res => {

              console.log(JSON.stringify(res))
              this.dataManagement(res)
              // this.zoomReset();

            });

          }

        })
      ), error => console.error(error)


  }
  initialiseCanvas(): void {
    if (this._CANVAS.getContext) {
      this.setupCanvas();
    }
  }

  getMax(n1: number, n2: number) {
    if (n1 >= n2) {
      return n1;
    } else {
      return n2;
    }

  }

  getMin(n1: number, n2: number) {
    if (n1 <= n2) {
      return n1;
    } else {
      return n2;
    }

  }

  zoomReset() {
    this._CONTEXT.save();
    D3.select("canvas").call(D3Zoom.zoom().transform, D3Zoom.zoomIdentity.translate(0, 0).scale(1))

    this._CONTEXT.clearRect(0, 0, this.screenW, this.screenH);
    this._CONTEXT.translate(0, 0);
    this._CONTEXT.scale(1, 1);
    this.x = 0;
    this.y = 0;
    this.k = 1;
    this.setupCanvas();
    this._CONTEXT.restore();
  }



  setupCanvas(): void {
    // this._CONTEXT = this._CANVAS.getContext('2d');

    this._CONTEXT.clearRect(0, 0, this._CANVAS.width, this._CANVAS.height);
    this._CONTEXT.fillStyle = "#3e3e3e";
    var img = new Image();
    img.src = this.listData['url'];



    if (this.checkFirstDraw == false) {
      img.onload = () => {
        this.imgH = img.height;
        this.imgW = img.width
        this._CONTEXT.drawImage(img, this.imgPosX, this.imgPosY, this.canvasW, this.canvasH);
        this.checkFirstDraw = true;

        this.drawRec();

      };
        

    } else {
      this._CONTEXT.drawImage(img, this.imgPosX, this.imgPosY, this.canvasW, this.canvasH);
      this.drawRec();
    }


    this.load.dismiss()


  }


  drawRec() {

    console.log("Draw Rec")
    this._CONTEXT.beginPath();
    this._CONTEXT.save();
    this._CONTEXT.translate(this.imgPosX, this.imgPosY)

    var redColor = "#ff001a";
    var greenColor = "#62ff5f";
    var or = "#f79036";

    for (var i = 0; i < this.listData['data'].length; i++) {



      var rawX = this.listData['data'][i]['x']
      var rawY = this.listData['data'][i]['y']
      var rawH = this.listData['data'][i]['chh']
      var rawW = this.listData['data'][i]['chw']

      var w = (rawW / (this.imgW / this.canvasW));
      var h = (rawH / (this.imgH / this.canvasH));

      var x = (rawX / (this.imgW / this.canvasW));
      var y = (rawY / (this.imgH / this.canvasH));

      var angle = this.listData['data'][i]['angle']
      var type = this.listData['data'][i]['type']

      if (this.listData['data'][i]['status'] == 1 || this.listData['data'][i]['statusReserve'] == 1) {


        this._CONTEXT.save();

        if (this.uid == this.listData['data'][i]['uid']) {
          this._CONTEXT.translate(x, y)
          this._CONTEXT.rotate(this.listData['data'][i]['angle'] * Math.PI / 180);
          this._CONTEXT.fillStyle = or;
          this._CONTEXT.fillRect(0, 0, w, h);


        } else {
          this._CONTEXT.translate(x, y)
          this._CONTEXT.rotate(this.listData['data'][i]['angle'] * Math.PI / 180);
          this._CONTEXT.fillStyle = redColor;
          this._CONTEXT.fillRect(0, 0, w, h);


        }


        if (type == 1) {
          this._CONTEXT.fillStyle = "#FFFFFF";
          this._CONTEXT.font = '15px serif';
          this._CONTEXT.fillText('R', (w / 2), (h / 2));


        }


        this._CONTEXT.restore();
      } else if (this.listData['data'][i]['status'] == 0 || this.listData['data'][i]['statusReserve'] == 0) {
        this._CONTEXT.save();

        this._CONTEXT.translate(x, y)
        this._CONTEXT.rotate(this.listData['data'][i]['angle'] * Math.PI / 180);
        this._CONTEXT.fillStyle = greenColor;
        this._CONTEXT.fillRect(0, 0, w, h);



        if (type == 1) {
          this._CONTEXT.fillStyle = "#FFFFFF";
          this._CONTEXT.font = '15px serif';
          this._CONTEXT.fillText('R', (w / 2), (h / 2));


        }

        this._CONTEXT.restore();
      }
    }

    this._CONTEXT.restore();

  }


  
}
