import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GeneralViewZonePage } from './general-view-zone';

@NgModule({
  declarations: [
    GeneralViewZonePage,
  ],
  imports: [
    IonicPageModule.forChild(GeneralViewZonePage),
  ],
})
export class GeneralViewZonePageModule {}
