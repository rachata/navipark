
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { AngularFireDatabase } from 'angularfire2/database';
import { AuthProvider } from './../../providers/auth/auth';
import { LoginPage } from './../login/login';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';

/**
 * Generated class for the AgreePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-agree',
  templateUrl: 'agree.html',
})
export class AgreePage implements OnInit {

  email:string;
  pass:string;
  dfname
  ddname
  dphone
  dcar
  dmodel

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public auth:AuthProvider,
              public alertCtl : AlertController,
              public loadCtl: LoadingController,
              public adb:AngularFireDatabase,
              public screenOrientation : ScreenOrientation) {

                this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);

  }

  ngOnInit(){
    this.email = this.navParams.get('email')
    this.pass = this.navParams.get('pass')
    this.dfname = this.navParams.get('dfname')
    this.ddname = this.navParams.get('ddname')
    this.dphone = this.navParams.get('dphone')

    this.dcar = this.navParams.get('dcar')
    this.dmodel = this.navParams.get('dModelCar')
  }

  onClick(op:string){
    if(op == "Disagree"){
      this.navCtrl.setRoot(LoginPage)
    }else{

          const load = this.loadCtl.create({
      content: 'Please wait...'
    })
    load.present();

    this.auth.signup(this.email , this.pass)
    .then(data=>{





      const itemsRef = this.adb.list('data_user');
      var start = Date.now();
      itemsRef.set(data.user.uid, { fullname: this.dfname , displayname : this.ddname , phone : this.dphone , car : this.dcar , carmodel : this.dmodel , create:start*1000});


      this.auth.sendmail().then(()=>{
        load.dismiss();
      })


      

      this.alertCtl.create({
        title : "Registration",
        message : "Please confirm your email address. Via email you registered"
      }).present();

      this.navCtrl.setRoot(LoginPage)

    })
    .catch(error=>{
    load.dismiss();
      const  alert = this.alertCtl.create({
      title: "Registration failed!!",
      message : error.message,
      buttons : ['Ok']
    })
    alert.present();
    })

    }

  }

}
