
import { GeneralMenuPage } from './../general-menu/general-menu';
import { AuthProvider } from './../../providers/auth/auth';
import { RegisterPage } from './../register/register';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, App, Platform } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { AngularFireAuth } from 'angularfire2/auth';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage  {

 



   
  
  regis  = RegisterPage
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public loadCtl: LoadingController,
              public alertCtl :AlertController,
              public app:App,
              public auth : AuthProvider,
              public screenOrientation:ScreenOrientation,
              public af : AngularFireAuth,
              public platform : Platform


             ) {

              this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
  }

  onLogin(from : NgForm){
    const load = this.loadCtl.create({
      content: 'Please wait...'
    })
    load.present();

    this.auth.signin(from.value.txtEmail , from.value.txtPass)
      
      .then(data=>{
        load.dismiss();

        if(data['user']['emailVerified'] == false){
          this.af.auth.signOut();
          this.alertCtl.create({
            title : "Login failed!!",
            message : "Please confirm your email address. Via email you registered"
          }).present();
        }else{
          this.app.getRootNav().setRoot(GeneralMenuPage);
        }


      })
      .catch(error=>{

        load.dismiss();
        const  alert = this.alertCtl.create({
          title: "Login failed!!",
          message : error.message + " Please check email and password.",
          buttons : ['Ok']
        })
        alert.present();
      })
  }

  forgotPass(){

    const alertRevoke = this.alertCtl.create({
      title:"Reset password",
      subTitle:"Please enter your email address.",
      inputs:[

        {
          name : 'txtEmail',
          placeholder : 'Email'

        },
    
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
          
        },
        {
          text:'OK',
          handler : data=>{
            const loadRevoke = this.loadCtl.create({
              content: "Please wait..."
            })
            loadRevoke.present();
            if(data.txtEmail.trim() === ""){
              const alert = this.alertCtl.create({
                title:"Reset failed!!",
                message:"Please enter full information.",
                buttons: ['OK']
              })
              alert.present();
              loadRevoke.dismiss();
            }else{
              var email:string = data.txtEmail;
              var passRevoke:string = data.txtPass;
       
              this.auth.forgot(email).then(()=>{
                this.alertCtl.create({
                  title : "Reset password",
                  message : "Please check email We send messages. Reset password"

                }).present();
                loadRevoke.dismiss();
              })
     

            }
          }

        }
      ]
    })
    alertRevoke.present();

  }



}
