import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GeneralHistoryPage } from './general-history';

@NgModule({
  declarations: [
    GeneralHistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(GeneralHistoryPage),
  ],
})
export class GeneralHistoryPageModule {}
