import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GeneralHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-general-history',
  templateUrl: 'general-history.html',
})
export class GeneralHistoryPage implements OnInit {

  uid:any
  listData = [];
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public db : AngularFireDatabase,
              public anth : AngularFireAuth,
              public screenOrientation : ScreenOrientation) {
                this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
  }



  ngOnInit(){
    this.anth.authState.subscribe(data=>{
      this.uid = data['uid']
      let a = this.db.object('/data_user/'+data['uid']+'/history').valueChanges().subscribe(res=>{
        this.listData = [];
       
        var key = Object.keys(res)
        if(key.length > 0){
          for(var i = 0; i<key.length; i++){
            var raw = {};
            raw['date'] = res[key[i]]['date']
            raw['ch'] = res[key[i]]['ch']
            raw['name'] = res[key[i]]['name']
            raw['zone'] = res[key[i]]['zone']
            raw['startDT'] = res[key[i]]['startDateTime']
            raw['endDT'] = res[key[i]]['endDateTime']
  
            this.listData.push(raw)
          }
          
        }else{
          a.unsubscribe();
          return;
        }
        a.unsubscribe();
        
        
      });
    })
  }
}
