import { AgreePage } from './../agree/agree';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { AngularFireDatabase } from 'angularfire2/database';
import { NgForm } from '@angular/forms';
import { AuthProvider } from './../../providers/auth/auth';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public auth:AuthProvider,
    public alertCtl : AlertController,
    public loadCtl: LoadingController,
    public adb:AngularFireDatabase,
    public screenOrientation : ScreenOrientation) {


    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
  }



onSignUp(from:NgForm){
if(from.value.txtPass == from.value.txtREPass){
  this.navCtrl.push(AgreePage , 
    { email : from.value.txtEmail , 
      pass : from.value.txtPass,
      dfname : from.value.txtFName,
      ddname : from.value.txtDName,
      dphone : from.value.txtPhone,
      dcar : from.value.txtDCarBrand,
      dModelCar : from.value.txtDCarModel
      
    
    })
}else{
  this.alertCtl.create({
    title : "Unsuccessful",
    message : "Please double check the passwords"
  }).present();
}
 
}

}
