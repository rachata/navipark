import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ScreenOrientation } from '../../../node_modules/@ionic-native/screen-orientation';
import { BarcodeScanner } from '@ionic-native/barcode-scanner'
import { NativeStorage } from '../../../node_modules/@ionic-native/native-storage';

/**
 * Generated class for the GeneralAccessToParkingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-general-access-to-parking',
  templateUrl: 'general-access-to-parking.html',
})
export class GeneralAccessToParkingPage implements OnInit {

  uid: string;
  arrayOfStrings
  ln
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public screenOrientation: ScreenOrientation,
    private alertCtrl: AlertController,
    private barcodeScanner: BarcodeScanner,
    public anth: AngularFireAuth,
    public db: AngularFireDatabase,
    public nativeStorage: NativeStorage
  ) {





  }


  ngOnInit() {
    this.anth.authState.subscribe(data => {
      this.uid = data['uid']
    })

    this.nativeStorage.getItem('ln')
      .then(
        ((data) => {

          if (data == "en") {
            this.ln = 'en'
          } else {
            this.ln = 'th'
          }
        })
        ,

        error => console.error(error)
      );
  }
  scanQRCode() {
    console.log("OK")

    this.barcodeScanner.scan().then(barcodeData => {
      console.log('Barcode data', JSON.stringify(barcodeData));

      this.arrayOfStrings = barcodeData['text'].split(';');

      let a = this.db.object('/' + this.arrayOfStrings[0] + '/' + this.arrayOfStrings[1] + '/' + this.arrayOfStrings[2]).valueChanges().subscribe(res => {

      
      if(res['priceStatus'] == 0){
        if (this.ln == 'th') {
          this.alertCtrl.create({
            title: "ผิดพลาด",
            message: "กรุณาจ่ายเงินก่อนเข้าที่จอดรถ"
          }).present();
        }else{
          this.alertCtrl.create({
            title: "Error",
            message: "Please pay before parking."
          }).present();
        }
      }else{
      // this.checkOpen(res);
      a.unsubscribe();
      }
       
       
      })
    }).catch(err => {
      console.log('Error', err);
    });
  }
  checkOpen(res) {



    if (this.ln == 'th') {
      if (this.uid == res['uid']) {
        this.db.object('/' + this.arrayOfStrings[0] + '/' + this.arrayOfStrings[1] + '/' + this.arrayOfStrings[2])
          .update({ 'Reserve Status': 0 })
          .then(() => {
            this.alertCtrl.create({
              title: "สำเร็จ",
              message: "ระบุตัวตนสำเร็จ ท่านสามารถเข้าที่จอดรถของท่านได้แล้ว"
            }).present();
          })
      } else {
        this.alertCtrl.create({
          title: "เกิดข้อผิดพลาด",
          message: "ระบุตัวตนไม่สำเร็จ ท่านไม่สามารถเข้าที่จองได้"
        }).present();
      }
    } else {
      if (this.uid == res['uid']) {
        this.db.object('/' + this.arrayOfStrings[0] + '/' + this.arrayOfStrings[1] + '/' + this.arrayOfStrings[2])
          .update({ 'Reserve Status': 0 })
          .then(() => {
            this.alertCtrl.create({
              title: "Success",
              message: "Identify Success You have access to your car park."
            }).present();
          })
      } else {
        this.alertCtrl.create({
          title: "Error",
          message: "Identity is not successful. You can not access the car park."
        }).present();
      }
    }



  }
}


