import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GeneralAccessToParkingPage } from './general-access-to-parking';

@NgModule({
  declarations: [
    GeneralAccessToParkingPage,
  ],
  imports: [
    IonicPageModule.forChild(GeneralAccessToParkingPage),
  ],
})
export class GeneralAccessToParkingPageModule {}
