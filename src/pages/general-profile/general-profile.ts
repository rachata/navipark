import { NativeStorage } from '@ionic-native/native-storage';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, Platform } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';

/**
 * Generated class for the GeneralProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-general-profile',
  templateUrl: 'general-profile.html',
})
export class GeneralProfilePage implements OnInit {
  editForm : FormGroup
  uid:string
  loadMain: any
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public db : AngularFireDatabase,
              public loadCtl : LoadingController,
              public anth : AngularFireAuth,
              public toastCtrl : ToastController,
              public screenOrientation : ScreenOrientation,
              public nativeStorage : NativeStorage,
              public plt : Platform ) {

                this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
  }

  ngOnInit(){

   

   
    this.editForm = new FormGroup({
      'fullname' : new FormControl(null , Validators.required),
      'displayname'  : new FormControl(null , Validators.required),
      'phone'  : new FormControl(null , Validators.required),
      'car' : new FormControl(null , Validators.required),
      'carmodel' : new FormControl(null , Validators.required)
    });


    
    this.anth.authState.subscribe(data=>{
      this.uid = data['uid']
      this.db.object('/data_user/'+data['uid']).valueChanges().subscribe(res=>{
        this.initForm(res);
        // if (this.plt.is('android')) {
        //   this.loadMain.dismiss();
        // }
        
      });
    })

    
  }

  private initForm(res:any){
    this.editForm = new FormGroup({
      'fullname' : new FormControl(res['fullname'] , Validators.required),
      'displayname'  : new FormControl(res['displayname'] , Validators.required),
      'phone'  : new FormControl(res['phone'] , Validators.required),
      'car' : new FormControl(res['car'] , Validators.required),
      'carmodel' : new FormControl(res['carmodel'] , Validators.required)
    });
  }

  onSubmit(){

    var fullname = this.editForm.value.fullname
    var displayname = this.editForm.value.displayname

    var phone = this.editForm.value.phone
    var car = this.editForm.value.car
    var carmodel = this.editForm.value.carmodel
    

    const itemsRef = this.db.list('data_user');
    itemsRef.set(this.uid, { fullname: fullname , displayname : displayname , phone : phone , car : car , carmodel : carmodel });

    let toast = this.toastCtrl.create({
      message: 'Record Success',
      duration: 1500,
      position: 'bottom'
    });
    toast.present();


  }


}
