import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GeneralProfilePage } from './general-profile';

@NgModule({
  declarations: [
    GeneralProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(GeneralProfilePage),
  ],
})
export class GeneralProfilePageModule {}
