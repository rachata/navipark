import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { GeneralPeviewParkingPage } from './../general-peview-parking/general-peview-parking';

import { GeneralStatusParkingPage } from './../general-status-parking/general-status-parking';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { SearchDataProvider } from './../../providers/search-data/search-data';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Toast, ToastController, AlertController } from 'ionic-angular';
import { SearchAllPage } from '../search-all/search-all';



@IonicPage()
@Component({
  selector: 'page-search-favorite',
  templateUrl: 'search-favorite.html',
})
export class SearchFavoritePage implements OnInit {

  searchTerm: string = '';
  items: any;
  uid:any
  dataF = [];

  constructor(public navCtrl: NavController, 
              public navParams: NavParams , 
              public dataService : SearchDataProvider,
              public db : AngularFireDatabase,
              public afAuth : AngularFireAuth,
              public toastCtrl : ToastController,
              public alertCtrl : AlertController,
              public screenOrientation : ScreenOrientation) {

                this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
  }

  onClickAdd(){
    this.navCtrl.push(SearchAllPage)
    
  }




  ngOnInit(){
    this.afAuth.authState.subscribe(data=>{
      
      this.uid = data.uid
      this.db.object('/data_user/'+data.uid+'/favorite').valueChanges().subscribe(res=>{


        if(res){
          this.dataF = [];
          var key = Object.keys(res);
          for(var i = 0; i< key.length;i++){
            var raw = {};
            raw['title'] = res[key[i]]['place']
            raw['icon'] = 'md-close';
            raw['key'] = key[i];
  
            this.dataF.push(raw);
          }
  
          if(this.dataF != []){
            this.setFilteredItems();
          }
        }
        
       

  
      })
    })
  }
  setFilteredItems() {



    this.items = this.dataService.filterItems(this.searchTerm , this.dataF);

  }

  itemSelected(item , event){
    this.navCtrl.push(GeneralPeviewParkingPage , {pos : item['title']})
    // this.navCtrl.push(GeneralStatusParkingPage , {pos : item['title']})
  }

  onClick(item , event){
    event.stopPropagation();
    const itemsRef = this.db.list('/data_user/'+this.uid+'/favorite');
    itemsRef.remove(item['key'])
    this.toastCtrl.create({
      message: 'ลบข้อมูลเรียบร้อย',
      duration: 2000,
      position: 'bottom'
    }).present();
  }

}
