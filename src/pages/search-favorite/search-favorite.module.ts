import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchFavoritePage } from './search-favorite';

@NgModule({
  declarations: [
    SearchFavoritePage,
  ],
  imports: [
    IonicPageModule.forChild(SearchFavoritePage),
  ],
})
export class SearchFavoritePageModule {}
