import { NativeStorage } from '@ionic-native/native-storage';
import { SearchDataProvider } from './../../providers/search-data/search-data';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { SelectFindComponent } from './../../components/select-find/select-find';
import { GeneralStatusParkingPage } from './../general-status-parking/general-status-parking';
import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, AlertController, LoadingController, PopoverController } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { GoogleMaps, GoogleMap, GoogleMapsEvent, GoogleMapOptions, LocationService, MyLocation, MyLocationOptions, Marker, LatLng, MarkerIcon } from '@ionic-native/google-maps';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';


@IonicPage()
@Component({
  selector: 'page-general-home',
  templateUrl: 'general-home.html',
})
export class GeneralHomePage {

  items: Observable<any[]>;
  data: object
  datam: string
  listData = [];

  statusZoom: boolean = false;

  map: GoogleMap;

  state: boolean = true

  loadMain: any

  stateMap: boolean = false;

  itemsOutput = [];
  itemAll = [];
  autocomplete: any
  search: string;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public afDb: AngularFireDatabase,
    public platfrom: Platform,
    public AlertCtl: AlertController,
    public zone: NgZone,
    public loadCtl: LoadingController,
    public popverCtrl: PopoverController,
    public screenOrientation: ScreenOrientation,
    public dataService: SearchDataProvider,
    public transition: TranslateService,
  public nativeStorage : NativeStorage) {

    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT
    );
    this.autocomplete = { input: '' };
  }
  ionViewDidLoad() {
    this.nativeStorage.getItem('ln')
    .then(
      ((data) => {

        if(data == "en"){
          this.loadMain = this.loadCtl.create({
            content: "Please wait..."
          })
          this.loadMain.present();
        }else{
          this.loadMain = this.loadCtl.create({
            content: "โปรดรอสักครู่..."
          })
          this.loadMain.present();
        }

      })
    ),error => console.error(error)


    // this.loadMain = this.loadCtl.create({
    //   content: "Please wait..."
    // })
    // this.loadMain.present();



    this.platfrom.ready().then(() => {
      this.afDb.object('/').valueChanges().subscribe(res => {
        this.data = res

        this.rawDataMaps(this.data)

      });

    }).catch((error) => {
      this.AlertCtl.create({
        title: "Error !!!",
        message: error + ""
      }).present()
    })

  }

  rawDataMaps(data) {
    this.listData = []
    if (this.stateMap == true) this.map.clear()
    var keyNameZone = Object.keys(data);
    for (var i = 0; i < keyNameZone.length; i++) {

      if (keyNameZone[i] != "data_user" && keyNameZone[i] != "data") {


        var resultMain = {};
        resultMain['name'] = keyNameZone[i];
        resultMain['lat'] = data[keyNameZone[i]]['lat']
        resultMain['lng'] = data[keyNameZone[i]]['long']
        resultMain['title'] = keyNameZone[i];


        var keyCheckCh = Object.keys(data[keyNameZone[i]]);

        var spaces = 0;
        var total = 0

        // เช็คจำนวนช่องว่าง
        for (var j = 0; j < keyCheckCh.length; j++) {

          if (keyCheckCh[j] != "A_Total Zone" && keyCheckCh[j] != "lat" && keyCheckCh[j] != "long" && keyCheckCh[j] != "image-zone" && keyCheckCh[j] != "opening-time" && keyCheckCh[j] != "price-range" && keyCheckCh[j] != "nameTH" && keyCheckCh[j] != "nameEN") {
            spaces += data[keyNameZone[i]][keyCheckCh[j]]['A_Available'];
            total += data[keyNameZone[i]][keyCheckCh[j]]['A_Total Lot'];
          }
        }

        resultMain['spaces'] = spaces
        resultMain['total'] = total
        this.listData.push(resultMain)
      }

    }


    this.loadMap();

  }

  updateSearchResults() {

    if (this.autocomplete.input == '') {
      this.itemsOutput = [];
      return;
    }
    this.itemsOutput = this.dataService.filterItems(this.autocomplete.input, this.listData);
    console.log(JSON.stringify(this.itemsOutput))
  }

  loadMap() {



    let mapOptions: GoogleMapOptions = {

      camera: {
        target: {
          lat: 13.764881,
          lng: 100.538298,



        },

        zoom: 6,
        tilt: 30

      },




    };
    if (this.stateMap == false) this.map = GoogleMaps.create('map_canvas', mapOptions);
    else {

      if (this.map.getCameraPosition().zoom <= 7) {
        this.addParking();
      } else {
        this.addParkingWithNaumber();
      }
    }


    this.map.on(GoogleMapsEvent.MAP_READY).subscribe(() => {
      this.loadMain.dismiss();
      this.stateMap = true

      this.map.on(GoogleMapsEvent.MAP_DRAG_END).subscribe(() => {

        console.log(this.map.getCameraPosition().zoom)
        if (this.map.getCameraPosition().zoom > 7 && this.statusZoom == false) {

          this.statusZoom = true
          this.map.clear()
          this.addParkingWithNaumber();
        } else if (this.map.getCameraPosition().zoom <= 7 && this.statusZoom == true) {

          this.statusZoom = false
          this.map.clear()
          this.addParking();
        }

      })
      this.addParking();
    })



  }

  addParking() {
    for (var i = 0; i < this.listData.length; i++) {

      var icon: MarkerIcon;
      if (this.listData[i].total == this.listData[i].spaces) {
        icon = {
          url: 'assets/imgs/marker-100.png',
          size: {
            width: 45,
            height: 40
          }
        };
      } else if (this.listData[i].spaces < this.listData[i].total && this.listData[i].spaces != 0) {
        icon = {
          url: 'assets/imgs/marker-1.png',
          size: {
            width: 45,
            height: 40
          }
        };
      } else if (this.listData[i].spaces == 0) {
        icon = {
          url: 'assets/imgs/marker-0.png',
          size: {
            width: 45,
            height: 40
          }
        };
      }

      let marker: Marker = this.map.addMarkerSync({
        icon: icon,
        position: {
          lat: this.listData[i].lat,
          lng: this.listData[i].lng
        }
      });
      marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe((data) => {

        this.checkNamePos(data[0].lat, data[0].lng)
      });
    }
  }


  addParkingWithNaumber() {
    for (var i = 0; i < this.listData.length; i++) {

      var icon: MarkerIcon;
      if (this.listData[i].spaces == 0) {
        icon = {
          url: 'assets/imgs/marker0.png',
          size: {
            width: 45,
            height: 40
          }
        };
      } else if (this.listData[i].spaces == 1) {
        icon = {
          url: 'assets/imgs/marker1.png',
          size: {
            width: 45,
            height: 40
          }
        };
      } else if (this.listData[i].spaces == 2) {
        icon = {
          url: 'assets/imgs/marker2.png',
          size: {
            width: 45,
            height: 40
          }
        };
      } else if (this.listData[i].spaces == 3) {
        icon = {
          url: 'assets/imgs/marker3.png',
          size: {
            width: 45,
            height: 40
          }
        };
      } else if (this.listData[i].spaces == 4) {
        icon = {
          url: 'assets/imgs/marker4.png',
          size: {
            width: 45,
            height: 40
          }
        };
      } else if (this.listData[i].spaces == 5) {
        icon = {
          url: 'assets/imgs/marker5.png',
          size: {
            width: 45,
            height: 40
          }
        };
      } else if (this.listData[i].spaces == 6) {
        icon = {
          url: 'assets/imgs/marker6.png',
          size: {
            width: 45,
            height: 40
          }
        };
      } else if (this.listData[i].spaces == 7) {
        icon = {
          url: 'assets/imgs/marker7.png',
          size: {
            width: 45,
            height: 40
          }
        };
      } else if (this.listData[i].spaces == 8) {
        icon = {
          url: 'assets/imgs/marker8.png',
          size: {
            width: 45,
            height: 40
          }
        };
      } else if (this.listData[i].spaces == 9) {
        icon = {
          url: 'assets/imgs/marker9.png',
          size: {
            width: 45,
            height: 40
          }
        };
      } else if (this.listData[i].spaces >= 10) {
        icon = {
          url: 'assets/imgs/marker10P.png',
          size: {
            width: 45,
            height: 40
          }
        };
      }

      let marker: Marker = this.map.addMarkerSync({
        icon: icon,
        position: {
          lat: this.listData[i].lat,
          lng: this.listData[i].lng
        }
      });
      marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe((data) => {

        this.checkNamePos(data[0].lat, data[0].lng)
      });
    }
  }


  checkNamePos(lat: number, lng: number) {

    for (var i = 0; i < this.listData.length; i++) {
      if (this.listData[i].lat == lat && this.listData[i].lng == lng) {

        this.navCtrl.push(GeneralStatusParkingPage, { pos: this.listData[i].name })
      }
    }

  }
  selectSearchResult(item) {
    this.navCtrl.push(GeneralStatusParkingPage, { pos: item['name'] })
    // console.log(JSON.stringify(item))
  }
  onShowOption(ev) {
    const popver = this.popverCtrl.create(
      SelectFindComponent
    )
    popver.present({ ev: ev });
  }

}
