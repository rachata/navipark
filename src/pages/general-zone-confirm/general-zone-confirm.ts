import { NativeStorage } from '@ionic-native/native-storage';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Keyboard } from 'ionic-angular';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { DatePicker } from '../../../node_modules/@ionic-native/date-picker';

/**
 * Generated class for the GeneralZoneConfirmPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-general-zone-confirm',
  templateUrl: 'general-zone-confirm.html',
})
export class GeneralZoneConfirmPage implements OnInit{

  myDateStart
  myDateEnd  

  ch:any;
  nameParking:any;
  status:any;
  zone:any
  uid:any
  showBtn:string;
  statusChange:boolean;

  price;
  priceShow;


  startShow = true;
  endShow = true;

  ln;
  checkPrice = true;

  priceRange
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public alertCtrl : AlertController,
              public db : AngularFireDatabase,
              public anth:AngularFireAuth,
              public screenOrientation : ScreenOrientation,
              public datePicker : DatePicker,
              public keyboard: Keyboard,
              public nativeStorage : NativeStorage) {
              this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
  }
  ngOnInit(){

    this.nativeStorage.getItem('ln')
    .then(
    ((data) =>{
     
      if(data == "en"){
        this.ln = "en"
      }else{
        this.ln = "th"
      }
    }) 
    ,

    error => console.error(error)
    );

   
    this.ch = this.navParams.get('CH')
    this.zone = this.navParams.get('nameParking')
    this.nameParking = this.navParams.get('zone')
    this.status = this.navParams.get('status');

    this.anth.authState.subscribe(data=>{
      this.uid = data['uid']


      this.db.object('/'+this.nameParking+'/'+this.zone+'/'+this.ch).valueChanges().subscribe(res => {

        

        if(res['uid'] == this.uid){

          let a = this.db.object('/'+this.nameParking).valueChanges().subscribe(resP => {

            this.priceRange = resP['price-range'];
            this.price = resP['price-range']
            this.endShow = false;
            this.startShow = false;
            this.showBtn = "Cancel Reservation"
            this.myDateStart = res['startDateTime']
            this.myDateEnd = res['endDateTime']
            this.statusChange = true;
            this.checkPrice = false;
            var checkStart = new Date(this.myDateStart)
            var checkEnd = new Date(this.myDateEnd)
        
            this.priceShow =  this.price * this.diff_hours(checkStart , checkEnd);
            a.unsubscribe();
          })

        

        }else{
          let a = this.db.object('/'+this.nameParking).valueChanges().subscribe(resP => {
            this.priceRange = resP['price-range'];
            this.price = resP['price-range']
            this.priceShow = this.price
            a.unsubscribe();
          })
          var date = new Date();

          var houreAdd1 = new Date();
          houreAdd1.setHours(houreAdd1.getHours() + 2);
         
          this.myDateStart =  date.getFullYear()+'/'+date.getMonth()+'/'+date.getDate()+' '+date.getHours()+':'+date.getMinutes()
          this.myDateEnd =  date.getFullYear()+'/'+date.getMonth()+'/'+date.getDate()+' '+date.getHours()+':'+date.getMinutes()
          this.showBtn = "Confirm"
          this.statusChange = false;
        }

      });
    })



  }

  onClick(){

    if(this.statusChange == false){
      const a = this.db.object('/'+this.nameParking+'/'+this.zone+'/'+this.ch).valueChanges().subscribe(res=>{
        if(res['Park Status'] == 1 || res['Reserve Status'] == 1){

          if(this.ln == "en"){
            this.alertCtrl.create({
              title : "Error",
              message : "Can not be booked There are people booking before. Please select a new parking area."
      
            }).present();
          }else{
            this.alertCtrl.create({
              title : "ผิดพลาด",
              message : "ไม่สามารถจองได้ เนื่องจากมีคนจองก่อนหน้าแล้ว โปรดเลือกช่องจอดใหม่"
      
            }).present();
          }
        
         this.navCtrl.pop();
         a.unsubscribe();
        }else{
          var checkStart = new Date(this.myDateStart)
          var checkEnd = new Date(this.myDateEnd)
      
          this.priceShow =  this.price * this.diff_hours(checkStart , checkEnd);

         this.db.object('/'+this.nameParking+'/'+this.zone+'/'+this.ch)

         .update({'Reserve Status' : 1 , 'uid' : this.uid , 'startDateTime' : this.myDateStart , 'endDateTime' : this.myDateEnd , 'priceStatus' : 0 }).then(()=>{
            const itemsRef = this.db.list('/data_user/'+this.uid+'/history');
            var myDate  = new Date().toLocaleString()
            itemsRef.push({name:this.nameParking , zone:this.zone , ch:this.ch , date:myDate , startDateTime:this.myDateStart , endDateTime:this.myDateEnd , 'priceStatus' : 0 , 'price' : this.priceRange , 'priceShow' : this.priceShow})
           .then((data)=>{


            this.db.object('/'+this.nameParking+'/'+this.zone+'/'+this.ch)
            .update({'idRefHis' : data.key})
            .then(()=>{

              if(this.ln =="en"){
                this.alertCtrl.create({
                  title : "Success",
                  message : "Successful reservation"
          
                }).present();
              }else{
                this.alertCtrl.create({
                  title : "สำเร็จ",
                  message : "จองสำเร็จ"
          
                }).present();
              }

              this.navCtrl.pop();
              this.statusChange = true;
              this.showBtn = "Cancel Reservation"
            })
           
             
           })
   
          
         }).catch((error)=>{
   
         });
         
         a.unsubscribe();
     
        }
       });
    }else{
      
      const a = this.db.object('/'+this.nameParking+'/'+this.zone+'/'+this.ch+'/Park Status').valueChanges().subscribe(res=>{
        if(res == 1){

          if(this.ln == "en"){
            this.alertCtrl.create({
              title : "Error",
              message : "Can not be booked Because people parked before. Please select a new parking area"
      
            }).present();
          }else{
            this.alertCtrl.create({
              title : "ผิดพลาด",
              message : "ไม่สามารถจองได้ เนื่องจากมีคนจอดอยู่ก่อนหน้าแล้ว โปรดเลือกช่องจอดใหม่"
      
            }).present();
          }

         this.navCtrl.pop();
         a.unsubscribe();
        }else{
         this.db.object('/'+this.nameParking+'/'+this.zone+'/'+this.ch)
         
         .update({'Reserve Status' : 0 })
          .then(()=>{
            this.db.object('/'+this.nameParking+'/'+this.zone+'/'+this.ch+'/startDateTime').remove()
            this.db.object('/'+this.nameParking+'/'+this.zone+'/'+this.ch+'/endDateTime').remove()
            this.db.object('/'+this.nameParking+'/'+this.zone+'/'+this.ch+'/uid').remove()
            this.db.object('/'+this.nameParking+'/'+this.zone+'/'+this.ch+'/priceStatus').remove()
            
            .then(()=>{
              let rm =  this.db.object('/'+this.nameParking+'/'+this.zone+'/'+this.ch+'/idRefHis').valueChanges().subscribe(resS=>{
                this.rmData(resS);
                rm.unsubscribe();
              })
            });

           
   
          
         }).catch((error)=>{
   
         });
         
         a.unsubscribe();
     
        }
       });
    }
    
  }

  rmData(data){

    this.db.object('/data_user'+'/'+this.uid+'/history/'+data).remove()
    .then(()=>{
      this.db.object('/'+this.nameParking+'/'+this.zone+'/'+this.ch+'/idRefHis').remove()
      .then(()=>{

        if(this.ln =="en"){
          this.alertCtrl.create({
            title : 'Success',
            message : "Cancel reservation"
  
          }).present();
        }else{
          this.alertCtrl.create({
            title : "สำเร็จ",
            message : "ยกเลิกการจองสำเร็จ"
  
          }).present();
        }
       
        this.navCtrl.pop();
        this.statusChange = false;
        this.showBtn = "Confirm"
      })
    })
  }


  openDatepicker(){


    this.keyboard.close();
    if(this.startShow == true){
      this.datePicker.show({
        date: new Date(),
        mode: 'datetime',
        // androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT,
        is24Hour: true
      }).then(
        date => {
          this.myDateStart= date.getFullYear()+'/'+date.getMonth()+'/'+date.getDate()+' '+date.getHours()+':'+date.getMinutes()},
        err => console.log('Error occurred while getting date: ', err)
      );
    }
   
  }

  openDatepickerEnd(){
    this.keyboard.close();
    if(this.endShow == true){
    this.datePicker.show({
      date: new Date(),
      mode: 'datetime',
      // androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT,
      is24Hour: true
    }).then(
      date => {
        this.myDateEnd= date.getFullYear()+'/'+date.getMonth()+'/'+date.getDate()+' '+date.getHours()+':'+date.getMinutes()},
      err => console.log('Error occurred while getting date: ', err)
    );
    }
   
  }

  setTime(){

    var date = new Date();
    var today = date.getFullYear()+'/'+date.getMonth()+'/'+date.getDate()+' '+date.getHours()+':'+date.getMinutes()

    var todays = new Date(today);
    var checkStart = new Date(this.myDateStart)
    var checkEnd = new Date(this.myDateEnd)

    this.priceShow =  this.price * this.diff_hours(checkStart , checkEnd);

    if(this.priceShow <=0){
      this.checkPrice = true
    }else{
      this.checkPrice = false
    }
  
  }

   diff_hours(dt2, dt1) {
  
    var diff =(dt2.getTime() - dt1.getTime()) / 1000;
    diff /= (60 * 60);
    return Math.abs(Math.round(diff));
  
  }

}
