import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GeneralZoneConfirmPage } from './general-zone-confirm';

@NgModule({
  declarations: [
    GeneralZoneConfirmPage,
  ],
  imports: [
    IonicPageModule.forChild(GeneralZoneConfirmPage),
  ],
})
export class GeneralZoneConfirmPageModule {}
