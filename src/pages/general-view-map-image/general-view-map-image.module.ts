import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GeneralViewMapImagePage } from './general-view-map-image';

@NgModule({
  declarations: [
    GeneralViewMapImagePage,
  ],
  imports: [
    IonicPageModule.forChild(GeneralViewMapImagePage),
  ],
})
export class GeneralViewMapImagePageModule {}
