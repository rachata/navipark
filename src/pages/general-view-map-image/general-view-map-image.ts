import { NativeStorage } from '@ionic-native/native-storage';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { AngularFireDatabase } from 'angularfire2/database';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';



import { Observable } from 'Rxjs/rx';
import { Subscription } from "rxjs/Subscription";
/**
 * Generated class for the GeneralViewMapImagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-general-view-map-image',
  templateUrl: 'general-view-map-image.html',
})
export class GeneralViewMapImagePage implements OnInit{
  dataReCheck;
  ln
  titleShow: string;
  observableVar: Subscription;
  title
  urlImage = [];
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public db: AngularFireDatabase,

    public screenOrientation: ScreenOrientation,
    public nativeStorage: NativeStorage) {

    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);

  }

  ionViewDidEnter(){
    this.observableVar = Observable.interval(2000).subscribe(()=>{
                          this.nativeStorage.getItem('ln')
                .then(
                ((data) =>{
                 
                  if(data == "en"){
                    this.titleShow = this.dataReCheck['nameEN']
                  }else{
                    this.titleShow = this.dataReCheck['nameTH']
                  }
                }) 
                ,
            
                error => console.error(error)
                );
              });
              
  }

  ionViewDidLeave(){

    this.observableVar.unsubscribe();
  }


  ngOnInit() {
    this.title = this.navParams.get('data');
    this.nativeStorage.getItem('ln')
      .then(
        ((data) => {

          if (data == "en") {
            this.ln = "en"
          } else {
            this.ln = "th"
          }
        })
        ,

        error => console.error(error)
      );

    this.db.object('/' + this.title).valueChanges().subscribe(resF => {
      this.dataReCheck = resF;
      var spaces = 0;
      this.urlImage = [];
      var keyMain = Object.keys(resF);

      if(this.ln == "en"){
      this.titleShow = resF['nameEN']
      }else{
        this.titleShow = resF['nameTH']
      }

      for(var i = 0; i< keyMain.length;i++){
        if (keyMain[i] != 'A_Total Zone' && keyMain[i] != 'lat' && keyMain[i] != 'long' && keyMain[i] != "image-zone" && keyMain[i] != "opening-time" && keyMain[i] != "price-range" && keyMain[i] != "nameTH" && keyMain[i] != "nameEN") {
          var raw = {};
          raw['url'] = resF[keyMain[i]]['URL'];
          raw['name']   = resF[keyMain[i]]['Name'];

          this.urlImage.push(raw)
        }

      }
     

    })
  }


}
