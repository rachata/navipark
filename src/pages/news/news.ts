import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { ScreenOrientation } from '@ionic-native/screen-orientation';

import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';


import { Observable } from 'Rxjs/rx';
import { Subscription } from "rxjs/Subscription";
import { NativeStorage } from '../../../node_modules/@ionic-native/native-storage';
@IonicPage()
@Component({
  selector: 'page-news',
  templateUrl: 'news.html',
})
export class NewsPage implements OnInit {

  listNews = [];
  observableVar: Subscription;
  ln: string;

  dataRecheck: any
  dataLn: string
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public loadCtl: LoadingController,
    private db: AngularFireDatabase,
    public screenOrientation: ScreenOrientation,
    public nativeStorage: NativeStorage,
    private iab: InAppBrowser) {

    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
  }

  ionViewDidEnter() {
    this.observableVar = Observable.interval(3000).subscribe(() => {
      this.nativeStorage.getItem('ln')
        .then(
          ((data) => {

            if (data == "en") {
              var key = Object.keys(this.dataRecheck);
              this.listNews = [];
              for (var i = 0; i < key.length; i++) {
                var rawNews = {};
                rawNews['title'] = this.dataRecheck[key[i]]['titleEN']
                rawNews['date'] = this.dataRecheck[key[i]]['date']
                rawNews['content'] = this.dataRecheck[key[i]]['contentEN']

                this.listNews.push(rawNews);
              }
            } else {
              var key = Object.keys(this.dataRecheck);
              this.listNews = [];
              for (var i = 0; i < key.length; i++) {
                var rawNews = {};
                rawNews['title'] = this.dataRecheck[key[i]]['titleTH']
                rawNews['date'] = this.dataRecheck[key[i]]['date']
                rawNews['content'] = this.dataRecheck[key[i]]['contentTH']
                this.listNews.push(rawNews);
              }
            }
          })
          ,

          error => console.error(error)
        );
    });

  }
  ionViewDidLeave() {

    this.observableVar.unsubscribe();
  }
  openUrl() {

    
      // const options: InAppBrowserOptions = {
      //   zoom: 'no',
      //   location: 'no',
      //   toolbar: 'no'
      // };
      const browser = this.iab.create('http://skt-check.com/checkout/','_self',{location: 'no', toolbar: 'no' ,  zoom: 'no'});
    
    // window.open('https://www.google.com', '_system');
    // return false;

  }
  ngOnInit() {
    var dataLn;
    var dataShow;
    this.nativeStorage.getItem('ln')
      .then(
        ((data) => {

          if (data == "en") {
            dataLn = "en"
            dataShow = "Please wait..."

            const load = this.loadCtl.create({
              content: dataShow
            })
            load.present();

            this.db.object('/data/news').valueChanges().subscribe(res => {
              this.showNews(res , dataLn);
              load.dismiss();
            });

          } else {
            dataLn = "th"
            dataShow = 'โปรดรอสักครู่...'

            const load = this.loadCtl.create({
              content: dataShow
            })
            load.present();

            this.db.object('/data/news').valueChanges().subscribe(res => {
              this.showNews(res , dataLn);
              load.dismiss();
            });
          }
        })
        ,

        error => console.error(error)
      );


  }
  showNews(res: any , setLn : string) {
    var key = Object.keys(res);
    this.dataRecheck = res;
    this.listNews = [];
    for (var i = 0; i < key.length; i++) {
      var rawNews = {};

      if (setLn == "en") {
        rawNews['title'] = res[key[i]]['titleEN']
        rawNews['date'] = res[key[i]]['date']
        rawNews['content'] = res[key[i]]['contentEN']
      } else {
        rawNews['title'] = res[key[i]]['titleTH']
        rawNews['date'] = res[key[i]]['date']
        rawNews['content'] = res[key[i]]['contentTH']
      }





      this.listNews.push(rawNews);
    }
  }





}
