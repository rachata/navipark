import { NativeStorage } from '@ionic-native/native-storage';
import { ScreenOrientation } from '@ionic-native/screen-orientation';


import { GeneralStatusParkingPage } from './../general-status-parking/general-status-parking';
import { AngularFireDatabase } from 'angularfire2/database';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, App } from 'ionic-angular';
import { LaunchNavigator } from '@ionic-native/launch-navigator';


import { Observable } from 'Rxjs/rx';
import { Subscription } from "rxjs/Subscription";
import { GeneralViewMapImagePage } from '../general-view-map-image/general-view-map-image';
/**
 * Generated class for the GeneralPeviewParkingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-general-peview-parking',
  templateUrl: 'general-peview-parking.html',
})
export class GeneralPeviewParkingPage implements OnInit {
  title: string
  data = {};

  viewStatus = GeneralStatusParkingPage;
  statuslot = true;
  titleShow: string;
  ln
  observableVar: Subscription;
  dataReCheck;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public db: AngularFireDatabase,
    public platform: Platform,
    private launchNavigator: LaunchNavigator,
    public screenOrientation: ScreenOrientation,
    public nativeStorage: NativeStorage) {

    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);

  }

  ionViewDidEnter(){
    this.observableVar = Observable.interval(2000).subscribe(()=>{
                          this.nativeStorage.getItem('ln')
                .then(
                ((data) =>{
                 
                  if(data == "en"){
                    this.titleShow = this.dataReCheck['nameEN']
                  }else{
                    this.titleShow = this.dataReCheck['nameTH']
                  }
                }) 
                ,
            
                error => console.error(error)
                );
              });
              
  }

  ionViewDidLeave(){

    this.observableVar.unsubscribe();
  }

  openImage(){
    this.navParams.get('pos');
    this.navCtrl.push(GeneralViewMapImagePage , {'data' : this.navParams.get('pos')})
  }

  ngOnInit() {
    this.title = this.navParams.get('pos');
    this.nativeStorage.getItem('ln')
      .then(
        ((data) => {

          if (data == "en") {
            this.ln = "en"
          } else {
            this.ln = "th"
          }
        })
        ,

        error => console.error(error)
      );

    this.db.object('/' + this.title).valueChanges().subscribe(resF => {
      this.dataReCheck = resF;
      var spaces = 0;
      var key = Object.keys(resF);

      if(this.ln == "en"){
      this.titleShow = resF['nameEN']
      }else{
        this.titleShow = resF['nameTH']
      }



      for (var j = 0; j < key.length; j++) {

        if (key[j] != "A_Total Zone" && key[j] != "lat" && key[j] != "long" && key[j] != "image-zone" && key[j] != "opening-time" && key[j] != "price-range" && key[j] != "nameTH" && key[j] != "nameEN") {
          spaces += resF[key[j]]['A_Available']
        }
      }

      this.data['price'] = resF['price-range']
      this.data['time'] = resF['opening-time']
      this.data['image'] = resF['image-zone']
      this.data['lat'] = resF['lat']
      this.data['lng'] = resF['long'];
      this.data['totalZone'] = resF['A_Total Zone']

      console.log(JSON.stringify(resF))

      if (spaces > 0) {
        this.data['spaces'] = spaces
        this.statuslot = true;
      } else {
        this.statuslot = false;
        // this.data['spaces'] = "ที่จอดรถเต็ม"
      }


    })
  }

  onClickShowMap() {
    this.launchNavigator.navigate([this.data['lat'], this.data['lng']]);;









   
  }



}
