import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GeneralPeviewParkingPage } from './general-peview-parking';

@NgModule({
  declarations: [
    GeneralPeviewParkingPage,
  ],
  imports: [
    IonicPageModule.forChild(GeneralPeviewParkingPage),
  ],
})
export class GeneralPeviewParkingPageModule {}
