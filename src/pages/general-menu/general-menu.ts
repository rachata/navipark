import { PaymentPage } from './../payment/payment';
import { NativeStorage } from '@ionic-native/native-storage';
import { LoginPage } from './../login/login';
import { AngularFireAuth } from 'angularfire2/auth';
import { GeneralContactPage } from './../general-contact/general-contact';
import { GeneralProfilePage } from './../general-profile/general-profile';
import { GeneralHomePage } from './../general-home/general-home';
import { GeneralFirstPage } from './../general-first/general-first';
import { GeneralHistoryPage } from './../general-history/general-history';
import { GeneralTabPage } from './../general-tab/general-tab';
import { Component, ViewChild, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams  , Nav} from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { GeneralAccessToParkingPage } from '../general-access-to-parking/general-access-to-parking';


export interface PageInterface {
  title: string;
  name:any;
  pageName: any;
  tabComponent?: any;
  index?: number;
  icon: string;
}

@IonicPage()
@Component({
  selector: 'page-general-menu',
  templateUrl: 'general-menu.html',
})
export class GeneralMenuPage implements OnInit{

  @ViewChild(Nav) nav: Nav;

  rootPage = GeneralTabPage;

  name;

  pages: PageInterface[] = [
    { title: 'HOME',  name: 'GeneralTabPage' , pageName: GeneralTabPage, tabComponent: GeneralFirstPage, index: 0, icon: 'ios-home-outline' },
    { title: 'PARKING', name: 'GeneralTabPage' ,pageName: GeneralTabPage, tabComponent: GeneralHomePage, index: 1, icon: 'ios-car-outline' },
    { title: 'PROFILE',name: 'GeneralTabPage' , pageName: GeneralProfilePage,  icon: 'ios-contact-outline' },
    { title: 'HISTORY', name: 'GeneralHistoryPage' , pageName: GeneralHistoryPage, icon: 'ios-list-box-outline' },
    { title: 'payment', name: 'PaymentPage' , pageName: PaymentPage, icon: 'logo-usd' },
    { title: 'CONTACT US', name: 'GeneralContactPage' , pageName: GeneralContactPage, icon: 'ios-mail-outline' },
    { title: 'Access To Parking', name: 'GeneralAccessToParkingPage' , pageName: GeneralAccessToParkingPage, icon: 'ios-mail-outline' },
  ];


  setEng(){
   

    this.nativeStorage.setItem('ln', 'en')
    .then(
    () =>  this.translate.use('en'),
    error => console.error('Error storing item', error)
    );

  }
  setThai(){
    
    this.nativeStorage.setItem('ln', 'th')
    .then(
    () => this.translate.use('th'),
    error => console.error('Error storing item', error)
    );
  }

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public anth : AngularFireAuth,
              public db : AngularFireDatabase,
            public  translate: TranslateService,
          public nativeStorage  : NativeStorage) {

       
  }


  ngOnInit(){
    this.anth.authState.subscribe(data=>{
                 
      this.db.object('/data_user/'+data['uid']+'/displayname').valueChanges().subscribe(res=>{
        this.name = res
      });
  })
  }



 openPage(page: PageInterface) {
    let params = {};
 

    if (page.index) {
      params = { tabIndex: page.index };
    }
 


    if (this.nav.getActiveChildNavs().length && page.index != undefined) {
      this.nav.getActiveChildNavs()[0].select(page.index);
    } else {


      
      
      this.nav.push(page.pageName , params);
      

      
    }
  }

  editProfile(){
    this.navCtrl.push(GeneralProfilePage)
  }
 
  isActive(page: PageInterface) {

    let childNav = this.nav.getActiveChildNavs()[0];;

 
    if (childNav) {
      if (childNav.getSelected() && childNav.getSelected().root === page.tabComponent) {
        return 'navbar';
      }
      return;
    }

    if (this.nav.getActive() && this.nav.getActive().name === page.name) {
      return 'narbar';
    }
    return;
  }

  logout(){
    this.anth.auth.signOut()
      .then(()=>{
        this.navCtrl.setRoot(LoginPage);
      }).catch((error)=>{
        this.navCtrl.setRoot(LoginPage);
      })
    console.log("Logout")
  }

}
