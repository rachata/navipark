import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GeneralMenuPage } from './general-menu';

@NgModule({
  declarations: [
    GeneralMenuPage,
  ],
  imports: [
    IonicPageModule.forChild(GeneralMenuPage),
  ],
})
export class GeneralMenuPageModule {}
