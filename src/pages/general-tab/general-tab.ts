import { GeneralHomePage } from './../general-home/general-home';
import { GeneralFirstPage } from './../general-first/general-first';
import { NewsPage } from './../news/news';
import { GeneralProfilePage } from './../general-profile/general-profile';
import { Component } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';
import { TranslateService } from '../../../node_modules/@ngx-translate/core';
import { SearchFavoritePage } from '../search-favorite/search-favorite';

/**
 * Generated class for the GeneralTabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-general-tab',
  templateUrl: 'general-tab.html',
})
export class GeneralTabPage {

  profile:any = GeneralProfilePage;
  news:any = NewsPage;
  home:any = GeneralFirstPage;
  map:any = GeneralHomePage
  fav:any = SearchFavoritePage
  homes:string;
  parkings:string;
  newss:string;

  myIndex : number
  constructor(navParams: NavParams , public translate: TranslateService) {



    this.myIndex = navParams.data.tabIndex || 0

  }


}
