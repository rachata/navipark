import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GeneralTabPage } from './general-tab';

@NgModule({
  declarations: [
    GeneralTabPage,
  ],
  imports: [
    IonicPageModule.forChild(GeneralTabPage),
  ],
})
export class GeneralTabPageModule {}
