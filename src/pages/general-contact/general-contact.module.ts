import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GeneralContactPage } from './general-contact';

@NgModule({
  declarations: [
    GeneralContactPage,
  ],
  imports: [
    IonicPageModule.forChild(GeneralContactPage),
  ],
})
export class GeneralContactPageModule {}
