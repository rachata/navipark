import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { AngularFireDatabase } from 'angularfire2/database';
import { NgForm } from '@angular/forms';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';

/**
 * Generated class for the GeneralContactPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-general-contact',
  templateUrl: 'general-contact.html',
})
export class GeneralContactPage {

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public db : AngularFireDatabase,
              public toastCtrl:ToastController,
              public alertCtrl : AlertController,
              public screenOrientation : ScreenOrientation) {

                this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GeneralContactPage');
  }
  submitContact(from : NgForm){
    var name = from.value.name;
    var subject = from.value.subject;
    var detail = from.value.detail;
    var email = from.value.email;
    var tel = from.value.tel;


    const itemsRef = this.db.list('/data/contact');
    itemsRef.push({name:name , subject:subject , detail:detail , email:email , tel:tel})

    from.reset();



    this.alertCtrl.create({
      title : "สำเร็จ",
      message : "ส่งข้อมูลให้ผู้บริการแล้ว"

    }).present();




    


    
  }

}
