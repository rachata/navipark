import { GeneralPeviewParkingPage } from './../general-peview-parking/general-peview-parking';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { SearchDataProvider } from './../../providers/search-data/search-data';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { ScreenOrientation } from '@ionic-native/screen-orientation';

/**
 * Generated class for the SearchAllPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search-all',
  templateUrl: 'search-all.html',
})
export class SearchAllPage implements OnInit{

  searchTerm: string = '';
  items = [];
  data = [];
  dataF = [];
  loadMain :any;
  uid :any;
  dataFull = [];
  constructor(public navCtrl: NavController, 
              public navParams: NavParams , 
              public dataService : SearchDataProvider,
              public db : AngularFireDatabase,
              public loadCtl:LoadingController,
              public afAuth:AngularFireAuth,
              public toastCtrl : ToastController,
              public screenOrientation : ScreenOrientation 

             ) {


              this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);



  }

  ngOnInit(){
    this.afAuth.authState.subscribe(data=>{
      this.uid = data.uid
    })
    this.db.object('/').valueChanges().subscribe(res=>{
      var key = Object.keys(res);
      this.data = [];
      for(var i = 0; i < key.length; i++){
        if(key[i] != "data_user" && key[i] != "data"){
          var raw = {};
          raw['title'] = res[key[i]]['nameTH']
          this.data.push(raw);
          
        }
      }



      if(this.data){

        this.db.object('/data_user/'+this.uid+'/favorite').valueChanges().subscribe(resF=>{
          this.dataF =[];
          this.dataFull = [];
          if(resF){
            var keyF = Object.keys(resF);

            var keyCheckF = []
            
            for(var j = 0 ; j<keyF.length; j++){
              var rawF = {};
              rawF['place'] = resF[keyF[j]]['place'];
              rawF['key'] = keyF[j];
              keyCheckF.push(rawF['place']);
              this.dataF.push(rawF)
            }
  
            
            for(var k = 0; k < key.length; k++){
              if(key[k] != "data" && key[k] != "data_user"){
                var a = keyCheckF.indexOf(key[k]);
                
                var rawFull  = {};
                if(a != -1){
                  
  
                  rawFull['title']= key[k] ;
                  rawFull['key'] = this.dataF[a]['key'];
                  rawFull['icon'] = 'md-star';
                  this.dataFull.push(rawFull)
                }else{
                  rawFull['title']= key[k] ;
                  rawFull['key'] = '-1';
                  rawFull['icon'] = 'md-star-outline';
  
                  this.dataFull.push(rawFull)
                  
                }
              }
            }
          }else{

            for(var u = 0; u< key.length; u++){
              if(key[u] != "data" && key[u] != "data_user"){
                var rawOnFavorite = {}
                rawOnFavorite['title']= key[u] ;
                rawOnFavorite['key'] = '-1';
                rawOnFavorite['icon'] = 'md-star-outline';
  
                this.dataFull.push(rawOnFavorite)
  
              }
              }
            
          }

          
          
          if(this.dataFull){

         
            this.setFilteredItems();
          }

        })

       

        
      }

    
    });
   

  }



  setFilteredItems() {

    this.items = this.dataService.filterItems(this.searchTerm , this.dataFull);

  }

  onClick(item , event){


      event.stopPropagation();

      if(item['key'] == '-1'){
        const itemsRef = this.db.list('/data_user/'+this.uid+'/favorite');
        itemsRef.push({place : item['title']})
    
     
    
        this.toastCtrl.create({
          message: 'บันทึกข้อมูลเรียบร้อย',
          duration: 2000,
          position: 'bottom'
        }).present();

      }else{
        const itemsRef = this.db.list('/data_user/'+this.uid+'/favorite');
        itemsRef.remove(item['key'])
        this.toastCtrl.create({
          message: 'ลบข้อมูลเรียบร้อย',
          duration: 2000,
          position: 'bottom'
        }).present();
      }
     

      // this.navCtrl.push(GeneralStatusParkingPage , {pos : this.listData[i].name})

  }

  itemSelected(item , $event){

    // this.navCtrl.push(GeneralStatusParkingPage , {pos : item['title']})

    this.navCtrl.push(GeneralPeviewParkingPage , {pos : item['title']})

  }
}
