import { NativeStorage } from '@ionic-native/native-storage';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { GeneralViewZonePage } from './../general-view-zone/general-view-zone';
import { AngularFireDatabase } from 'angularfire2/database';

import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Observable } from 'Rxjs/rx';
import { Subscription } from "rxjs/Subscription";

@IonicPage()
@Component({
  selector: 'page-general-status-parking',
  templateUrl: 'general-status-parking.html',
})
export class GeneralStatusParkingPage implements OnInit {


  loadProgress: any
  title: string;

  titleShow:string
  dataReCheck
  items: any
  numberOfZone

  listDataShowZone = {};
  lat;
  lng;
  nameZoneShow
  observableVar: Subscription;
  ln:string;
  itemSelected(item: number) {


    var space = this.listDataShowZone['data'][item]['availOfZone'];

    var nameZone = this.listDataShowZone['data'][item]['nameZone'];
    var nameZoneS = this.listDataShowZone['data'][item]['nameZoneShow'];
    console.log(nameZoneS)
    this.navCtrl.push(GeneralViewZonePage, { 'nameZone': nameZone, 'nameMap': this.title, 'showName': nameZoneS });


  }



  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public afDb: AngularFireDatabase,
    public alertCtl: AlertController,
    public screenOrientation: ScreenOrientation,
    private launchNavigator: LaunchNavigator,
    public nativeStorage: NativeStorage) {

    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
  }

  ionViewDidEnter(){
    this.observableVar = Observable.interval(2000).subscribe(()=>{
                          this.nativeStorage.getItem('ln')
                .then(
                ((data) =>{
                 
                  if(data == "en"){
                    this.titleShow = this.dataReCheck['nameEN']
                  }else{
                    this.titleShow = this.dataReCheck['nameTH']
                  }
                }) 
                ,
            
                error => console.error(error)
                );
              });
              
  }

  ionViewDidLeave(){

    this.observableVar.unsubscribe();
  }


  ngOnInit() {

    this.nativeStorage.getItem('ln')
    .then(
    ((data) =>{
     
      if(data == "en"){
        this.ln = "en"
      }else{
        this.ln = "th"
      }
    }) 
    ,

    error => console.error(error)
    );



    this.title = this.navParams.get('pos')

    this.afDb.object('/' + this.title).valueChanges().subscribe(res => {
      this.items = res;
      this.dataReCheck = res;

      if(this.ln =="en"){
        this.titleShow = res['nameEN']
      }else{
        this.titleShow = res['nameTH']
      }
      
      this.dataManagement(res);

    });


  }
  dataManagement(res: any) {
    this.listDataShowZone = {};
    this.listDataShowZone['total'] = res['A_Total Zone']
    var keyMain = Object.keys(res);

    this.lat = res['lat'];
    this.lng = res['long'];

    var listDataShow = []
    for (var i = 0; i < keyMain.length; i++) {
      if (keyMain[i] != 'A_Total Zone' && keyMain[i] != 'lat' && keyMain[i] != 'long' && keyMain[i] != "image-zone" && keyMain[i] != "opening-time" && keyMain[i] != "price-range" && keyMain[i] != "nameTH" && keyMain[i] != "nameEN") {
        var listAllZone = {};
        var nameZone = keyMain[i]
        var avail = res[keyMain[i]]['A_Available'];
        var total = res[keyMain[i]]['A_Total Lot'];
        var progress = ((total - avail) / total) * 100

        // if(progress == 0){
        //   progress = -1
        // }nameZone
        listAllZone['nameZone'] = nameZone
        listAllZone['nameZoneShow'] = res[keyMain[i]]['Name'];

        console.log("  " + res[keyMain[i]]['Name'])

        if (avail > 0) {
          listAllZone['availOfZone'] = avail
          listAllZone['count'] = true;
        } else {
          listAllZone['availOfZone'] = 0
          listAllZone['count'] = false;
        }

        listAllZone['totalOfZone'] = total
        listAllZone['progress'] = progress

        listDataShow.push(listAllZone)


      }
    }

    this.listDataShowZone['data'] = listDataShow
    this.numberOfZone = Object.keys(this.listDataShowZone['data']).length
  }


  gotoMap() {
    this.launchNavigator.navigate([this.lat, this.lng]);;
  }


}
