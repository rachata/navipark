import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GeneralStatusParkingPage } from './general-status-parking';

@NgModule({
  declarations: [
    GeneralStatusParkingPage,
  ],
  imports: [
    IonicPageModule.forChild(GeneralStatusParkingPage),
  ],
})
export class GeneralStatusParkingPageModule {}
