import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html',
})
export class PaymentPage {

  constructor(public navCtrl: NavController, public navParams: NavParams , public iab :InAppBrowser) {
  }

  ionViewDidLoad() {
    const browser = this.iab.create('http://192.168.1.104/checkout/index1.html','_self',{location: 'no', toolbar: 'no' ,  zoom: 'no'});
  }

  payments(){
    const browser = this.iab.create('http://192.168.1.104/checkout/index1.html','_self',{location: 'no', toolbar: 'no' ,  zoom: 'no'});
  }

}
