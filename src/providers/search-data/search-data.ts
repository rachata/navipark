import { AngularFireDatabase } from 'angularfire2/database';
import { Injectable } from '@angular/core';

/*
  Generated class for the SearchDataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SearchDataProvider {

  items = [];


  constructor(public db : AngularFireDatabase) {

  //   this.items = [
  //     {title: 'One'},
  //     {title: 'two'},
  //     {title: 'three'},
  //     {title: 'four'},
  //     {title: 'five'},
  //     {title: 'six'},
  //     {title: 'One'},
  //     {title: 'two'},
  //     {title: 'three'},
  //     {title: 'four'},
  //     {title: 'five'},
  //     {title: 'six'},
  //     {title: 'One'},
  //     {title: 'two'},
  //     {title: 'three'},
  //     {title: 'four'},
  //     {title: 'five'},
  //     {title: 'six'}
  // ]





  }
  filterItems(searchTerm , inputItem){

    this.items = inputItem
 
    return this.items.filter((item) => {
        return item.title.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });    

}

}
