import { ScreenOrientation } from '@ionic-native/screen-orientation';

class ScreenOrientationMock extends ScreenOrientation {
    lock(type) {
      return new Promise((resolve, reject) => {
        resolve("locked");
      })
    }
  }