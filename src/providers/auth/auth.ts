
import { Injectable } from '@angular/core';


import { AngularFireAuth} from 'angularfire2/auth'
/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {


  signup(email:string , pass:string){
    return this.afAuth.auth.createUserWithEmailAndPassword(email , pass);
    
    // return firebase.auth().createUserWithEmailAndPassword(email , pass);
  }

  signin(email:string , pass:string){

    return this.afAuth.auth.signInWithEmailAndPassword(email , pass);
    // return firebase.auth().signInWithEmailAndPassword(email, pass);
  }

  sendmail(){
    return this.afAuth.auth.currentUser.sendEmailVerification();

  }



  forgot(email:string){
    return this.afAuth.auth.sendPasswordResetEmail(email)
  }

  constructor(public afAuth : AngularFireAuth) {
    
  }

}
