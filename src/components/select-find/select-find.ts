import { SearchFavoritePage } from './../../pages/search-favorite/search-favorite';
import { SearchAllPage } from './../../pages/search-all/search-all';


import { Component } from '@angular/core';
import { ViewController, NavController, App } from 'ionic-angular';
/**
 * Generated class for the SelectFindComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */

@Component({
  selector: 'select-find',
  templateUrl: 'select-find.html'
})
export class SelectFindComponent {



  constructor(private viewCtrl : ViewController , public navCtrl:NavController , public app: App ) {
  }

  onAction(action:string){
    this.viewCtrl.dismiss({action:action})
    if(action == "all"){
      this.app.getActiveNav().push(SearchAllPage)
    
    }else{
      this.app.getActiveNav().push(SearchFavoritePage)
    }
  }

}
