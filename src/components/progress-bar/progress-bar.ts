import { Component, Input } from '@angular/core';

@Component({
  selector: 'progress-bar',
  templateUrl: 'progress-bar.html'
})
export class ProgressBarComponent {

  @Input('progress') progress;
  // @Input('cpro')  cpro;

  @Input('colorProgress') colorProgress

 
  constructor() {

  }

  setMyStyles(){
    var colorpro
    if(this.progress == 0){
      colorpro = "#64dd17"
    }else{
      colorpro = "#f44336"
    }
    let styles = {
      'width' : this.progress+'%',
      'background-color' : colorpro
    };
    return styles;
  }

}
