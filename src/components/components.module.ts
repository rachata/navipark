import { NgModule } from '@angular/core';
import { ProgressBarComponent } from './progress-bar/progress-bar';
import { SelectFindComponent } from './select-find/select-find';
import { ZoomAreaComponent } from './zoom-area/zoom-area';
@NgModule({
	declarations: [ProgressBarComponent,
    SelectFindComponent,
    SelectFindComponent,
    ZoomAreaComponent],
	imports: [],
	exports: [ProgressBarComponent,
    SelectFindComponent,
    SelectFindComponent,
    ZoomAreaComponent]
})
export class ComponentsModule {}
