import { AngularFireDatabase } from 'angularfire2/database';
import { NativeStorage } from '@ionic-native/native-storage';
import { GeneralMenuPage } from './../pages/general-menu/general-menu';
import { AngularFireAuth } from 'angularfire2/auth';
import { Component } from '@angular/core';
import { Platform, AlertController, LoadingController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


import { LoginPage } from '../pages/login/login';
import { TranslateService } from '../../node_modules/@ngx-translate/core';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;


  constructor(platform: Platform, 
              statusBar: StatusBar, 
              splashScreen: SplashScreen , 
              public afAuth: AngularFireAuth,
              public loading : LoadingController,
              public alertCtrl:AlertController,
              public platfrom:Platform,
              public translate: TranslateService,
              public nativeStorage:  NativeStorage,
              public db : AngularFireDatabase
   ) {
    platform.ready().then(() => {


      this.nativeStorage.getItem('ln')
       .then(
        data => this.translate.use(data),
       (error =>{
        
        this.nativeStorage.setItem('ln', 'en')
        .then(
          () => this.translate.use('en'),
          error => console.error('Error storing item', error)
        );
       }) 
  );

      
      
      const load = this.loading.create({
        content: 'Please wait...'
      })
      load.present();

     
        const a = this.afAuth.authState.subscribe(data=>{

          try {
            if(data.emailVerified == true){
              this.rootPage = GeneralMenuPage
              // this.fcm.subscribeToTopic(data['uid']);
              console.log(data['uid'])
              load.dismiss();
              a.unsubscribe();
              var start = Date.now();
              this.db.object('/data_user/'+data['uid']).update({'lastUsed' : start*1000 })
              

                 
            }else{
                this.rootPage = LoginPage
                load.dismiss();
                a.unsubscribe();
            }
          } catch (error) {
            this.rootPage = LoginPage
            load.dismiss();
            a.unsubscribe();
          }

         
        })
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }


}

