import { GeneralAccessToParkingPage } from './../pages/general-access-to-parking/general-access-to-parking';
import { GeneralViewMapImagePage } from './../pages/general-view-map-image/general-view-map-image';
import { AgreePage } from './../pages/agree/agree';
import { GeneralZoneConfirmPage } from './../pages/general-zone-confirm/general-zone-confirm';
import { SearchFavoritePage } from './../pages/search-favorite/search-favorite';
import { GeneralHistoryPage } from './../pages/general-history/general-history';
import { GeneralMenuPage } from './../pages/general-menu/general-menu';

import { GeneralProfilePage } from './../pages/general-profile/general-profile';
import { GeneralTabPage } from './../pages/general-tab/general-tab';
import { NewsPage } from './../pages/news/news';
import { GeneralFirstPage } from './../pages/general-first/general-first';
import { GeneralViewZonePage } from './../pages/general-view-zone/general-view-zone';
import { ProgressBarComponent } from './../components/progress-bar/progress-bar';
import { GeneralHomePage } from './../pages/general-home/general-home';
import { RegisterPage } from './../pages/register/register';
import { LoginPage } from './../pages/login/login';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';

import { AuthProvider } from '../providers/auth/auth';

import {  AngularFireModule } from 'angularfire2'
import { AngularFireAuthModule} from 'angularfire2/auth'
import { AngularFireDatabaseModule} from 'angularfire2/database'
import {AngularFirestoreModule } from 'angularfire2/firestore'
import { GeneralStatusParkingPage } from '../pages/general-status-parking/general-status-parking';


import { SelectFindComponent } from '../components/select-find/select-find';

import { ZoomAreaProvider } from '../providers/zoom-area/zoom-area';
import { ZoomAreaComponent } from '../components/zoom-area/zoom-area';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { SearchAllPage } from '../pages/search-all/search-all';
import { SearchDataProvider } from '../providers/search-data/search-data';
import { GeneralContactPage } from '../pages/general-contact/general-contact';
import { GeneralPeviewParkingPage } from '../pages/general-peview-parking/general-peview-parking';

import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';


export const FIREBASE_CONFIG = {
  apiKey: "AIzaSyDoK7hK5mhl88jRhVMqjG2YCpWs1nvNlig",
  authDomain: "parking-intelligent.firebaseapp.com",
  databaseURL: "https://parking-intelligent.firebaseio.com",
  projectId: "parking-intelligent",
  storageBucket: "parking-intelligent.appspot.com",
  messagingSenderId: "140803614618"
};


import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

import { InAppBrowser } from '@ionic-native/in-app-browser';

import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { SettingPage } from '../pages/setting/setting';

import { NativeStorage } from '@ionic-native/native-storage';

import { DatePicker } from '@ionic-native/date-picker'

import { PaymentPage } from '../pages/payment/payment';

class ScreenOrientationMock extends ScreenOrientation {
  lock(type) {
    return new Promise((resolve, reject) => {
      resolve("locked");
    })
  }
}


@NgModule({
  declarations: [
    GeneralTabPage,
    MyApp,
    LoginPage,
    RegisterPage,
    GeneralHomePage,
    GeneralStatusParkingPage,
    ProgressBarComponent,
    GeneralViewZonePage,

    GeneralFirstPage,
    NewsPage,
    GeneralHomePage,
    
    GeneralProfilePage,
    SelectFindComponent,
    ZoomAreaComponent,
    GeneralMenuPage,
    GeneralHistoryPage,
    SearchAllPage,
    SearchFavoritePage,
    GeneralContactPage,
    GeneralPeviewParkingPage,
    GeneralZoneConfirmPage,
    AgreePage,
    SettingPage,

    GeneralViewMapImagePage,
    GeneralAccessToParkingPage,
    PaymentPage


  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp,{
      scrollPadding: false,
      scrollAssist: true,
      autoFocusAssist: true
    }),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    BrowserAnimationsModule,
    AngularFirestoreModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    GeneralTabPage,
    MyApp,
    LoginPage,
    RegisterPage,
    GeneralHomePage,
    GeneralStatusParkingPage,
    GeneralViewZonePage,
    GeneralFirstPage,
    NewsPage,
    GeneralHomePage,
    PaymentPage,
    GeneralProfilePage,
    SelectFindComponent,
    ZoomAreaComponent,
    GeneralMenuPage,
    GeneralHistoryPage,
    SearchAllPage,
    SearchFavoritePage,
    GeneralContactPage,
    GeneralPeviewParkingPage,
    GeneralZoneConfirmPage,
    AgreePage,
    SettingPage,
    GeneralViewMapImagePage,
    GeneralAccessToParkingPage


  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    ZoomAreaProvider,
    SearchDataProvider,
    LaunchNavigator,
    NativeStorage,
    BarcodeScanner,
    DatePicker,
    InAppBrowser,
    { provide: ScreenOrientation, useClass: ScreenOrientationMock }
  ]
  
})
export class AppModule {}
