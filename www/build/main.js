webpackJsonp([21],{

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__general_menu_general_menu__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_auth_auth__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__register_register__ = __webpack_require__(276);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_screen_orientation__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angularfire2_auth__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_angularfire2_auth__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, loadCtl, alertCtl, app, auth, screenOrientation, af, platform) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadCtl = loadCtl;
        this.alertCtl = alertCtl;
        this.app = app;
        this.auth = auth;
        this.screenOrientation = screenOrientation;
        this.af = af;
        this.platform = platform;
        this.regis = __WEBPACK_IMPORTED_MODULE_2__register_register__["a" /* RegisterPage */];
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }
    LoginPage.prototype.onLogin = function (from) {
        var _this = this;
        var load = this.loadCtl.create({
            content: 'Please wait...'
        });
        load.present();
        this.auth.signin(from.value.txtEmail, from.value.txtPass)
            .then(function (data) {
            load.dismiss();
            if (data['user']['emailVerified'] == false) {
                _this.af.auth.signOut();
                _this.alertCtl.create({
                    title: "Login failed!!",
                    message: "Please confirm your email address. Via email you registered"
                }).present();
            }
            else {
                _this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_0__general_menu_general_menu__["a" /* GeneralMenuPage */]);
            }
        })
            .catch(function (error) {
            load.dismiss();
            var alert = _this.alertCtl.create({
                title: "Login failed!!",
                message: error.message + " Please check email and password.",
                buttons: ['Ok']
            });
            alert.present();
        });
    };
    LoginPage.prototype.forgotPass = function () {
        var _this = this;
        var alertRevoke = this.alertCtl.create({
            title: "Reset password",
            subTitle: "Please enter your email address.",
            inputs: [
                {
                    name: 'txtEmail',
                    placeholder: 'Email'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel'
                },
                {
                    text: 'OK',
                    handler: function (data) {
                        var loadRevoke = _this.loadCtl.create({
                            content: "Please wait..."
                        });
                        loadRevoke.present();
                        if (data.txtEmail.trim() === "") {
                            var alert_1 = _this.alertCtl.create({
                                title: "Reset failed!!",
                                message: "Please enter full information.",
                                buttons: ['OK']
                            });
                            alert_1.present();
                            loadRevoke.dismiss();
                        }
                        else {
                            var email = data.txtEmail;
                            var passRevoke = data.txtPass;
                            _this.auth.forgot(email).then(function () {
                                _this.alertCtl.create({
                                    title: "Reset password",
                                    message: "Please check email We send messages. Reset password"
                                }).present();
                                loadRevoke.dismiss();
                            });
                        }
                    }
                }
            ]
        });
        alertRevoke.present();
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Volumes/9rachata/Archive/navi-park/src/pages/login/login.html"*/'\n<ion-content padding class="animated fadeIn login auth-page">\n  <div class="login-content">\n\n    <!-- Logo -->\n    <div style="padding-bottom: 1px" text-center class="animated fadeInDown">\n      <div class="logo"></div>\n      <h2 ion-text class="text-primary">\n        <strong>Naviparking</strong> \n      </h2>\n    </div>\n\n    <!-- Login form -->\n\n    <ion-card>\n      <ion-card-content>\n        <form class="list-form" #f="ngForm" (ngSubmit)="onLogin(f)">\n          <ion-item class="borders" style="margin-bottom: 10px;">\n            <ion-label style="padding-left: 10px;" >\n              <ion-icon name="person" item-start class="text-primary"></ion-icon>\n              E-mail\n            </ion-label>\n            <ion-input \n              name="txtEmail"\n              type="email" \n              ngModel\n              pattern="[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})"\n              required>\n            </ion-input>\n          </ion-item>\n    \n          <ion-item class="borders"> \n            <ion-label style="padding-left: 10px" >\n              <ion-icon name="lock" item-start class="text-primary"></ion-icon>\n              Password\n            </ion-label>\n            <ion-input \n              name="txtPass"\n              type="password" \n              ngModel\n              required> \n            </ion-input>\n          </ion-item>\n          <ion-item no-lines >\n    \n            <p text-right ion-text color="light" tappable (click)="forgotPass()"><strong>Forgot Password?</strong></p>\n          </ion-item>\n          <ion-item no-lines >\n            <div>\n              <button ion-button round large  icon-start block color="primary" tappable  type="submit"  [disabled]="!f.valid">\n                <ion-icon name="log-in"></ion-icon>\n                Login\n              </button>\n            </div>\n        \n        \n          </ion-item>\n        </form>\n    \n        <!-- Other links -->\n        <div text-center margin-top>\n          <span ion-text color="light" [navPush]="regis">New User? </span>\n        </div>\n      </ion-card-content>\n    </ion-card>\n   \n\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Volumes/9rachata/Archive/navi-park/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_1__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_screen_orientation__["a" /* ScreenOrientation */],
            __WEBPACK_IMPORTED_MODULE_6_angularfire2_auth__["AngularFireAuth"],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["n" /* Platform */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 108:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeneralHomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_native_native_storage__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_search_data_search_data__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_screen_orientation__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_select_find_select_find__ = __webpack_require__(432);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__general_status_parking_general_status_parking__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angularfire2_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_google_maps__ = __webpack_require__(1313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__node_modules_ngx_translate_core__ = __webpack_require__(76);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var GeneralHomePage = /** @class */ (function () {
    function GeneralHomePage(navCtrl, navParams, afDb, platfrom, AlertCtl, zone, loadCtl, popverCtrl, screenOrientation, dataService, transition, nativeStorage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.afDb = afDb;
        this.platfrom = platfrom;
        this.AlertCtl = AlertCtl;
        this.zone = zone;
        this.loadCtl = loadCtl;
        this.popverCtrl = popverCtrl;
        this.screenOrientation = screenOrientation;
        this.dataService = dataService;
        this.transition = transition;
        this.nativeStorage = nativeStorage;
        this.listData = [];
        this.statusZoom = false;
        this.state = true;
        this.stateMap = false;
        this.itemsOutput = [];
        this.itemAll = [];
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
        this.autocomplete = { input: '' };
    }
    GeneralHomePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.nativeStorage.getItem('ln')
            .then((function (data) {
            if (data == "en") {
                _this.loadMain = _this.loadCtl.create({
                    content: "Please wait..."
                });
                _this.loadMain.present();
            }
            else {
                _this.loadMain = _this.loadCtl.create({
                    content: "โปรดรอสักครู่..."
                });
                _this.loadMain.present();
            }
        })), function (error) { return console.error(error); };
        // this.loadMain = this.loadCtl.create({
        //   content: "Please wait..."
        // })
        // this.loadMain.present();
        this.platfrom.ready().then(function () {
            _this.afDb.object('/').valueChanges().subscribe(function (res) {
                _this.data = res;
                _this.rawDataMaps(_this.data);
            });
        }).catch(function (error) {
            _this.AlertCtl.create({
                title: "Error !!!",
                message: error + ""
            }).present();
        });
    };
    GeneralHomePage.prototype.rawDataMaps = function (data) {
        this.listData = [];
        if (this.stateMap == true)
            this.map.clear();
        var keyNameZone = Object.keys(data);
        for (var i = 0; i < keyNameZone.length; i++) {
            if (keyNameZone[i] != "data_user" && keyNameZone[i] != "data") {
                var resultMain = {};
                resultMain['name'] = keyNameZone[i];
                resultMain['lat'] = data[keyNameZone[i]]['lat'];
                resultMain['lng'] = data[keyNameZone[i]]['long'];
                resultMain['title'] = keyNameZone[i];
                var keyCheckCh = Object.keys(data[keyNameZone[i]]);
                var spaces = 0;
                var total = 0;
                // เช็คจำนวนช่องว่าง
                for (var j = 0; j < keyCheckCh.length; j++) {
                    if (keyCheckCh[j] != "A_Total Zone" && keyCheckCh[j] != "lat" && keyCheckCh[j] != "long" && keyCheckCh[j] != "image-zone" && keyCheckCh[j] != "opening-time" && keyCheckCh[j] != "price-range" && keyCheckCh[j] != "nameTH" && keyCheckCh[j] != "nameEN") {
                        spaces += data[keyNameZone[i]][keyCheckCh[j]]['A_Available'];
                        total += data[keyNameZone[i]][keyCheckCh[j]]['A_Total Lot'];
                    }
                }
                resultMain['spaces'] = spaces;
                resultMain['total'] = total;
                this.listData.push(resultMain);
            }
        }
        this.loadMap();
    };
    GeneralHomePage.prototype.updateSearchResults = function () {
        if (this.autocomplete.input == '') {
            this.itemsOutput = [];
            return;
        }
        this.itemsOutput = this.dataService.filterItems(this.autocomplete.input, this.listData);
        console.log(JSON.stringify(this.itemsOutput));
    };
    GeneralHomePage.prototype.loadMap = function () {
        var _this = this;
        var mapOptions = {
            camera: {
                target: {
                    lat: 13.764881,
                    lng: 100.538298,
                },
                zoom: 6,
                tilt: 30
            },
        };
        if (this.stateMap == false)
            this.map = __WEBPACK_IMPORTED_MODULE_8__ionic_native_google_maps__["a" /* GoogleMaps */].create('map_canvas', mapOptions);
        else {
            if (this.map.getCameraPosition().zoom <= 7) {
                this.addParking();
            }
            else {
                this.addParkingWithNaumber();
            }
        }
        this.map.on(__WEBPACK_IMPORTED_MODULE_8__ionic_native_google_maps__["b" /* GoogleMapsEvent */].MAP_READY).subscribe(function () {
            _this.loadMain.dismiss();
            _this.stateMap = true;
            _this.map.on(__WEBPACK_IMPORTED_MODULE_8__ionic_native_google_maps__["b" /* GoogleMapsEvent */].MAP_DRAG_END).subscribe(function () {
                console.log(_this.map.getCameraPosition().zoom);
                if (_this.map.getCameraPosition().zoom > 7 && _this.statusZoom == false) {
                    _this.statusZoom = true;
                    _this.map.clear();
                    _this.addParkingWithNaumber();
                }
                else if (_this.map.getCameraPosition().zoom <= 7 && _this.statusZoom == true) {
                    _this.statusZoom = false;
                    _this.map.clear();
                    _this.addParking();
                }
            });
            _this.addParking();
        });
    };
    GeneralHomePage.prototype.addParking = function () {
        var _this = this;
        for (var i = 0; i < this.listData.length; i++) {
            var icon;
            if (this.listData[i].total == this.listData[i].spaces) {
                icon = {
                    url: 'assets/imgs/marker-100.png',
                    size: {
                        width: 45,
                        height: 40
                    }
                };
            }
            else if (this.listData[i].spaces < this.listData[i].total && this.listData[i].spaces != 0) {
                icon = {
                    url: 'assets/imgs/marker-1.png',
                    size: {
                        width: 45,
                        height: 40
                    }
                };
            }
            else if (this.listData[i].spaces == 0) {
                icon = {
                    url: 'assets/imgs/marker-0.png',
                    size: {
                        width: 45,
                        height: 40
                    }
                };
            }
            var marker = this.map.addMarkerSync({
                icon: icon,
                position: {
                    lat: this.listData[i].lat,
                    lng: this.listData[i].lng
                }
            });
            marker.on(__WEBPACK_IMPORTED_MODULE_8__ionic_native_google_maps__["b" /* GoogleMapsEvent */].MARKER_CLICK).subscribe(function (data) {
                _this.checkNamePos(data[0].lat, data[0].lng);
            });
        }
    };
    GeneralHomePage.prototype.addParkingWithNaumber = function () {
        var _this = this;
        for (var i = 0; i < this.listData.length; i++) {
            var icon;
            if (this.listData[i].spaces == 0) {
                icon = {
                    url: 'assets/imgs/marker0.png',
                    size: {
                        width: 45,
                        height: 40
                    }
                };
            }
            else if (this.listData[i].spaces == 1) {
                icon = {
                    url: 'assets/imgs/marker1.png',
                    size: {
                        width: 45,
                        height: 40
                    }
                };
            }
            else if (this.listData[i].spaces == 2) {
                icon = {
                    url: 'assets/imgs/marker2.png',
                    size: {
                        width: 45,
                        height: 40
                    }
                };
            }
            else if (this.listData[i].spaces == 3) {
                icon = {
                    url: 'assets/imgs/marker3.png',
                    size: {
                        width: 45,
                        height: 40
                    }
                };
            }
            else if (this.listData[i].spaces == 4) {
                icon = {
                    url: 'assets/imgs/marker4.png',
                    size: {
                        width: 45,
                        height: 40
                    }
                };
            }
            else if (this.listData[i].spaces == 5) {
                icon = {
                    url: 'assets/imgs/marker5.png',
                    size: {
                        width: 45,
                        height: 40
                    }
                };
            }
            else if (this.listData[i].spaces == 6) {
                icon = {
                    url: 'assets/imgs/marker6.png',
                    size: {
                        width: 45,
                        height: 40
                    }
                };
            }
            else if (this.listData[i].spaces == 7) {
                icon = {
                    url: 'assets/imgs/marker7.png',
                    size: {
                        width: 45,
                        height: 40
                    }
                };
            }
            else if (this.listData[i].spaces == 8) {
                icon = {
                    url: 'assets/imgs/marker8.png',
                    size: {
                        width: 45,
                        height: 40
                    }
                };
            }
            else if (this.listData[i].spaces == 9) {
                icon = {
                    url: 'assets/imgs/marker9.png',
                    size: {
                        width: 45,
                        height: 40
                    }
                };
            }
            else if (this.listData[i].spaces >= 10) {
                icon = {
                    url: 'assets/imgs/marker10P.png',
                    size: {
                        width: 45,
                        height: 40
                    }
                };
            }
            var marker = this.map.addMarkerSync({
                icon: icon,
                position: {
                    lat: this.listData[i].lat,
                    lng: this.listData[i].lng
                }
            });
            marker.on(__WEBPACK_IMPORTED_MODULE_8__ionic_native_google_maps__["b" /* GoogleMapsEvent */].MARKER_CLICK).subscribe(function (data) {
                _this.checkNamePos(data[0].lat, data[0].lng);
            });
        }
    };
    GeneralHomePage.prototype.checkNamePos = function (lat, lng) {
        for (var i = 0; i < this.listData.length; i++) {
            if (this.listData[i].lat == lat && this.listData[i].lng == lng) {
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__general_status_parking_general_status_parking__["a" /* GeneralStatusParkingPage */], { pos: this.listData[i].name });
            }
        }
    };
    GeneralHomePage.prototype.selectSearchResult = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__general_status_parking_general_status_parking__["a" /* GeneralStatusParkingPage */], { pos: item['name'] });
        // console.log(JSON.stringify(item))
    };
    GeneralHomePage.prototype.onShowOption = function (ev) {
        var popver = this.popverCtrl.create(__WEBPACK_IMPORTED_MODULE_3__components_select_find_select_find__["a" /* SelectFindComponent */]);
        popver.present({ ev: ev });
    };
    GeneralHomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_5__angular_core__["m" /* Component */])({
            selector: 'page-general-home',template:/*ion-inline-start:"/Volumes/9rachata/Archive/navi-park/src/pages/general-home/general-home.html"*/'<!--\n  Generated template for the GeneralHomePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color = "navbar">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-searchbar [(ngModel)]="autocomplete.input" (ionInput)="updateSearchResults()" placeholder="{{\'SEARCH FOR  A PARKING\' | translate}}"></ion-searchbar>\n    <ion-buttons end>\n        <button ion-button  icon-only (click)="onShowOption($event)">\n            <ion-icon name="ios-search"></ion-icon>\n        </button>\n    </ion-buttons>\n\n    \n<!-- \n   <ion-buttons end>\n        <button ion-button  icon-only>\n            <ion-icon name="more"></ion-icon>\n        </button>\n   </ion-buttons> -->\n\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content no-padding>\n    <ion-list [hidden]="itemsOutput.length == 0">\n         <button ion-item *ngFor="let item of itemsOutput" tappable (click)="selectSearchResult(item)" >\n          {{ item.name }}\n        </button>\n      </ion-list>\n  \n  <div id="map_canvas">\n\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Volumes/9rachata/Archive/navi-park/src/pages/general-home/general-home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_7_angularfire2_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5__angular_core__["M" /* NgZone */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["o" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_screen_orientation__["a" /* ScreenOrientation */],
            __WEBPACK_IMPORTED_MODULE_1__providers_search_data_search_data__["a" /* SearchDataProvider */],
            __WEBPACK_IMPORTED_MODULE_9__node_modules_ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_0__ionic_native_native_storage__["a" /* NativeStorage */]])
    ], GeneralHomePage);
    return GeneralHomePage;
}());

//# sourceMappingURL=general-home.js.map

/***/ }),

/***/ 130:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var AuthProvider = /** @class */ (function () {
    function AuthProvider(afAuth) {
        this.afAuth = afAuth;
    }
    AuthProvider.prototype.signup = function (email, pass) {
        return this.afAuth.auth.createUserWithEmailAndPassword(email, pass);
        // return firebase.auth().createUserWithEmailAndPassword(email , pass);
    };
    AuthProvider.prototype.signin = function (email, pass) {
        return this.afAuth.auth.signInWithEmailAndPassword(email, pass);
        // return firebase.auth().signInWithEmailAndPassword(email, pass);
    };
    AuthProvider.prototype.sendmail = function () {
        return this.afAuth.auth.currentUser.sendEmailVerification();
    };
    AuthProvider.prototype.forgot = function (email) {
        return this.afAuth.auth.sendPasswordResetEmail(email);
    };
    AuthProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__["AngularFireAuth"]])
    ], AuthProvider);
    return AuthProvider;
}());

//# sourceMappingURL=auth.js.map

/***/ }),

/***/ 131:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchDataProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the SearchDataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var SearchDataProvider = /** @class */ (function () {
    function SearchDataProvider(db) {
        //   this.items = [
        //     {title: 'One'},
        //     {title: 'two'},
        //     {title: 'three'},
        //     {title: 'four'},
        //     {title: 'five'},
        //     {title: 'six'},
        //     {title: 'One'},
        //     {title: 'two'},
        //     {title: 'three'},
        //     {title: 'four'},
        //     {title: 'five'},
        //     {title: 'six'},
        //     {title: 'One'},
        //     {title: 'two'},
        //     {title: 'three'},
        //     {title: 'four'},
        //     {title: 'five'},
        //     {title: 'six'}
        // ]
        this.db = db;
        this.items = [];
    }
    SearchDataProvider.prototype.filterItems = function (searchTerm, inputItem) {
        this.items = inputItem;
        return this.items.filter(function (item) {
            return item.title.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        });
    };
    SearchDataProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["AngularFireDatabase"]])
    ], SearchDataProvider);
    return SearchDataProvider;
}());

//# sourceMappingURL=search-data.js.map

/***/ }),

/***/ 1331:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProgressBarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProgressBarComponent = /** @class */ (function () {
    function ProgressBarComponent() {
    }
    ProgressBarComponent.prototype.setMyStyles = function () {
        var colorpro;
        if (this.progress == 0) {
            colorpro = "#64dd17";
        }
        else {
            colorpro = "#f44336";
        }
        var styles = {
            'width': this.progress + '%',
            'background-color': colorpro
        };
        return styles;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('progress'),
        __metadata("design:type", Object)
    ], ProgressBarComponent.prototype, "progress", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('colorProgress'),
        __metadata("design:type", Object)
    ], ProgressBarComponent.prototype, "colorProgress", void 0);
    ProgressBarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'progress-bar',template:/*ion-inline-start:"/Volumes/9rachata/Archive/navi-park/src/components/progress-bar/progress-bar.html"*/'<div class="progress-outer">\n  <div class="progress-inner" [ngStyle]="setMyStyles()"  >\n    \n  </div>\n</div>\n<!-- [style.background-color]="\'#\'+colorProgress" [style.width]="progress + \'%\'" -->'/*ion-inline-end:"/Volumes/9rachata/Archive/navi-park/src/components/progress-bar/progress-bar.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], ProgressBarComponent);
    return ProgressBarComponent;
}());

//# sourceMappingURL=progress-bar.js.map

/***/ }),

/***/ 1332:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_native_storage__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_general_menu_general_menu__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__ = __webpack_require__(596);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__ = __webpack_require__(595);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_login_login__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__node_modules_ngx_translate_core__ = __webpack_require__(76);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, afAuth, loading, alertCtrl, platfrom, translate, nativeStorage, db) {
        var _this = this;
        this.afAuth = afAuth;
        this.loading = loading;
        this.alertCtrl = alertCtrl;
        this.platfrom = platfrom;
        this.translate = translate;
        this.nativeStorage = nativeStorage;
        this.db = db;
        platform.ready().then(function () {
            _this.nativeStorage.getItem('ln')
                .then(function (data) { return _this.translate.use(data); }, (function (error) {
                _this.nativeStorage.setItem('ln', 'en')
                    .then(function () { return _this.translate.use('en'); }, function (error) { return console.error('Error storing item', error); });
            }));
            var load = _this.loading.create({
                content: 'Please wait...'
            });
            load.present();
            var a = _this.afAuth.authState.subscribe(function (data) {
                try {
                    if (data.emailVerified == true) {
                        _this.rootPage = __WEBPACK_IMPORTED_MODULE_2__pages_general_menu_general_menu__["a" /* GeneralMenuPage */];
                        // this.fcm.subscribeToTopic(data['uid']);
                        console.log(data['uid']);
                        load.dismiss();
                        a.unsubscribe();
                        var start = Date.now();
                        _this.db.object('/data_user/' + data['uid']).update({ 'lastUsed': start * 1000 });
                    }
                    else {
                        _this.rootPage = __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */];
                        load.dismiss();
                        a.unsubscribe();
                    }
                }
                catch (error) {
                    _this.rootPage = __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */];
                    load.dismiss();
                    a.unsubscribe();
                }
            });
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Volumes/9rachata/Archive/navi-park/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Volumes/9rachata/Archive/navi-park/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__["AngularFireAuth"],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_9__node_modules_ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_1__ionic_native_native_storage__["a" /* NativeStorage */],
            __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["AngularFireDatabase"]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 1341:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ZoomAreaComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_zoom_area_zoom_area__ = __webpack_require__(598);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_animations__ = __webpack_require__(265);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ZoomAreaComponent = /** @class */ (function () {
    function ZoomAreaComponent(zoomAreaProvider) {
        this.zoomAreaProvider = zoomAreaProvider;
        this.afterZoomIn = new __WEBPACK_IMPORTED_MODULE_3__angular_core__["v" /* EventEmitter */]();
        this.afterZoomOut = new __WEBPACK_IMPORTED_MODULE_3__angular_core__["v" /* EventEmitter */]();
        this.controlsChanged = new __WEBPACK_IMPORTED_MODULE_3__angular_core__["v" /* EventEmitter */]();
        this.scaleChanged = new __WEBPACK_IMPORTED_MODULE_3__angular_core__["v" /* EventEmitter */]();
        this.zoomConfig = {
            ow: 0,
            oh: 0,
            original_x: 0,
            original_y: 0,
            max_x: 0,
            max_y: 0,
            min_x: 0,
            min_y: 0,
            x: 0,
            y: 0,
            last_x: 0,
            last_y: 0,
            scale: 1,
            base: 1,
        };
        this.zoomControlsState = 'shown';
    }
    ZoomAreaComponent.prototype.ngOnChanges = function (changes) {
        if ('controls' in changes) {
            var showControls = changes['controls'];
            if (showControls && showControls.currentValue) {
                this.zoomControlsState = 'shown';
            }
            else {
                this.zoomControlsState = 'shown';
            }
        }
        if ('scale' in changes) {
            var scaleValue = changes['scale'];
            if (scaleValue && scaleValue.currentValue && scaleValue.currentValue === 1) {
                this.zoomReset();
            }
        }
    };
    ZoomAreaComponent.prototype.zoomResetAll = function () {
    };
    ZoomAreaComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.content.ionScroll.subscribe(function (data) {
            _this.zoomAreaProvider.notifyScroll(data);
        });
        this._pinchZoom(this.zoom.nativeElement, this.content);
        // Watch for user setCenter call
        var self = this;
        this.zoomAreaProvider.centerChanged$.subscribe(function (coords) {
            if (self.zoomConfig.scale === 1) {
                return;
            }
            self.setCoor(coords.x, coords.y);
            self.transform(coords.x, coords.y);
        });
    };
    ZoomAreaComponent.prototype.toggleZoomControls = function () {
        this.zoomControlsState = "shown";
        // this.zoomControlsState = this.zoomControlsState === 'shown' ? 'hidden' : 'shown';
    };
    ZoomAreaComponent.prototype.zoomIn = function () {
        this.zoomConfig.scale += 0.2;
        if (this.zoomConfig.scale > 1) {
            this.zoomAreaProvider.notifyScrollState(this.zoomAreaProvider.SCROLL_STATE.COLAPSED);
        }
        // if (this.zoomConfig.scale >= 2) {
        //   this.zoomConfig.scale = 1;
        // }
        this.transform();
        this.afterZoomIn.emit();
    };
    ZoomAreaComponent.prototype.zoomOut = function (reset) {
        if (!this.zoomRootElement) {
            return;
        }
        this.zoomConfig.scale -= 1;
        if (this.zoomConfig.scale < 1) {
            this.zoomConfig.scale = 1;
        }
        if (this.zoomConfig.scale === 1) {
            reset = true;
            this.zoomAreaProvider.notifyScrollState(this.zoomAreaProvider.SCROLL_STATE.NORMAL);
        }
        reset ? this.transform(0.1, 0.1) : this.transform();
        this.afterZoomOut.emit();
    };
    ZoomAreaComponent.prototype.zoomReset = function () {
        this.zoomConfig.scale = 1;
        if (this.content && this.content.scrollTop) {
            this.content.scrollTop = 0;
        }
        this.zoomOut(true);
    };
    ZoomAreaComponent.prototype._pinchZoom = function (elm, content) {
        this.zoomRootElement = elm;
        this.gesture = new __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Gesture */](this.zoomRootElement);
        for (var i = 0; i < elm.children.length; i++) {
            var c = elm.children.item(i);
            this.zoomConfig.ow = c.offsetWidth;
            this.zoomConfig.oh += c.offsetHeight;
        }
        this.zoomConfig.original_x = content.contentWidth - this.zoomConfig.ow;
        this.zoomConfig.original_y = content.contentHeight - this.zoomConfig.oh;
        this.zoomConfig.max_x = this.zoomConfig.original_x;
        this.zoomConfig.max_y = this.zoomConfig.original_y;
        this.zoomConfig.base = this.zoomConfig.scale;
        this.gesture.listen();
        this.gesture.on('pan', this.onPan.bind(this));
        this.gesture.on('panend', this.onPanend.bind(this));
        this.gesture.on('pancancel', this.onPanend.bind(this));
        // this.gesture.on('tap', this.onTap.bind(this));
        this.gesture.on('pinch', this.onPinch.bind(this));
        this.gesture.on('pinchend', this.onPinchend.bind(this));
        this.gesture.on('pinchcancel', this.onPinchend.bind(this));
    };
    ZoomAreaComponent.prototype.onPan = function (ev) {
        console.log("OnPan  " + this.zoomConfig.scale);
        if (this.zoomConfig.scale === 1) {
            return;
        }
        this.setCoor(ev.deltaX, ev.deltaY);
        this.transform();
    };
    ZoomAreaComponent.prototype.onPanend = function () {
        console.log("onPanend  " + this.zoomConfig.scale);
        this.zoomConfig.last_x = this.zoomConfig.x;
        this.zoomConfig.last_y = this.zoomConfig.y;
    };
    // onTap(ev) {
    //   if (ev && ev.tapCount > 1) {
    //     let reset = false;
    //     this.zoomConfig.scale += .5;
    //     if (this.zoomConfig.scale >= 2) {
    //       this.zoomConfig.scale = 2;
    //       reset = true;
    //     }
    //     this.setBounds();
    //     reset ? this.transform(this.zoomConfig.max_x/2, this.zoomConfig.max_y/2) : this.transform();
    //   }
    // }
    ZoomAreaComponent.prototype.onPinch = function (ev) {
        this.zoomConfig.scale = this.zoomConfig.base + (ev.scale * this.zoomConfig.scale - this.zoomConfig.scale) / this.zoomConfig.scale;
        if (this.zoomConfig.scale >= 2) {
            this.zoomConfig.scale = 2;
        }
        this.setBounds();
        this.transform();
        console.log("onPinch  " + this.zoomConfig.scale);
    };
    ZoomAreaComponent.prototype.onPinchend = function (ev) {
        if (this.zoomConfig.scale >= 2) {
            this.zoomConfig.scale = 2;
        }
        if (this.zoomConfig.scale < 1) {
            this.zoomConfig.scale = 1;
        }
        this.zoomConfig.base = this.zoomConfig.scale;
        this.setBounds();
        this.transform();
    };
    ZoomAreaComponent.prototype.setBounds = function () {
        var scaled_x = Math.ceil((this.zoomRootElement.offsetWidth * this.zoomConfig.scale - this.zoomRootElement.offsetWidth) / 2);
        var scaled_y = Math.ceil((this.zoomRootElement.offsetHeight * this.zoomConfig.scale - this.zoomRootElement.offsetHeight) / 2);
        var overflow_x = Math.ceil(this.zoomConfig.original_x * this.zoomConfig.scale - this.zoomConfig.original_x);
        var overflow_y = Math.ceil(this.zoomConfig.oh * this.zoomConfig.scale - this.zoomConfig.oh);
        this.zoomConfig.max_x = this.zoomConfig.original_x - scaled_x + overflow_x;
        this.zoomConfig.min_x = 0 + scaled_x;
        this.zoomConfig.max_y = this.zoomConfig.original_y + scaled_y - overflow_y;
        this.zoomConfig.min_y = 0 + scaled_y;
        this.setCoor(-scaled_x, scaled_y);
    };
    ZoomAreaComponent.prototype.setCoor = function (xx, yy) {
        var compensation = this.zoomConfig.scale === 2 ? 1.05 : (this.zoomConfig.scale / 1.25);
        this.zoomConfig.x = Math.min(Math.max((this.zoomConfig.last_x + xx), this.zoomConfig.max_x * compensation), this.zoomConfig.min_x * compensation);
        this.zoomConfig.y = Math.min(Math.max((this.zoomConfig.last_y + yy), this.zoomConfig.max_y * compensation), this.zoomConfig.min_y * compensation);
    };
    ZoomAreaComponent.prototype.transform = function (xx, yy) {
        this.zoomRootElement.style.transform = "translate3d(" + (xx || this.zoomConfig.x) + "px, " + (yy || this.zoomConfig.y) + "px, 0) scale3d(" + this.zoomConfig.scale + ", " + this.zoomConfig.scale + ", 1)";
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["_8" /* ViewChild */])('zoomAreaRoot'),
        __metadata("design:type", Object)
    ], ZoomAreaComponent.prototype, "zoom", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], ZoomAreaComponent.prototype, "content", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["O" /* Output */])(),
        __metadata("design:type", Object)
    ], ZoomAreaComponent.prototype, "afterZoomIn", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["O" /* Output */])(),
        __metadata("design:type", Object)
    ], ZoomAreaComponent.prototype, "afterZoomOut", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["D" /* Input */])(),
        __metadata("design:type", Boolean)
    ], ZoomAreaComponent.prototype, "controls", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["O" /* Output */])(),
        __metadata("design:type", Object)
    ], ZoomAreaComponent.prototype, "controlsChanged", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["D" /* Input */])(),
        __metadata("design:type", Number)
    ], ZoomAreaComponent.prototype, "scale", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["O" /* Output */])(),
        __metadata("design:type", Object)
    ], ZoomAreaComponent.prototype, "scaleChanged", void 0);
    ZoomAreaComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["m" /* Component */])({
            selector: 'zoom-area',template:/*ion-inline-start:"/Volumes/9rachata/Archive/navi-park/src/components/zoom-area/zoom-area.html"*/'<ion-content class="bgView">\n  <div #zoomAreaRoot class="zoom" (click)="toggleZoomControls()">\n    <div class="fit">\n      <ng-content></ng-content>\n    </div>\n  </div>\n  <ion-fab right top [@visibilityChanged]="zoomControlsState">\n    <!-- <button (click)="zoomIn()" ion-fab color="primary" class="btn-zoom">\n        <ion-icon name="add-circle"></ion-icon>\n    </button>\n    <button (click)="zoomOut()" ion-fab color="primary" class="btn-zoom">\n        <ion-icon name="remove-circle"></ion-icon>\n    </button> -->\n    <button (click)="zoomReset()" ion-fab color="primary" class="btn-zoom">\n        <ion-icon name="md-contract"></ion-icon>\n    </button>\n  </ion-fab>\n\n</ion-content>'/*ion-inline-end:"/Volumes/9rachata/Archive/navi-park/src/components/zoom-area/zoom-area.html"*/,
            animations: [
                Object(__WEBPACK_IMPORTED_MODULE_2__angular_animations__["j" /* trigger */])('visibilityChanged', [
                    Object(__WEBPACK_IMPORTED_MODULE_2__angular_animations__["g" /* state */])('shown', Object(__WEBPACK_IMPORTED_MODULE_2__angular_animations__["h" /* style */])({ opacity: 1, display: 'block' })),
                    Object(__WEBPACK_IMPORTED_MODULE_2__angular_animations__["g" /* state */])('hidden', Object(__WEBPACK_IMPORTED_MODULE_2__angular_animations__["h" /* style */])({ opacity: 0, display: 'none' })),
                    Object(__WEBPACK_IMPORTED_MODULE_2__angular_animations__["i" /* transition */])('shown => hidden', Object(__WEBPACK_IMPORTED_MODULE_2__angular_animations__["e" /* animate */])('300ms')),
                    Object(__WEBPACK_IMPORTED_MODULE_2__angular_animations__["i" /* transition */])('hidden => shown', Object(__WEBPACK_IMPORTED_MODULE_2__angular_animations__["e" /* animate */])('300ms')),
                ])
            ]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__providers_zoom_area_zoom_area__["a" /* ZoomAreaProvider */]])
    ], ZoomAreaComponent);
    return ZoomAreaComponent;
}());

//# sourceMappingURL=zoom-area.js.map

/***/ }),

/***/ 156:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeneralMenuPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__payment_payment__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_native_storage__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__general_contact_general_contact__ = __webpack_require__(269);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__general_profile_general_profile__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__general_home_general_home__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__general_first_general_first__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__general_history_general_history__ = __webpack_require__(273);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__general_tab_general_tab__ = __webpack_require__(274);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_angularfire2_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__node_modules_ngx_translate_core__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__general_access_to_parking_general_access_to_parking__ = __webpack_require__(266);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};















var GeneralMenuPage = /** @class */ (function () {
    function GeneralMenuPage(navCtrl, navParams, anth, db, translate, nativeStorage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.anth = anth;
        this.db = db;
        this.translate = translate;
        this.nativeStorage = nativeStorage;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_9__general_tab_general_tab__["a" /* GeneralTabPage */];
        this.pages = [
            { title: 'HOME', name: 'GeneralTabPage', pageName: __WEBPACK_IMPORTED_MODULE_9__general_tab_general_tab__["a" /* GeneralTabPage */], tabComponent: __WEBPACK_IMPORTED_MODULE_7__general_first_general_first__["a" /* GeneralFirstPage */], index: 0, icon: 'ios-home-outline' },
            { title: 'PARKING', name: 'GeneralTabPage', pageName: __WEBPACK_IMPORTED_MODULE_9__general_tab_general_tab__["a" /* GeneralTabPage */], tabComponent: __WEBPACK_IMPORTED_MODULE_6__general_home_general_home__["a" /* GeneralHomePage */], index: 1, icon: 'ios-car-outline' },
            { title: 'PROFILE', name: 'GeneralTabPage', pageName: __WEBPACK_IMPORTED_MODULE_5__general_profile_general_profile__["a" /* GeneralProfilePage */], icon: 'ios-contact-outline' },
            { title: 'HISTORY', name: 'GeneralHistoryPage', pageName: __WEBPACK_IMPORTED_MODULE_8__general_history_general_history__["a" /* GeneralHistoryPage */], icon: 'ios-list-box-outline' },
            { title: 'payment', name: 'PaymentPage', pageName: __WEBPACK_IMPORTED_MODULE_0__payment_payment__["a" /* PaymentPage */], icon: 'logo-usd' },
            { title: 'CONTACT US', name: 'GeneralContactPage', pageName: __WEBPACK_IMPORTED_MODULE_4__general_contact_general_contact__["a" /* GeneralContactPage */], icon: 'ios-mail-outline' },
            { title: 'Access To Parking', name: 'GeneralAccessToParkingPage', pageName: __WEBPACK_IMPORTED_MODULE_14__general_access_to_parking_general_access_to_parking__["a" /* GeneralAccessToParkingPage */], icon: 'ios-mail-outline' },
        ];
    }
    GeneralMenuPage.prototype.setEng = function () {
        var _this = this;
        this.nativeStorage.setItem('ln', 'en')
            .then(function () { return _this.translate.use('en'); }, function (error) { return console.error('Error storing item', error); });
    };
    GeneralMenuPage.prototype.setThai = function () {
        var _this = this;
        this.nativeStorage.setItem('ln', 'th')
            .then(function () { return _this.translate.use('th'); }, function (error) { return console.error('Error storing item', error); });
    };
    GeneralMenuPage.prototype.ngOnInit = function () {
        var _this = this;
        this.anth.authState.subscribe(function (data) {
            _this.db.object('/data_user/' + data['uid'] + '/displayname').valueChanges().subscribe(function (res) {
                _this.name = res;
            });
        });
    };
    GeneralMenuPage.prototype.openPage = function (page) {
        var params = {};
        if (page.index) {
            params = { tabIndex: page.index };
        }
        if (this.nav.getActiveChildNavs().length && page.index != undefined) {
            this.nav.getActiveChildNavs()[0].select(page.index);
        }
        else {
            this.nav.push(page.pageName, params);
        }
    };
    GeneralMenuPage.prototype.editProfile = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__general_profile_general_profile__["a" /* GeneralProfilePage */]);
    };
    GeneralMenuPage.prototype.isActive = function (page) {
        var childNav = this.nav.getActiveChildNavs()[0];
        ;
        if (childNav) {
            if (childNav.getSelected() && childNav.getSelected().root === page.tabComponent) {
                return 'navbar';
            }
            return;
        }
        if (this.nav.getActive() && this.nav.getActive().name === page.name) {
            return 'narbar';
        }
        return;
    };
    GeneralMenuPage.prototype.logout = function () {
        var _this = this;
        this.anth.auth.signOut()
            .then(function () {
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
        }).catch(function (error) {
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
        });
        console.log("Logout");
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_10__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_11_ionic_angular__["k" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_11_ionic_angular__["k" /* Nav */])
    ], GeneralMenuPage.prototype, "nav", void 0);
    GeneralMenuPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_10__angular_core__["m" /* Component */])({
            selector: 'page-general-menu',template:/*ion-inline-start:"/Volumes/9rachata/Archive/navi-park/src/pages/general-menu/general-menu.html"*/'\n<ion-menu [content]="content">\n  <ion-header>\n    <ion-toolbar class="user-profile">\n      <ion-grid>\n        <ion-row>\n          <ion-col col-4>\n              <div class="user-avatar">\n                <img src="assets/imgs/park.png">\n              </div>\n          </ion-col>\n          <ion-col padding-top col-8>\n            <h6 ion-text class="no-margin bold text-white">\n              smart  parking intelligent\n            </h6>\n            <span ion-text color="light">{{name}}</span>\n          </ion-col>\n        </ion-row>\n\n        <ion-row no-padding class="other-data">\n          <ion-col no-padding class="column">\n            <button ion-button icon-left small round full color="btndis" menuClose  (click)="editProfile()">\n              <ion-icon name="contact"></ion-icon>\n              {{ \'PROFILE\' | translate }} \n            </button>\n          </ion-col>\n          <ion-col no-padding class="column">\n            <button ion-button icon-left small round full color="light" menuClose (click)="logout()">\n              <ion-icon name="log-out"></ion-icon>\n              {{ \'SIGN OUT\' | translate }} \n            </button>\n          </ion-col>\n        </ion-row>\n\n        <!-- <ion-row>\n          <ion-col >\n            <p>ภาษา</p>\n          </ion-col>\n          <ion-col >\n            <button small clear ion-button color="light"(click)="setThai()">thai</button>\n            \n          </ion-col>\n          <ion-col >\n            <button small clear ion-button  color="light" (click)="setEng()">Eng</button>\n          </ion-col>\n        </ion-row> -->\n\n      </ion-grid>\n    </ion-toolbar>\n  </ion-header>\n \n  <ion-content >\n    <ion-list>\n\n    \n        <ion-item-group>\n            <ion-item-divider color="light">{{\'Menu\' | translate}}</ion-item-divider>\n      <button ion-item menuClose *ngFor="let p of pages" (click)="openPage(p)">\n          <ion-icon item-start [name]="p.icon" [color]="isActive(p)"></ion-icon>\n           {{p.title | translate}}\n        </button>\n      </ion-item-group>\n      <ion-item-group>\n          <ion-item-divider color="light">{{\'language setting\' | translate}}</ion-item-divider>\n          <button ion-item   (click)="setThai()">{{\'Thai Language\' | translate}}</button>\n          <button ion-item   (click)="setEng()">{{\'English language\' | translate}}</button>\n        </ion-item-group>\n    </ion-list>\n  </ion-content>\n</ion-menu>\n \n<!-- main navigation -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"/Volumes/9rachata/Archive/navi-park/src/pages/general-menu/general-menu.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_11_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_11_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_auth__["AngularFireAuth"],
            __WEBPACK_IMPORTED_MODULE_12_angularfire2_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_13__node_modules_ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_1__ionic_native_native_storage__["a" /* NativeStorage */]])
    ], GeneralMenuPage);
    return GeneralMenuPage;
}());

//# sourceMappingURL=general-menu.js.map

/***/ }),

/***/ 157:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeneralProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_native_native_storage__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_screen_orientation__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__(43);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the GeneralProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var GeneralProfilePage = /** @class */ (function () {
    function GeneralProfilePage(navCtrl, navParams, db, loadCtl, anth, toastCtrl, screenOrientation, nativeStorage, plt) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.db = db;
        this.loadCtl = loadCtl;
        this.anth = anth;
        this.toastCtrl = toastCtrl;
        this.screenOrientation = screenOrientation;
        this.nativeStorage = nativeStorage;
        this.plt = plt;
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }
    GeneralProfilePage.prototype.ngOnInit = function () {
        var _this = this;
        this.editForm = new __WEBPACK_IMPORTED_MODULE_6__angular_forms__["b" /* FormGroup */]({
            'fullname': new __WEBPACK_IMPORTED_MODULE_6__angular_forms__["a" /* FormControl */](null, __WEBPACK_IMPORTED_MODULE_6__angular_forms__["g" /* Validators */].required),
            'displayname': new __WEBPACK_IMPORTED_MODULE_6__angular_forms__["a" /* FormControl */](null, __WEBPACK_IMPORTED_MODULE_6__angular_forms__["g" /* Validators */].required),
            'phone': new __WEBPACK_IMPORTED_MODULE_6__angular_forms__["a" /* FormControl */](null, __WEBPACK_IMPORTED_MODULE_6__angular_forms__["g" /* Validators */].required),
            'car': new __WEBPACK_IMPORTED_MODULE_6__angular_forms__["a" /* FormControl */](null, __WEBPACK_IMPORTED_MODULE_6__angular_forms__["g" /* Validators */].required),
            'carmodel': new __WEBPACK_IMPORTED_MODULE_6__angular_forms__["a" /* FormControl */](null, __WEBPACK_IMPORTED_MODULE_6__angular_forms__["g" /* Validators */].required)
        });
        this.anth.authState.subscribe(function (data) {
            _this.uid = data['uid'];
            _this.db.object('/data_user/' + data['uid']).valueChanges().subscribe(function (res) {
                _this.initForm(res);
                // if (this.plt.is('android')) {
                //   this.loadMain.dismiss();
                // }
            });
        });
    };
    GeneralProfilePage.prototype.initForm = function (res) {
        this.editForm = new __WEBPACK_IMPORTED_MODULE_6__angular_forms__["b" /* FormGroup */]({
            'fullname': new __WEBPACK_IMPORTED_MODULE_6__angular_forms__["a" /* FormControl */](res['fullname'], __WEBPACK_IMPORTED_MODULE_6__angular_forms__["g" /* Validators */].required),
            'displayname': new __WEBPACK_IMPORTED_MODULE_6__angular_forms__["a" /* FormControl */](res['displayname'], __WEBPACK_IMPORTED_MODULE_6__angular_forms__["g" /* Validators */].required),
            'phone': new __WEBPACK_IMPORTED_MODULE_6__angular_forms__["a" /* FormControl */](res['phone'], __WEBPACK_IMPORTED_MODULE_6__angular_forms__["g" /* Validators */].required),
            'car': new __WEBPACK_IMPORTED_MODULE_6__angular_forms__["a" /* FormControl */](res['car'], __WEBPACK_IMPORTED_MODULE_6__angular_forms__["g" /* Validators */].required),
            'carmodel': new __WEBPACK_IMPORTED_MODULE_6__angular_forms__["a" /* FormControl */](res['carmodel'], __WEBPACK_IMPORTED_MODULE_6__angular_forms__["g" /* Validators */].required)
        });
    };
    GeneralProfilePage.prototype.onSubmit = function () {
        var fullname = this.editForm.value.fullname;
        var displayname = this.editForm.value.displayname;
        var phone = this.editForm.value.phone;
        var car = this.editForm.value.car;
        var carmodel = this.editForm.value.carmodel;
        var itemsRef = this.db.list('data_user');
        itemsRef.set(this.uid, { fullname: fullname, displayname: displayname, phone: phone, car: car, carmodel: carmodel });
        var toast = this.toastCtrl.create({
            message: 'Record Success',
            duration: 1500,
            position: 'bottom'
        });
        toast.present();
    };
    GeneralProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["m" /* Component */])({
            selector: 'page-general-profile',template:/*ion-inline-start:"/Volumes/9rachata/Archive/navi-park/src/pages/general-profile/general-profile.html"*/'<!--\n  Generated template for the GeneralProfilePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <ion-navbar color = "navbar">\n        <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title>{{ \'PROFILE\' | translate }}</ion-title>\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n<ion-content padding class="bg">\n\n        <div class="login-content">\n    <form  class="list-form" [formGroup]="editForm" (ngSubmit)="onSubmit()">\n        \n    \n\n            <ion-item class="borders" no-lines style="margin-bottom: 10px;">\n              <ion-label floating>{{ \'FULL NAME\' | translate }}</ion-label>\n              <ion-input STYLE=\'color:#FFFFFF;\' type="text" formControlName="fullname"></ion-input>\n            </ion-item>\n            <ion-item class="borders" no-lines style="margin-bottom: 10px;">\n              <ion-label floating>{{ \'DISPLAY NAME\' | translate }}</ion-label>\n              <ion-input STYLE=\'color:#FFFFFF;\' type="text" formControlName="displayname" ></ion-input>\n            </ion-item>\n            <ion-item class="borders" no-lines style="margin-bottom: 10px;">\n              <ion-label floating >{{ \'TELEPHONE NUMBER\' | translate }}</ion-label>\n              <ion-input STYLE=\'color:#FFFFFF;\' type="number" formControlName="phone"></ion-input>\n            </ion-item>\n            <ion-item class="borders" no-lines style="margin-bottom: 10px;">\n              <ion-label floating>{{ \'CAR BRAND\' | translate }}</ion-label>\n              <ion-input STYLE=\'color:#FFFFFF;\' type="text" formControlName="car"></ion-input>\n            </ion-item>\n\n            <ion-item class="borders" no-lines style="margin-bottom: 10px;">\n              <ion-label floating>{{ \'CAR MODEL\' | translate }}</ion-label>\n              <ion-input STYLE=\'color:#FFFFFF;\' type="text" formControlName="carmodel"></ion-input>\n            </ion-item>\n    \n            <ion-item no-lines style="background: transparent">\n                \n        <button type="submit" [disabled]="!editForm.valid" ion-button full round >{{ \'SAVE\' | translate }}</button>\n      \n            </ion-item>\n        \n    </form>\n\n\n    </div>\n\n\n    <!-- <div class="login-content">\n\n         \n            <form class="list-form" #f="ngForm" (ngSubmit)="submitContact(f)">\n              \n             <ion-item   class="borders" no-lines style="margin-bottom: 10px;">\n               <ion-label >{{\'NAME\' | translate}}</ion-label>\n               <ion-input STYLE=\'color:#FFFFFF;\'\n                    name="name"\n                    type="text" \n                    ngModel\n                    required></ion-input>\n             </ion-item>\n         \n             <ion-item class="borders" no-lines style="margin-bottom: 10px;">\n                 <ion-label >{{\'SUBJECT\' | translate}}</ion-label>\n                 <ion-input  \n                    STYLE=\'color:#FFFFFF\' \n                    type="text" \n                    name="subject"\n                    ngModel\n                    required>\n                    </ion-input>\n             </ion-item>\n         \n         \n             <ion-item class="borders" no-lines style="margin-bottom: 10px;">\n                 <ion-label >{{\'DETAIL\' | translate}}</ion-label>\n                 <ion-textarea  \n                    style="height: 100px; color:#FFFFFF;"\n                    name="detail"\n                    ngModel\n                    required></ion-textarea>\n             </ion-item>\n         \n         \n             <ion-item class="borders" no-lines style="margin-bottom: 10px;">\n                 <ion-label >{{\'E-mail\' | translate}}</ion-label>\n                 <ion-input \n                    STYLE=\'color:#FFFFFF\' \n                    type="email" \n                    name="email"\n                    pattern="[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})"\n                    ngModel\n                    required></ion-input>\n             </ion-item>\n         \n             <ion-item class="borders" no-lines style="margin-bottom: 10px;">\n                 <ion-label >{{\'TELEPHONE NUMBER\' | translate}}</ion-label>\n                 <ion-input STYLE=\'color:#FFFFFF\'  \n                    type="number" \n                    name="tel"\n                    ngModel\n                    required></ion-input>\n             </ion-item>\n\n\n             <ion-item no-lines style="background: transparent">\n                    \n                <button [disabled]="!f.valid" ion-button round   icon-start block color="primary" tappable  type="submit">\n                    <ion-icon name="ios-checkmark-circle-outline"></ion-icon>\n                    {{\'SEND\' | translate}}\n                </button>\n                       \n             </ion-item>\n\n\n\n             \n             \n            </form>\n        \n\n</div> -->\n                 \n\n</ion-content>\n'/*ion-inline-end:"/Volumes/9rachata/Archive/navi-park/src/pages/general-profile/general-profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__["AngularFireAuth"],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1__ionic_native_screen_orientation__["a" /* ScreenOrientation */],
            __WEBPACK_IMPORTED_MODULE_0__ionic_native_native_storage__["a" /* NativeStorage */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["n" /* Platform */]])
    ], GeneralProfilePage);
    return GeneralProfilePage;
}());

//# sourceMappingURL=general-profile.js.map

/***/ }),

/***/ 158:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchFavoritePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_native_screen_orientation__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__general_peview_parking_general_peview_parking__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_search_data_search_data__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__search_all_search_all__ = __webpack_require__(161);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var SearchFavoritePage = /** @class */ (function () {
    function SearchFavoritePage(navCtrl, navParams, dataService, db, afAuth, toastCtrl, alertCtrl, screenOrientation) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.db = db;
        this.afAuth = afAuth;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.screenOrientation = screenOrientation;
        this.searchTerm = '';
        this.dataF = [];
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }
    SearchFavoritePage.prototype.onClickAdd = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__search_all_search_all__["a" /* SearchAllPage */]);
    };
    SearchFavoritePage.prototype.ngOnInit = function () {
        var _this = this;
        this.afAuth.authState.subscribe(function (data) {
            _this.uid = data.uid;
            _this.db.object('/data_user/' + data.uid + '/favorite').valueChanges().subscribe(function (res) {
                if (res) {
                    _this.dataF = [];
                    var key = Object.keys(res);
                    for (var i = 0; i < key.length; i++) {
                        var raw = {};
                        raw['title'] = res[key[i]]['place'];
                        raw['icon'] = 'md-close';
                        raw['key'] = key[i];
                        _this.dataF.push(raw);
                    }
                    if (_this.dataF != []) {
                        _this.setFilteredItems();
                    }
                }
            });
        });
    };
    SearchFavoritePage.prototype.setFilteredItems = function () {
        this.items = this.dataService.filterItems(this.searchTerm, this.dataF);
    };
    SearchFavoritePage.prototype.itemSelected = function (item, event) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_1__general_peview_parking_general_peview_parking__["a" /* GeneralPeviewParkingPage */], { pos: item['title'] });
        // this.navCtrl.push(GeneralStatusParkingPage , {pos : item['title']})
    };
    SearchFavoritePage.prototype.onClick = function (item, event) {
        event.stopPropagation();
        var itemsRef = this.db.list('/data_user/' + this.uid + '/favorite');
        itemsRef.remove(item['key']);
        this.toastCtrl.create({
            message: 'ลบข้อมูลเรียบร้อย',
            duration: 2000,
            position: 'bottom'
        }).present();
    };
    SearchFavoritePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_5__angular_core__["m" /* Component */])({
            selector: 'page-search-favorite',template:/*ion-inline-start:"/Volumes/9rachata/Archive/navi-park/src/pages/search-favorite/search-favorite.html"*/'<!--\n  Generated template for the SearchFavoritePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n\n\n    <ion-navbar color="navbar">\n        <ion-searchbar [(ngModel)]="searchTerm" (ionInput)="setFilteredItems()" placeholder="{{\'SEARCH\' | translate}}"></ion-searchbar>\n      </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n\n \n    <button ion-button outline round   icon-start  color="primary" (click)="onClickAdd()">\n        <ion-icon name="md-add"></ion-icon>{{\'ADD FAVORITE\' | translate}}\n    </button>\n\n    \n    <ion-list>\n\n        <button  ion-item *ngFor="let item of items" (click)="itemSelected(item , $event)" >\n                {{item.title}} <ion-icon color ="danger" [name]="item.icon" item-end (click)="onClick(item , $event)"></ion-icon>\n                   \n              \n        </button>\n\n\n\n    </ion-list>\n\n</ion-content>\n'/*ion-inline-end:"/Volumes/9rachata/Archive/navi-park/src/pages/search-favorite/search-favorite.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__providers_search_data_search_data__["a" /* SearchDataProvider */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_auth__["AngularFireAuth"],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_0__ionic_native_screen_orientation__["a" /* ScreenOrientation */]])
    ], SearchFavoritePage);
    return SearchFavoritePage;
}());

//# sourceMappingURL=search-favorite.js.map

/***/ }),

/***/ 159:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeneralPeviewParkingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_native_native_storage__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_screen_orientation__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__general_status_parking_general_status_parking__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_launch_navigator__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_Rxjs_rx__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_Rxjs_rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_Rxjs_rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__general_view_map_image_general_view_map_image__ = __webpack_require__(272);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the GeneralPeviewParkingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var GeneralPeviewParkingPage = /** @class */ (function () {
    function GeneralPeviewParkingPage(navCtrl, navParams, db, platform, launchNavigator, screenOrientation, nativeStorage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.db = db;
        this.platform = platform;
        this.launchNavigator = launchNavigator;
        this.screenOrientation = screenOrientation;
        this.nativeStorage = nativeStorage;
        this.data = {};
        this.viewStatus = __WEBPACK_IMPORTED_MODULE_2__general_status_parking_general_status_parking__["a" /* GeneralStatusParkingPage */];
        this.statuslot = true;
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }
    GeneralPeviewParkingPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.observableVar = __WEBPACK_IMPORTED_MODULE_7_Rxjs_rx__["Observable"].interval(2000).subscribe(function () {
            _this.nativeStorage.getItem('ln')
                .then((function (data) {
                if (data == "en") {
                    _this.titleShow = _this.dataReCheck['nameEN'];
                }
                else {
                    _this.titleShow = _this.dataReCheck['nameTH'];
                }
            }), function (error) { return console.error(error); });
        });
    };
    GeneralPeviewParkingPage.prototype.ionViewDidLeave = function () {
        this.observableVar.unsubscribe();
    };
    GeneralPeviewParkingPage.prototype.openImage = function () {
        this.navParams.get('pos');
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__general_view_map_image_general_view_map_image__["a" /* GeneralViewMapImagePage */], { 'data': this.navParams.get('pos') });
    };
    GeneralPeviewParkingPage.prototype.ngOnInit = function () {
        var _this = this;
        this.title = this.navParams.get('pos');
        this.nativeStorage.getItem('ln')
            .then((function (data) {
            if (data == "en") {
                _this.ln = "en";
            }
            else {
                _this.ln = "th";
            }
        }), function (error) { return console.error(error); });
        this.db.object('/' + this.title).valueChanges().subscribe(function (resF) {
            _this.dataReCheck = resF;
            var spaces = 0;
            var key = Object.keys(resF);
            if (_this.ln == "en") {
                _this.titleShow = resF['nameEN'];
            }
            else {
                _this.titleShow = resF['nameTH'];
            }
            for (var j = 0; j < key.length; j++) {
                if (key[j] != "A_Total Zone" && key[j] != "lat" && key[j] != "long" && key[j] != "image-zone" && key[j] != "opening-time" && key[j] != "price-range" && key[j] != "nameTH" && key[j] != "nameEN") {
                    spaces += resF[key[j]]['A_Available'];
                }
            }
            _this.data['price'] = resF['price-range'];
            _this.data['time'] = resF['opening-time'];
            _this.data['image'] = resF['image-zone'];
            _this.data['lat'] = resF['lat'];
            _this.data['lng'] = resF['long'];
            _this.data['totalZone'] = resF['A_Total Zone'];
            console.log(JSON.stringify(resF));
            if (spaces > 0) {
                _this.data['spaces'] = spaces;
                _this.statuslot = true;
            }
            else {
                _this.statuslot = false;
                // this.data['spaces'] = "ที่จอดรถเต็ม"
            }
        });
    };
    GeneralPeviewParkingPage.prototype.onClickShowMap = function () {
        this.launchNavigator.navigate([this.data['lat'], this.data['lng']]);
        ;
    };
    GeneralPeviewParkingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["m" /* Component */])({
            selector: 'page-general-peview-parking',template:/*ion-inline-start:"/Volumes/9rachata/Archive/navi-park/src/pages/general-peview-parking/general-peview-parking.html"*/'<!--\n  Generated template for the GeneralPeviewParkingPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color ="navbar">\n    <ion-title>{{titleShow}}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content no-padding class="cards-bg social-cards bg">\n    <ion-card *ngIf="data">      \n\n\n        <img [src]="data.image">\n      \n\n\n        <ion-item>\n          <ion-icon name="md-book" item-start large color="yellow"></ion-icon>\n          <h2>{{\'ALL\' | translate}} {{data.totalZone}} {{\'ZONE\' | translate}} </h2>\n          <p ion-text color="primary" (click)="openImage()">{{\'View parking map\' | translate}}</p>\n        </ion-item>\n        <ion-item>\n            <ion-icon name="ios-car" item-start large color="primary"></ion-icon>\n            <h2 ion-text color="secondary" *ngIf="statuslot">{{\'Available\' | translate}} {{data.spaces}} {{\'channels\' | translate}} </h2>\n\n            <h2  ion-text color="danger" *ngIf="!statuslot">{{\'Parking is not busy\' | translate}}</h2>\n          </ion-item>\n        <ion-item>\n            <ion-icon name="md-alarm" item-start large color="secondary"></ion-icon>\n            <h2>{{data.time}}</h2>\n          </ion-item>\n        \n          <ion-item>\n            <ion-icon name="logo-usd" item-start large color="danger"></ion-icon>\n            <h2>{{data.price}} {{\'baht\' | translate}}</h2>\n          </ion-item>\n\n\n           \n    \n\n\n            <ion-card-content>\n              <ion-row>\n                <ion-col col-6 text-left>\n                  <button ion-button color="primary"  round (click)="onClickShowMap()">\n                    \n                    {{\'navigate\' | translate}}\n                  </button>\n                </ion-col>\n                <ion-col col-6 text-right >\n                  <button ion-button color="secondary" outline round [navPush]="viewStatus" [navParams]="{pos : title}">\n                    \n                    {{\'View parking lot\' | translate}}\n                  </button>\n                </ion-col>\n                \n              </ion-row>\n            </ion-card-content>\n      \n\n        \n\n      \n      </ion-card>\n      \n      \n     \n      \n      </ion-content>\n      \n      <style>\n        .social-cards ion-col {\n          padding: 0;\n        }\n      </style>\n\n'/*ion-inline-end:"/Volumes/9rachata/Archive/navi-park/src/pages/general-peview-parking/general-peview-parking.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_launch_navigator__["a" /* LaunchNavigator */],
            __WEBPACK_IMPORTED_MODULE_1__ionic_native_screen_orientation__["a" /* ScreenOrientation */],
            __WEBPACK_IMPORTED_MODULE_0__ionic_native_native_storage__["a" /* NativeStorage */]])
    ], GeneralPeviewParkingPage);
    return GeneralPeviewParkingPage;
}());

//# sourceMappingURL=general-peview-parking.js.map

/***/ }),

/***/ 160:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeneralStatusParkingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_native_native_storage__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_launch_navigator__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_screen_orientation__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__general_view_zone_general_view_zone__ = __webpack_require__(270);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_Rxjs_rx__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_Rxjs_rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_Rxjs_rx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var GeneralStatusParkingPage = /** @class */ (function () {
    function GeneralStatusParkingPage(navCtrl, navParams, afDb, alertCtl, screenOrientation, launchNavigator, nativeStorage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.afDb = afDb;
        this.alertCtl = alertCtl;
        this.screenOrientation = screenOrientation;
        this.launchNavigator = launchNavigator;
        this.nativeStorage = nativeStorage;
        this.listDataShowZone = {};
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }
    GeneralStatusParkingPage.prototype.itemSelected = function (item) {
        var space = this.listDataShowZone['data'][item]['availOfZone'];
        var nameZone = this.listDataShowZone['data'][item]['nameZone'];
        var nameZoneS = this.listDataShowZone['data'][item]['nameZoneShow'];
        console.log(nameZoneS);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__general_view_zone_general_view_zone__["a" /* GeneralViewZonePage */], { 'nameZone': nameZone, 'nameMap': this.title, 'showName': nameZoneS });
    };
    GeneralStatusParkingPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.observableVar = __WEBPACK_IMPORTED_MODULE_7_Rxjs_rx__["Observable"].interval(2000).subscribe(function () {
            _this.nativeStorage.getItem('ln')
                .then((function (data) {
                if (data == "en") {
                    _this.titleShow = _this.dataReCheck['nameEN'];
                }
                else {
                    _this.titleShow = _this.dataReCheck['nameTH'];
                }
            }), function (error) { return console.error(error); });
        });
    };
    GeneralStatusParkingPage.prototype.ionViewDidLeave = function () {
        this.observableVar.unsubscribe();
    };
    GeneralStatusParkingPage.prototype.ngOnInit = function () {
        var _this = this;
        this.nativeStorage.getItem('ln')
            .then((function (data) {
            if (data == "en") {
                _this.ln = "en";
            }
            else {
                _this.ln = "th";
            }
        }), function (error) { return console.error(error); });
        this.title = this.navParams.get('pos');
        this.afDb.object('/' + this.title).valueChanges().subscribe(function (res) {
            _this.items = res;
            _this.dataReCheck = res;
            if (_this.ln == "en") {
                _this.titleShow = res['nameEN'];
            }
            else {
                _this.titleShow = res['nameTH'];
            }
            _this.dataManagement(res);
        });
    };
    GeneralStatusParkingPage.prototype.dataManagement = function (res) {
        this.listDataShowZone = {};
        this.listDataShowZone['total'] = res['A_Total Zone'];
        var keyMain = Object.keys(res);
        this.lat = res['lat'];
        this.lng = res['long'];
        var listDataShow = [];
        for (var i = 0; i < keyMain.length; i++) {
            if (keyMain[i] != 'A_Total Zone' && keyMain[i] != 'lat' && keyMain[i] != 'long' && keyMain[i] != "image-zone" && keyMain[i] != "opening-time" && keyMain[i] != "price-range" && keyMain[i] != "nameTH" && keyMain[i] != "nameEN") {
                var listAllZone = {};
                var nameZone = keyMain[i];
                var avail = res[keyMain[i]]['A_Available'];
                var total = res[keyMain[i]]['A_Total Lot'];
                var progress = ((total - avail) / total) * 100;
                // if(progress == 0){
                //   progress = -1
                // }nameZone
                listAllZone['nameZone'] = nameZone;
                listAllZone['nameZoneShow'] = res[keyMain[i]]['Name'];
                console.log("  " + res[keyMain[i]]['Name']);
                if (avail > 0) {
                    listAllZone['availOfZone'] = avail;
                    listAllZone['count'] = true;
                }
                else {
                    listAllZone['availOfZone'] = 0;
                    listAllZone['count'] = false;
                }
                listAllZone['totalOfZone'] = total;
                listAllZone['progress'] = progress;
                listDataShow.push(listAllZone);
            }
        }
        this.listDataShowZone['data'] = listDataShow;
        this.numberOfZone = Object.keys(this.listDataShowZone['data']).length;
    };
    GeneralStatusParkingPage.prototype.gotoMap = function () {
        this.launchNavigator.navigate([this.lat, this.lng]);
        ;
    };
    GeneralStatusParkingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_5__angular_core__["m" /* Component */])({
            selector: 'page-general-status-parking',template:/*ion-inline-start:"/Volumes/9rachata/Archive/navi-park/src/pages/general-status-parking/general-status-parking.html"*/'<!--\n  Generated template for the GeneralStatusParkingPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color ="navbar">\n    <ion-title>{{titleShow}}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding class="bgView">\n\n    <ion-grid>\n      <ion-row>\n        <ion-col >\n            <p ion-text color="light"><b>{{\'ALL\' | translate}} {{numberOfZone}} {{\'ZONE\' | translate}}</b> </p>\n        </ion-col>\n        <ion-col >\n            <button ion-button round full small  color = "primary" (click)="gotoMap()">{{\'navigate\' | translate}}</button>\n           </ion-col>\n      </ion-row>\n      <ion-row>\n        \n      </ion-row>\n    </ion-grid>\n \n    <ion-list *ngIf ="listDataShowZone" no-lines >\n\n    \n          <button class="item-content" ion-item *ngFor="let i of listDataShowZone.data; let j = index" (click)="itemSelected(j)" >\n              <h2 ion-text color="light"  item-start>{{\'ZONE\' | translate}} {{i.nameZoneShow}}</h2>\n              <h3 *ngIf="i.count" style="padding-top: 10px;" ion-text color="progressGreen" >{{\'Available\' | translate}} {{i.availOfZone}} {{\'channels\' | translate}}</h3>\n              <h3 *ngIf="!i.count" style="padding-top: 10px;" ion-text color="progressDanger" >{{\'Parking is not busy\' | translate}}</h3>\n              \n              <progress-bar [progress]="i.progress"></progress-bar>\n    \n          </button>\n    \n     \n      </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Volumes/9rachata/Archive/navi-park/src/pages/general-status-parking/general-status-parking.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_screen_orientation__["a" /* ScreenOrientation */],
            __WEBPACK_IMPORTED_MODULE_1__ionic_native_launch_navigator__["a" /* LaunchNavigator */],
            __WEBPACK_IMPORTED_MODULE_0__ionic_native_native_storage__["a" /* NativeStorage */]])
    ], GeneralStatusParkingPage);
    return GeneralStatusParkingPage;
}());

//# sourceMappingURL=general-status-parking.js.map

/***/ }),

/***/ 161:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchAllPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__general_peview_parking_general_peview_parking__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_search_data_search_data__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_screen_orientation__ = __webpack_require__(23);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the SearchAllPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SearchAllPage = /** @class */ (function () {
    function SearchAllPage(navCtrl, navParams, dataService, db, loadCtl, afAuth, toastCtrl, screenOrientation) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.dataService = dataService;
        this.db = db;
        this.loadCtl = loadCtl;
        this.afAuth = afAuth;
        this.toastCtrl = toastCtrl;
        this.screenOrientation = screenOrientation;
        this.searchTerm = '';
        this.items = [];
        this.data = [];
        this.dataF = [];
        this.dataFull = [];
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }
    SearchAllPage.prototype.ngOnInit = function () {
        var _this = this;
        this.afAuth.authState.subscribe(function (data) {
            _this.uid = data.uid;
        });
        this.db.object('/').valueChanges().subscribe(function (res) {
            var key = Object.keys(res);
            _this.data = [];
            for (var i = 0; i < key.length; i++) {
                if (key[i] != "data_user" && key[i] != "data") {
                    var raw = {};
                    raw['title'] = res[key[i]]['nameTH'];
                    _this.data.push(raw);
                }
            }
            if (_this.data) {
                _this.db.object('/data_user/' + _this.uid + '/favorite').valueChanges().subscribe(function (resF) {
                    _this.dataF = [];
                    _this.dataFull = [];
                    if (resF) {
                        var keyF = Object.keys(resF);
                        var keyCheckF = [];
                        for (var j = 0; j < keyF.length; j++) {
                            var rawF = {};
                            rawF['place'] = resF[keyF[j]]['place'];
                            rawF['key'] = keyF[j];
                            keyCheckF.push(rawF['place']);
                            _this.dataF.push(rawF);
                        }
                        for (var k = 0; k < key.length; k++) {
                            if (key[k] != "data" && key[k] != "data_user") {
                                var a = keyCheckF.indexOf(key[k]);
                                var rawFull = {};
                                if (a != -1) {
                                    rawFull['title'] = key[k];
                                    rawFull['key'] = _this.dataF[a]['key'];
                                    rawFull['icon'] = 'md-star';
                                    _this.dataFull.push(rawFull);
                                }
                                else {
                                    rawFull['title'] = key[k];
                                    rawFull['key'] = '-1';
                                    rawFull['icon'] = 'md-star-outline';
                                    _this.dataFull.push(rawFull);
                                }
                            }
                        }
                    }
                    else {
                        for (var u = 0; u < key.length; u++) {
                            if (key[u] != "data" && key[u] != "data_user") {
                                var rawOnFavorite = {};
                                rawOnFavorite['title'] = key[u];
                                rawOnFavorite['key'] = '-1';
                                rawOnFavorite['icon'] = 'md-star-outline';
                                _this.dataFull.push(rawOnFavorite);
                            }
                        }
                    }
                    if (_this.dataFull) {
                        _this.setFilteredItems();
                    }
                });
            }
        });
    };
    SearchAllPage.prototype.setFilteredItems = function () {
        this.items = this.dataService.filterItems(this.searchTerm, this.dataFull);
    };
    SearchAllPage.prototype.onClick = function (item, event) {
        event.stopPropagation();
        if (item['key'] == '-1') {
            var itemsRef = this.db.list('/data_user/' + this.uid + '/favorite');
            itemsRef.push({ place: item['title'] });
            this.toastCtrl.create({
                message: 'บันทึกข้อมูลเรียบร้อย',
                duration: 2000,
                position: 'bottom'
            }).present();
        }
        else {
            var itemsRef = this.db.list('/data_user/' + this.uid + '/favorite');
            itemsRef.remove(item['key']);
            this.toastCtrl.create({
                message: 'ลบข้อมูลเรียบร้อย',
                duration: 2000,
                position: 'bottom'
            }).present();
        }
        // this.navCtrl.push(GeneralStatusParkingPage , {pos : this.listData[i].name})
    };
    SearchAllPage.prototype.itemSelected = function (item, $event) {
        // this.navCtrl.push(GeneralStatusParkingPage , {pos : item['title']})
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_0__general_peview_parking_general_peview_parking__["a" /* GeneralPeviewParkingPage */], { pos: item['title'] });
    };
    SearchAllPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["m" /* Component */])({
            selector: 'page-search-all',template:/*ion-inline-start:"/Volumes/9rachata/Archive/navi-park/src/pages/search-all/search-all.html"*/'<!--\n  Generated template for the SearchAllPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <ion-navbar color="navbar">\n      <ion-searchbar [(ngModel)]="searchTerm" (ionInput)="setFilteredItems()" placeholder="{{\'SEARCH\' | translate}}"></ion-searchbar>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    \n \n    <ion-list no-lines>\n        <button  ion-item *ngFor="let item of items" (click)="itemSelected(item , $event)" >\n                {{item.title}}<ion-icon color ="yellow" [name]="item.icon" item-end (click)="onClick(item , $event)"></ion-icon>\n          \n        </button>\n\n        <!-- <ion-item *ngFor="let item of items" (click)="itemSelected(item , $event)">\n            {{item.title}}<ion-icon color ="yellow" [name]="item.icon" item-end (click)="onClick(item , $event)"></ion-icon>\n        </ion-item> -->\n    </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Volumes/9rachata/Archive/navi-park/src/pages/search-all/search-all.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_search_data_search_data__["a" /* SearchDataProvider */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__["AngularFireAuth"],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_screen_orientation__["a" /* ScreenOrientation */]])
    ], SearchAllPage);
    return SearchAllPage;
}());

//# sourceMappingURL=search-all.js.map

/***/ }),

/***/ 162:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeneralFirstPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_auth__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__general_home_general_home__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_screen_orientation__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__node_modules_ngx_translate_core__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__node_modules_ionic_native_native_storage__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_Rxjs_rx__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_Rxjs_rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_Rxjs_rx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the GeneralFirstPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var GeneralFirstPage = /** @class */ (function () {
    function GeneralFirstPage(navCtrl, navParams, anth, db, loadCtl, screenOrientation, translate, nativeStorage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.anth = anth;
        this.db = db;
        this.loadCtl = loadCtl;
        this.screenOrientation = screenOrientation;
        this.translate = translate;
        this.nativeStorage = nativeStorage;
        this.homeMap = __WEBPACK_IMPORTED_MODULE_1__general_home_general_home__["a" /* GeneralHomePage */];
        this.listBook = [];
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }
    GeneralFirstPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.observableVar = __WEBPACK_IMPORTED_MODULE_8_Rxjs_rx__["Observable"].interval(3000).subscribe(function () {
            _this.nativeStorage.getItem('ln')
                .then((function (data) {
                if (data == "en") {
                }
                else {
                }
            }), function (error) { return console.error(error); });
        });
    };
    GeneralFirstPage.prototype.ionViewDidLeave = function () {
        this.observableVar.unsubscribe();
    };
    GeneralFirstPage.prototype.ngOnInit = function () {
        var _this = this;
        this.nativeStorage.getItem('ln')
            .then((function (data) {
            if (data == "en") {
                _this.loadMain = _this.loadCtl.create({
                    content: "Please wait..."
                });
                _this.loadMain.present();
            }
            else {
                _this.loadMain = _this.loadCtl.create({
                    content: "โปรดสักครู่..."
                });
                _this.loadMain.present();
            }
        })), function (error) { return console.error(error); };
        this.anth.authState.subscribe(function (data) {
            _this.db.object('/data_user/' + data['uid']).valueChanges().subscribe(function (res) {
                _this.name = res['displayname'];
                _this.regisDateTime = _this.Unix_timestamp(res['create'] / 1000000);
                _this.laseLoginDateTime = _this.Unix_timestamp(res['lastUsed'] / 1000000);
                if (_this.name) {
                    _this.loadMain.dismiss();
                }
            });
            _this.db.object('/').valueChanges().subscribe(function (dataBook) {
                _this.listBook = [];
                var key = Object.keys(dataBook);
                for (var i = 0; i < key.length; i++) {
                    if (key[i] != "data" && key[i] != "data_user") {
                        var keyZone = Object.keys(dataBook[key[i]]);
                        for (var j = 0; j < keyZone.length; j++) {
                            if (keyZone[j] != "A_Total Zone" && keyZone[j] != "lat" && keyZone[j] != "long" && keyZone[j] != "nameEN"
                                && keyZone[j] != "nameTH" && keyZone[j] != "opening-time" && keyZone[j] != "price-range" && keyZone[j] != "image-zone") {
                                var keyLot = (Object.keys(dataBook[key[i]][keyZone[j]]));
                                for (var k = 0; k < keyLot.length; k++) {
                                    if (keyLot[k] != "A_Available" && keyLot[k] != "A_Total Lot" && keyLot[k] != "Name" && keyLot[k] != "URL") {
                                        var checkKeyUid = ((Object.keys(dataBook[key[i]][keyZone[j]][keyLot[k]])));
                                        for (var l = 0; l < checkKeyUid.length; l++) {
                                            if (checkKeyUid[l] == "uid") {
                                                if (dataBook[key[i]][keyZone[j]][keyLot[k]]['uid'] == data['uid']) {
                                                    var raw = {};
                                                    raw['nameTH'] = dataBook[key[i]]['nameTH'];
                                                    raw['nameEN'] = dataBook[key[i]]['nameEN'];
                                                    raw['startTime'] = dataBook[key[i]][keyZone[j]][keyLot[k]]['startDateTime'];
                                                    raw['endTime'] = dataBook[key[i]][keyZone[j]][keyLot[k]]['endDateTime'];
                                                    _this.listBook.push(raw);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            });
        });
    };
    GeneralFirstPage.prototype.Unix_timestamp = function (t) {
        var dt = new Date(t * 1000);
        var dat = "0" + dt.getDate();
        var mm = "0" + dt.getMonth();
        var y = dt.getFullYear();
        var hr = dt.getHours();
        var m = "0" + dt.getMinutes();
        var s = "0" + dt.getSeconds();
        return dat.substr(-2) + '/' + mm.substr(-2) + '/' + y + ' ' + hr + ':' + m.substr(-2) + ':' + s.substr(-2);
    };
    GeneralFirstPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["m" /* Component */])({
            selector: 'page-general-first',template:/*ion-inline-start:"/Volumes/9rachata/Archive/navi-park/src/pages/general-first/general-first.html"*/'<ion-header>\n\n    <ion-navbar color="navbar">\n\n        <button ion-button menuToggle icon-only>\n            <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title>{{\'HOME\' | translate}}</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding class="bg">\n\n\n\n    <ion-list no-lines>\n        <h1 ion-text color="light" >{{ \'WELCOME\' | translate }} :\n                    <b>{{name}}</b>\n        </h1>\n        <ion-item class="item-content" style="margin-top: 20px;">\n            <h3 style="padding-top: 5px;" ion-text color="light">{{ \'Active access\' | translate }} : {{laseLoginDateTime}}</h3>\n        </ion-item>\n    </ion-list>\n\n        <h2 ion-text color="light" style="padding-left: 0px;" >\n            <b>{{ \'Advance booking\' | translate }}</b>\n        </h2>\n\n        <ion-list no-lines>\n        <ion-item  *ngFor="let data of listBook " class="item-content" style="margin-top: 0px;">\n            <h3 style="padding-top: 5px;" ion-text color="light" >{{data.nameEN}} {{data.startTime}} -  {{data.endTime}}</h3>\n        </ion-item>\n    </ion-list>\n\n        <!-- <ion-list-header>\n                Action\n        </ion-list-header>\n        <ion-item>Terminator II</ion-item>\n        <ion-item>The Empire Strikes Back</ion-item>\n        <ion-item>Blade Runner</ion-item> -->\n\n\n\n\n\n\n\n    \n    <!-- <img src="assets/imgs/true01.jpg">\n    <img src="assets/imgs/true2.jpg"> -->\n\n\n\n</ion-content>\n\n<!-- <ion-footer>\n    <img src="assets/imgs/true01.jpg"> \n</ion-footer> -->'/*ion-inline-end:"/Volumes/9rachata/Archive/navi-park/src/pages/general-first/general-first.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_0_angularfire2_auth__["AngularFireAuth"],
            __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_screen_orientation__["a" /* ScreenOrientation */],
            __WEBPACK_IMPORTED_MODULE_6__node_modules_ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_7__node_modules_ionic_native_native_storage__["a" /* NativeStorage */]])
    ], GeneralFirstPage);
    return GeneralFirstPage;
}());

//# sourceMappingURL=general-first.js.map

/***/ }),

/***/ 266:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeneralAccessToParkingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__node_modules_ionic_native_screen_orientation__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_barcode_scanner__ = __webpack_require__(594);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__node_modules_ionic_native_native_storage__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the GeneralAccessToParkingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var GeneralAccessToParkingPage = /** @class */ (function () {
    function GeneralAccessToParkingPage(navCtrl, navParams, screenOrientation, alertCtrl, barcodeScanner, anth, db, nativeStorage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.screenOrientation = screenOrientation;
        this.alertCtrl = alertCtrl;
        this.barcodeScanner = barcodeScanner;
        this.anth = anth;
        this.db = db;
        this.nativeStorage = nativeStorage;
    }
    GeneralAccessToParkingPage.prototype.ngOnInit = function () {
        var _this = this;
        this.anth.authState.subscribe(function (data) {
            _this.uid = data['uid'];
        });
        this.nativeStorage.getItem('ln')
            .then((function (data) {
            if (data == "en") {
                _this.ln = 'en';
            }
            else {
                _this.ln = 'th';
            }
        }), function (error) { return console.error(error); });
    };
    GeneralAccessToParkingPage.prototype.scanQRCode = function () {
        var _this = this;
        console.log("OK");
        this.barcodeScanner.scan().then(function (barcodeData) {
            console.log('Barcode data', JSON.stringify(barcodeData));
            _this.arrayOfStrings = barcodeData['text'].split(';');
            var a = _this.db.object('/' + _this.arrayOfStrings[0] + '/' + _this.arrayOfStrings[1] + '/' + _this.arrayOfStrings[2]).valueChanges().subscribe(function (res) {
                if (res['priceStatus'] == 0) {
                    if (_this.ln == 'th') {
                        _this.alertCtrl.create({
                            title: "ผิดพลาด",
                            message: "กรุณาจ่ายเงินก่อนเข้าที่จอดรถ"
                        }).present();
                    }
                    else {
                        _this.alertCtrl.create({
                            title: "Error",
                            message: "Please pay before parking."
                        }).present();
                    }
                }
                else {
                    // this.checkOpen(res);
                    a.unsubscribe();
                }
            });
        }).catch(function (err) {
            console.log('Error', err);
        });
    };
    GeneralAccessToParkingPage.prototype.checkOpen = function (res) {
        var _this = this;
        if (this.ln == 'th') {
            if (this.uid == res['uid']) {
                this.db.object('/' + this.arrayOfStrings[0] + '/' + this.arrayOfStrings[1] + '/' + this.arrayOfStrings[2])
                    .update({ 'Reserve Status': 0 })
                    .then(function () {
                    _this.alertCtrl.create({
                        title: "สำเร็จ",
                        message: "ระบุตัวตนสำเร็จ ท่านสามารถเข้าที่จอดรถของท่านได้แล้ว"
                    }).present();
                });
            }
            else {
                this.alertCtrl.create({
                    title: "เกิดข้อผิดพลาด",
                    message: "ระบุตัวตนไม่สำเร็จ ท่านไม่สามารถเข้าที่จองได้"
                }).present();
            }
        }
        else {
            if (this.uid == res['uid']) {
                this.db.object('/' + this.arrayOfStrings[0] + '/' + this.arrayOfStrings[1] + '/' + this.arrayOfStrings[2])
                    .update({ 'Reserve Status': 0 })
                    .then(function () {
                    _this.alertCtrl.create({
                        title: "Success",
                        message: "Identify Success You have access to your car park."
                    }).present();
                });
            }
            else {
                this.alertCtrl.create({
                    title: "Error",
                    message: "Identity is not successful. You can not access the car park."
                }).present();
            }
        }
    };
    GeneralAccessToParkingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["m" /* Component */])({
            selector: 'page-general-access-to-parking',template:/*ion-inline-start:"/Volumes/9rachata/Archive/navi-park/src/pages/general-access-to-parking/general-access-to-parking.html"*/'<!--\n  Generated template for the GeneralAccessToParkingPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="navbar">\n    <ion-title>{{ \'Access To Parking\' | translate }}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content >\n\n  <button ion-button full block round (click)="scanQRCode()">{{ \'Scan\' | translate }}</button>\n</ion-content>\n'/*ion-inline-end:"/Volumes/9rachata/Archive/navi-park/src/pages/general-access-to-parking/general-access-to-parking.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__node_modules_ionic_native_screen_orientation__["a" /* ScreenOrientation */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
            __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__["AngularFireAuth"],
            __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_6__node_modules_ionic_native_native_storage__["a" /* NativeStorage */]])
    ], GeneralAccessToParkingPage);
    return GeneralAccessToParkingPage;
}());

//# sourceMappingURL=general-access-to-parking.js.map

/***/ }),

/***/ 267:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AgreePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_native_screen_orientation__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the AgreePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AgreePage = /** @class */ (function () {
    function AgreePage(navCtrl, navParams, auth, alertCtl, loadCtl, adb, screenOrientation) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.alertCtl = alertCtl;
        this.loadCtl = loadCtl;
        this.adb = adb;
        this.screenOrientation = screenOrientation;
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }
    AgreePage.prototype.ngOnInit = function () {
        this.email = this.navParams.get('email');
        this.pass = this.navParams.get('pass');
        this.dfname = this.navParams.get('dfname');
        this.ddname = this.navParams.get('ddname');
        this.dphone = this.navParams.get('dphone');
        this.dcar = this.navParams.get('dcar');
        this.dmodel = this.navParams.get('dModelCar');
    };
    AgreePage.prototype.onClick = function (op) {
        var _this = this;
        if (op == "Disagree") {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
        }
        else {
            var load_1 = this.loadCtl.create({
                content: 'Please wait...'
            });
            load_1.present();
            this.auth.signup(this.email, this.pass)
                .then(function (data) {
                var itemsRef = _this.adb.list('data_user');
                var start = Date.now();
                itemsRef.set(data.user.uid, { fullname: _this.dfname, displayname: _this.ddname, phone: _this.dphone, car: _this.dcar, carmodel: _this.dmodel, create: start * 1000 });
                _this.auth.sendmail().then(function () {
                    load_1.dismiss();
                });
                _this.alertCtl.create({
                    title: "Registration",
                    message: "Please confirm your email address. Via email you registered"
                }).present();
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
            })
                .catch(function (error) {
                load_1.dismiss();
                var alert = _this.alertCtl.create({
                    title: "Registration failed!!",
                    message: error.message,
                    buttons: ['Ok']
                });
                alert.present();
            });
        }
    };
    AgreePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["m" /* Component */])({
            selector: 'page-agree',template:/*ion-inline-start:"/Volumes/9rachata/Archive/navi-park/src/pages/agree/agree.html"*/'\n\n\n<ion-content class="social-cards bg">\n\n\n\n      <ion-card>\n          <ion-card-content>\n            <p>Notice to user: Please read this software license agreement carefully. By using all or any portion of the software, you are agreeing to be bound by all the terms and conditions of this agreement. If you do not agree to any terms of this agreement, do not use the software.</p>\n            <h2><b>1 Definition</b></h2>\n             <p>1.1 "use" or "using" means to access, install, download, copy, or otherwise benefit from using the functionality of the Software.</p>\n            <p>1.2 "The Software" means the software and all associated documentation provided by KEYENCE.</p>\n            <h2><b>2 Grant of License.</b></h2>\n            <p>Conditioned upon compliance with all of the terms and conditions of this Agreement, KEYENCE grants you a nonexclusive and nontransferable license to install the Software on a single computer in order to use the KEYENCE product. You may make one copy of the Software for backup or archive purposes only.</p>\n            <h2><b>3 Restrictions.</b></h2>\n            <p>3.1 Except for installation of updates or new functions provided by KEYENCE, you may not modify or add any function to the Software.</p>\n            <p>3.2 You may not reverse engineer, decompile or disassemble the Software.</p>\n            <p>3.3 You may not create derivative works based on the Software.</p>\n            <p>3.4 Other than expressly stated by KEYENCE, you may not resell, retransfer, rent or otherwise redistribute the Software to any third parties.</p>\n            <h2><b>4 Intellectual Property Rights.</b></h2>\n            <p>Except as expressly stated herein, KEYENCE reserves all right, title and interest in the Software, and all associated copyrights, trademarks, and other intellectual property rights therein.</p>\n            <h2><b>5 Disclaimer.</b></h2>\n            <p>Keyence is licensing this the Software to you "AS IS" and without any warranty of any kind. In no event will KEYENCE or its suppliers be liable to you for any damages, claims, costs or any lost profits caused by using the Software.</p>\n            <h2><b>6 Termination.</b></h2>\n            <p>6.1 Your license under this Agreement will terminate automatically if you destroy the Software and the copy of the Software in your possession or voluntarily return the Software to us.</p>\n            <p>6.2 Your license under this Agreement will terminate automatically without any notice from KEYENCE if you fail to comply with any of the terms and conditions of this Agreement. Promptly upon termination, you shall cease all use of the Software and destroy all copies, full or partial, of the Software in your possession or control.</p>\n            <ion-grid>\n                <ion-row>\n                  <ion-col col-6 text-left >\n                    <button ion-button outline color="danger" (click)="onClick(\'Disagree\')">Disagree</button>\n                  </ion-col>\n                  <ion-col col-6 text-right >\n                      <button ion-button outline color="secondary" (click)="onClick(\'Agree\')">Agree</button>\n                  </ion-col>\n                </ion-row>\n              </ion-grid>\n          </ion-card-content>\n      </ion-card>\n      \n\n</ion-content>\n\n<style>\n    .social-cards ion-col {\n      padding: 0;\n    }\n  </style>\n'/*ion-inline-end:"/Volumes/9rachata/Archive/navi-park/src/pages/agree/agree.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_angularfire2_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_0__ionic_native_screen_orientation__["a" /* ScreenOrientation */]])
    ], AgreePage);
    return AgreePage;
}());

//# sourceMappingURL=agree.js.map

/***/ }),

/***/ 268:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_native_in_app_browser__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the PaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PaymentPage = /** @class */ (function () {
    function PaymentPage(navCtrl, navParams, iab) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.iab = iab;
    }
    PaymentPage.prototype.ionViewDidLoad = function () {
        var browser = this.iab.create('http://192.168.1.104/checkout/index1.html', '_self', { location: 'no', toolbar: 'no', zoom: 'no' });
    };
    PaymentPage.prototype.payments = function () {
        var browser = this.iab.create('http://192.168.1.104/checkout/index1.html', '_self', { location: 'no', toolbar: 'no', zoom: 'no' });
    };
    PaymentPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-payment',template:/*ion-inline-start:"/Volumes/9rachata/Archive/navi-park/src/pages/payment/payment.html"*/'<!--\n  Generated template for the PaymentPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="navbar">\n    <ion-title>{{ \'payment\' | translate }}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <button ion-button full block round (click)="payments()">{{ \'payment\' | translate }}</button>\n</ion-content>\n'/*ion-inline-end:"/Volumes/9rachata/Archive/navi-park/src/pages/payment/payment.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_0__ionic_native_in_app_browser__["a" /* InAppBrowser */]])
    ], PaymentPage);
    return PaymentPage;
}());

//# sourceMappingURL=payment.js.map

/***/ }),

/***/ 269:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeneralContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_native_screen_orientation__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the GeneralContactPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var GeneralContactPage = /** @class */ (function () {
    function GeneralContactPage(navCtrl, navParams, db, toastCtrl, alertCtrl, screenOrientation) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.db = db;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.screenOrientation = screenOrientation;
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }
    GeneralContactPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad GeneralContactPage');
    };
    GeneralContactPage.prototype.submitContact = function (from) {
        var name = from.value.name;
        var subject = from.value.subject;
        var detail = from.value.detail;
        var email = from.value.email;
        var tel = from.value.tel;
        var itemsRef = this.db.list('/data/contact');
        itemsRef.push({ name: name, subject: subject, detail: detail, email: email, tel: tel });
        from.reset();
        this.alertCtrl.create({
            title: "สำเร็จ",
            message: "ส่งข้อมูลให้ผู้บริการแล้ว"
        }).present();
    };
    GeneralContactPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["m" /* Component */])({
            selector: 'page-general-contact',template:/*ion-inline-start:"/Volumes/9rachata/Archive/navi-park/src/pages/general-contact/general-contact.html"*/'<!--\n  Generated template for the GeneralContactPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="navbar">\n      <button ion-button menuToggle icon-only>\n          <ion-icon name="menu"></ion-icon>\n</button>\n\n\n\n\n    <ion-title>Contact Us</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding class="bg">\n\n\n    <div class="login-content">\n\n         \n                    <form class="list-form" #f="ngForm" (ngSubmit)="submitContact(f)">\n                      \n                     <ion-item   class="borders" no-lines style="margin-bottom: 10px;">\n                       <ion-label >{{\'NAME\' | translate}}</ion-label>\n                       <ion-input STYLE=\'color:#FFFFFF;\'\n                            name="name"\n                            type="text" \n                            ngModel\n                            required></ion-input>\n                     </ion-item>\n                 \n                     <ion-item class="borders" no-lines style="margin-bottom: 10px;">\n                         <ion-label >{{\'SUBJECT\' | translate}}</ion-label>\n                         <ion-input  \n                            STYLE=\'color:#FFFFFF\' \n                            type="text" \n                            name="subject"\n                            ngModel\n                            required>\n                            </ion-input>\n                     </ion-item>\n                 \n                 \n                     <ion-item class="borders" no-lines style="margin-bottom: 10px;">\n                         <ion-label >{{\'DETAIL\' | translate}}</ion-label>\n                         <ion-textarea  \n                            style="height: 100px; color:#FFFFFF;"\n                            name="detail"\n                            ngModel\n                            required></ion-textarea>\n                     </ion-item>\n                 \n                 \n                     <ion-item class="borders" no-lines style="margin-bottom: 10px;">\n                         <ion-label >{{\'E-mail\' | translate}}</ion-label>\n                         <ion-input \n                            STYLE=\'color:#FFFFFF\' \n                            type="email" \n                            name="email"\n                            pattern="[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})"\n                            ngModel\n                            required></ion-input>\n                     </ion-item>\n                 \n                     <ion-item class="borders" no-lines style="margin-bottom: 10px;">\n                         <ion-label >{{\'TELEPHONE NUMBER\' | translate}}</ion-label>\n                         <ion-input STYLE=\'color:#FFFFFF\'  \n                            type="number" \n                            name="tel"\n                            ngModel\n                            required></ion-input>\n                     </ion-item>\n\n\n                     <ion-item no-lines style="background: transparent">\n                            \n                        <button [disabled]="!f.valid" ion-button round   icon-start block color="primary" tappable  type="submit">\n                            <ion-icon name="ios-checkmark-circle-outline"></ion-icon>\n                            {{\'SEND\' | translate}}\n                        </button>\n                               \n                     </ion-item>\n\n\n\n                     \n                     \n                    </form>\n                \n\n    </div>\n  \n\n</ion-content>\n'/*ion-inline-end:"/Volumes/9rachata/Archive/navi-park/src/pages/general-contact/general-contact.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_angularfire2_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_0__ionic_native_screen_orientation__["a" /* ScreenOrientation */]])
    ], GeneralContactPage);
    return GeneralContactPage;
}());

//# sourceMappingURL=general-contact.js.map

/***/ }),

/***/ 270:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeneralViewZonePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_native_native_storage__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__general_zone_confirm_general_zone_confirm__ = __webpack_require__(271);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_screen_orientation__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_d3_selection__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_d3_zoom__ = __webpack_require__(961);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the GeneralViewZonePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var GeneralViewZonePage = /** @class */ (function () {
    function GeneralViewZonePage(navCtrl, navParams, loadCtl, alertCtl, afDb, platform, screenOrientation, anth, nativeStorage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadCtl = loadCtl;
        this.alertCtl = alertCtl;
        this.afDb = afDb;
        this.platform = platform;
        this.screenOrientation = screenOrientation;
        this.anth = anth;
        this.nativeStorage = nativeStorage;
        this.ratio = 0;
        this.listData = {};
        this.checkDraw = false;
        this.x = 0;
        this.y = 0;
        this.k = 1;
        this.checkLoadImg = false;
        this.checkFirstDraw = false;
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }
    GeneralViewZonePage.prototype.dataManagement = function (data) {
        var _this = this;
        var keyMain = Object.keys(data);
        this.listData = {};
        var listRaw = [];
        this.listData = [];
        for (var i = 0; i < keyMain.length; i++) {
            if (keyMain[i] != 'A_Available' && keyMain[i] != 'A_Total Lot' && keyMain[i] != 'Name' && keyMain[i] != "nameTH" && keyMain[i] != "nameEN") {
                var dataDict = {};
                dataDict['nameCH'] = keyMain[i];
                dataDict['status'] = data[keyMain[i]]['Park Status'];
                dataDict['priceStatus'] = data[keyMain[i]]['priceStatus'];
                dataDict['uid'] = data[keyMain[i]]['uid'];
                dataDict['statusReserve'] = data[keyMain[i]]['Reserve Status'];
                dataDict['type'] = data[keyMain[i]]['Reserveable'];
                dataDict['x'] = data[keyMain[i]]['PosX1'];
                dataDict['y'] = data[keyMain[i]]['PosY1'];
                dataDict['chh'] = data[keyMain[i]]['CHH'];
                dataDict['chw'] = data[keyMain[i]]['CHW'];
                dataDict['angle'] = data[keyMain[i]]['angle'];
                dataDict['accessX1'] = data[keyMain[i]]['AccessX1'];
                dataDict['accessY1'] = data[keyMain[i]]['AccessY1'];
                dataDict['accessX2'] = data[keyMain[i]]['AccessX2'];
                dataDict['accessY2'] = data[keyMain[i]]['AccessY2'];
                listRaw.push(dataDict);
            }
        }
        this.listData['url'] = data['URL'];
        this.listData['data'] = listRaw;
        this._CANVAS = __WEBPACK_IMPORTED_MODULE_7_d3_selection__["f" /* select */]("canvas").call(__WEBPACK_IMPORTED_MODULE_8_d3_zoom__["a" /* zoom */]().scaleExtent([0.5, 8]).on("zoom", function () {
            var transform = __WEBPACK_IMPORTED_MODULE_7_d3_selection__["b" /* event */].transform;
            console.log(JSON.stringify(transform));
            _this._CONTEXT.save();
            _this.x = transform.x;
            _this.y = transform.y;
            _this.k = transform.k;
            _this._CONTEXT.clearRect(0, 0, _this.screenW, _this.screenH);
            _this._CONTEXT.translate(transform.x, transform.y);
            _this._CONTEXT.scale(transform.k, transform.k);
            _this.setupCanvas();
            _this._CONTEXT.restore();
        }));
        if (this.title && this.listData) {
            if (this.checkDraw == false) {
                this.checkDraw = true;
                var img = new Image();
                img.onload = function () {
                    console.log("HHHHHHH " + img.height + " WWWWW " + img.width);
                };
                img.src = this.listData['url'];
                this._CANVAS = this.canvasEl.nativeElement;
                var imageW = 1440;
                var imageH = 2940;
                this.screenW = this.platform.width();
                this.screenH = this.platform.height() - ((20 / 100) * this.platform.height());
                var n1 = this.getMax(this.getMax(imageW, imageH), this.getMax(this.screenW, this.screenH));
                var n2 = this.getMin(this.getMax(imageW, imageH), this.getMax(this.screenW, this.screenH));
                var ratioW = (n1) / (n2);
                var n2 = this.getMax(this.getMin(imageW, imageH), this.getMin(this.screenW, this.screenH));
                var n3 = this.getMin(this.getMin(imageW, imageH), this.getMin(this.screenW, this.screenH));
                var ratioH = (n2) / (n3);
                var imageScale = this.getMax(ratioW, ratioH);
                this.canvasW = imageW / imageScale;
                this.canvasH = imageH / imageScale;
                this._CANVAS.width = this.screenW;
                this._CANVAS.height = this.screenH;
                this._CANVAS.addEventListener("mousedown", function () {
                    for (var i = 0, len = _this.listData['data'].length; i < len; i++) {
                        var w = (_this.listData['data'][i].chw / (img.width / _this.canvasW));
                        var h = (_this.listData['data'][i].chh / (img.height / _this.canvasH));
                        var x = (_this.listData['data'][i].accessX1 / (img.width / _this.canvasW));
                        var y = (_this.listData['data'][i].accessY1 / (img.height / _this.canvasH));
                        // var left = this.listData['data'][i].accessX1 , right = this.listData['data'][i].accessX1+this.listData['data'][i].chw;
                        // var top = this.listData['data'][i].accessY1 , bottom = this.listData['data'][i].accessY1+this.listData['data'][i].chh;
                        var left, right, top, bottom;
                        left = x;
                        right = x + w;
                        top = y;
                        bottom = y + h;
                        if (_this.k == 1 && _this.x == 0 && _this.y == 0) {
                            left = left + _this.imgPosX,
                                right = right + _this.imgPosX;
                            top = top + _this.imgPosY,
                                bottom = bottom + _this.imgPosY;
                        }
                        else if (_this.k == 1 && _this.x != 0 && _this.y != 0) {
                            left = left + _this.imgPosX + _this.x,
                                right = right + _this.imgPosX + _this.x;
                            top = top + _this.imgPosY + _this.y,
                                bottom = bottom + _this.imgPosY + _this.y;
                        }
                        else {
                            left = ((left + _this.imgPosX) * _this.k) + _this.x;
                            right = ((right + _this.imgPosX) * _this.k) + _this.x;
                            top = ((top + _this.imgPosY) * _this.k) + _this.y;
                            bottom = ((bottom + _this.imgPosY) * _this.k) + _this.y;
                        }
                        if (right >= event['offsetX'] && left <= event['offsetX'] && bottom >= event['offsetY'] && top <= event['offsetY']) {
                            // CH: this.listData['data'][i]['nameCH'],
                            // nameParking: this.title,
                            // status: this.listData['data'][i]['status'],
                            // zone: this.nameMap
                            // this.alertCtl.create({
                            //         title: "Data",
                            //         message: this.listData['data'][i]['nameCH']
                            //       }).present();
                            if (_this.listData['data'][i]['status'] == 1 || _this.listData['data'][i]['statusReserve'] == 1) {
                                if (_this.listData['data'][i]['uid'] != _this.uid) {
                                    _this.alertCtl.create({
                                        title: "จองไม่สำเร็จ",
                                        message: "ช่องจอดรถไม่สามารถจองได้ เนื่องจากช่องจอดรถมีคนจองแล้ว"
                                    }).present();
                                }
                                else {
                                    if (_this.listData['data'][i]['status'] != 1 && _this.listData['data'][i]['priceStatus'] == 0) {
                                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__general_zone_confirm_general_zone_confirm__["a" /* GeneralZoneConfirmPage */], {
                                            CH: _this.listData['data'][i]['nameCH'],
                                            nameParking: _this.title,
                                            status: _this.listData['data'][i]['status'],
                                            zone: _this.nameMap
                                        });
                                    }
                                }
                            }
                            else {
                                if (_this.listData['data'][i]['type'] != 1) {
                                    _this.alertCtl.create({
                                        title: "จองไม่สำเร็จ",
                                        message: "ช่องจอดรถไม่สามารถจองได้ คุณสามารถจองได้เฉพาะช่องจอดที่มีสัญลักษณ์ R"
                                    }).present();
                                }
                                else {
                                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__general_zone_confirm_general_zone_confirm__["a" /* GeneralZoneConfirmPage */], {
                                        CH: _this.listData['data'][i]['nameCH'],
                                        nameParking: _this.title,
                                        status: _this.listData['data'][i]['status'],
                                        zone: _this.nameMap
                                    });
                                }
                            }
                            break;
                        }
                    }
                }, false);
                this.imgPosX = this._CANVAS.width / 2 - this.canvasW / 2;
                this.imgPosY = this._CANVAS.height / 2 - this.canvasH / 2;
            }
            else {
                this._CANVAS = this.canvasEl.nativeElement;
                var imageW = 1440;
                var imageH = 2940;
                this.screenW = this.platform.width();
                this.screenH = this.platform.height() - ((20 / 100) * this.platform.height());
                var n1 = this.getMax(this.getMax(imageW, imageH), this.getMax(this.screenW, this.screenH));
                var n2 = this.getMin(this.getMax(imageW, imageH), this.getMax(this.screenW, this.screenH));
                var ratioW = (n1) / (n2);
                var n2 = this.getMax(this.getMin(imageW, imageH), this.getMin(this.screenW, this.screenH));
                var n3 = this.getMin(this.getMin(imageW, imageH), this.getMin(this.screenW, this.screenH));
                var ratioH = (n2) / (n3);
                var imageScale = this.getMax(ratioW, ratioH);
                this.canvasW = imageW / imageScale;
                this.canvasH = imageH / imageScale;
                this._CANVAS.width = this.screenW;
                this._CANVAS.height = this.screenH;
                this.imgPosX = this._CANVAS.width / 2 - this.canvasW / 2;
                this.imgPosY = this._CANVAS.height / 2 - this.canvasH / 2;
                this.zoomReset();
            }
            // this.initialiseCanvas();
            this._CONTEXT = this._CANVAS.getContext('2d');
            this.setupCanvas();
        }
        else {
            this.alertCtl.create({
                title: "Error",
                message: "Error"
            }).present();
        }
    };
    GeneralViewZonePage.prototype.ngOnInit = function () {
        var _this = this;
        this.nativeStorage.getItem('ln')
            .then((function (data) {
            if (data == "en") {
                _this.load = _this.loadCtl.create({
                    content: "Please wait..."
                });
                _this.load.present();
                _this.anth.authState.subscribe(function (data) {
                    _this.uid = data['uid'];
                });
                _this.title = _this.navParams.get('nameZone');
                _this.nameMap = _this.navParams.get('nameMap');
                _this.showName = _this.navParams.get('showName');
                _this.afDb.object('/' + _this.nameMap + '/' + _this.title).valueChanges().subscribe(function (res) {
                    console.log(JSON.stringify(res));
                    _this.dataManagement(res);
                    // this.zoomReset();
                });
            }
            else {
                _this.load = _this.loadCtl.create({
                    content: "โปรดสักครู่..."
                });
                _this.load.present();
                _this.anth.authState.subscribe(function (data) {
                    _this.uid = data['uid'];
                });
                _this.title = _this.navParams.get('nameZone');
                _this.nameMap = _this.navParams.get('nameMap');
                _this.showName = _this.navParams.get('showName');
                _this.afDb.object('/' + _this.nameMap + '/' + _this.title).valueChanges().subscribe(function (res) {
                    console.log(JSON.stringify(res));
                    _this.dataManagement(res);
                    // this.zoomReset();
                });
            }
        })), function (error) { return console.error(error); };
    };
    GeneralViewZonePage.prototype.initialiseCanvas = function () {
        if (this._CANVAS.getContext) {
            this.setupCanvas();
        }
    };
    GeneralViewZonePage.prototype.getMax = function (n1, n2) {
        if (n1 >= n2) {
            return n1;
        }
        else {
            return n2;
        }
    };
    GeneralViewZonePage.prototype.getMin = function (n1, n2) {
        if (n1 <= n2) {
            return n1;
        }
        else {
            return n2;
        }
    };
    GeneralViewZonePage.prototype.zoomReset = function () {
        this._CONTEXT.save();
        __WEBPACK_IMPORTED_MODULE_7_d3_selection__["f" /* select */]("canvas").call(__WEBPACK_IMPORTED_MODULE_8_d3_zoom__["a" /* zoom */]().transform, __WEBPACK_IMPORTED_MODULE_8_d3_zoom__["b" /* zoomIdentity */].translate(0, 0).scale(1));
        this._CONTEXT.clearRect(0, 0, this.screenW, this.screenH);
        this._CONTEXT.translate(0, 0);
        this._CONTEXT.scale(1, 1);
        this.x = 0;
        this.y = 0;
        this.k = 1;
        this.setupCanvas();
        this._CONTEXT.restore();
    };
    GeneralViewZonePage.prototype.setupCanvas = function () {
        // this._CONTEXT = this._CANVAS.getContext('2d');
        var _this = this;
        this._CONTEXT.clearRect(0, 0, this._CANVAS.width, this._CANVAS.height);
        this._CONTEXT.fillStyle = "#3e3e3e";
        var img = new Image();
        img.src = this.listData['url'];
        if (this.checkFirstDraw == false) {
            img.onload = function () {
                _this.imgH = img.height;
                _this.imgW = img.width;
                _this._CONTEXT.drawImage(img, _this.imgPosX, _this.imgPosY, _this.canvasW, _this.canvasH);
                _this.checkFirstDraw = true;
                _this.drawRec();
            };
        }
        else {
            this._CONTEXT.drawImage(img, this.imgPosX, this.imgPosY, this.canvasW, this.canvasH);
            this.drawRec();
        }
        this.load.dismiss();
    };
    GeneralViewZonePage.prototype.drawRec = function () {
        console.log("Draw Rec");
        this._CONTEXT.beginPath();
        this._CONTEXT.save();
        this._CONTEXT.translate(this.imgPosX, this.imgPosY);
        var redColor = "#ff001a";
        var greenColor = "#62ff5f";
        var or = "#f79036";
        for (var i = 0; i < this.listData['data'].length; i++) {
            var rawX = this.listData['data'][i]['x'];
            var rawY = this.listData['data'][i]['y'];
            var rawH = this.listData['data'][i]['chh'];
            var rawW = this.listData['data'][i]['chw'];
            var w = (rawW / (this.imgW / this.canvasW));
            var h = (rawH / (this.imgH / this.canvasH));
            var x = (rawX / (this.imgW / this.canvasW));
            var y = (rawY / (this.imgH / this.canvasH));
            var angle = this.listData['data'][i]['angle'];
            var type = this.listData['data'][i]['type'];
            if (this.listData['data'][i]['status'] == 1 || this.listData['data'][i]['statusReserve'] == 1) {
                this._CONTEXT.save();
                if (this.uid == this.listData['data'][i]['uid']) {
                    this._CONTEXT.translate(x, y);
                    this._CONTEXT.rotate(this.listData['data'][i]['angle'] * Math.PI / 180);
                    this._CONTEXT.fillStyle = or;
                    this._CONTEXT.fillRect(0, 0, w, h);
                }
                else {
                    this._CONTEXT.translate(x, y);
                    this._CONTEXT.rotate(this.listData['data'][i]['angle'] * Math.PI / 180);
                    this._CONTEXT.fillStyle = redColor;
                    this._CONTEXT.fillRect(0, 0, w, h);
                }
                if (type == 1) {
                    this._CONTEXT.fillStyle = "#FFFFFF";
                    this._CONTEXT.font = '15px serif';
                    this._CONTEXT.fillText('R', (w / 2), (h / 2));
                }
                this._CONTEXT.restore();
            }
            else if (this.listData['data'][i]['status'] == 0 || this.listData['data'][i]['statusReserve'] == 0) {
                this._CONTEXT.save();
                this._CONTEXT.translate(x, y);
                this._CONTEXT.rotate(this.listData['data'][i]['angle'] * Math.PI / 180);
                this._CONTEXT.fillStyle = greenColor;
                this._CONTEXT.fillRect(0, 0, w, h);
                if (type == 1) {
                    this._CONTEXT.fillStyle = "#FFFFFF";
                    this._CONTEXT.font = '15px serif';
                    this._CONTEXT.fillText('R', (w / 2), (h / 2));
                }
                this._CONTEXT.restore();
            }
        }
        this._CONTEXT.restore();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["_8" /* ViewChild */])('canvas'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4__angular_core__["t" /* ElementRef */])
    ], GeneralViewZonePage.prototype, "canvasEl", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["c" /* Content */])
    ], GeneralViewZonePage.prototype, "content", void 0);
    GeneralViewZonePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["m" /* Component */])({
            selector: 'page-general-view-zone',template:/*ion-inline-start:"/Volumes/9rachata/Archive/navi-park/src/pages/general-view-zone/general-view-zone.html"*/'\n<ion-header>\n\n  <ion-navbar color="navbar">\n    <ion-title>{{\'ZONE\' | translate}} {{ showName }}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content no-padding >\n  <!-- <ion-grid>\n    <ion-row center no-padding>\n      <ion-col >\n          <div class ="Aligner">\n              <canvas #canvas ></canvas>\n          </div>\n      </ion-col>\n    </ion-row>\n  \n  </ion-grid> -->\n  <ion-fab right top >\n    <button (click)="zoomReset()" ion-fab color="primary" class="btn-zoom">\n        <ion-icon name="md-contract"></ion-icon>\n    </button>\n\n\n  </ion-fab>\n\n\n\n\n  <div class ="Aligner">\n    <canvas #canvas ></canvas>\n</div>\n\n</ion-content>\n'/*ion-inline-end:"/Volumes/9rachata/Archive/navi-park/src/pages/general-view-zone/general-view-zone.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_screen_orientation__["a" /* ScreenOrientation */],
            __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__["AngularFireAuth"],
            __WEBPACK_IMPORTED_MODULE_0__ionic_native_native_storage__["a" /* NativeStorage */]])
    ], GeneralViewZonePage);
    return GeneralViewZonePage;
}());

//# sourceMappingURL=general-view-zone.js.map

/***/ }),

/***/ 271:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeneralZoneConfirmPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_native_native_storage__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_screen_orientation__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__node_modules_ionic_native_date_picker__ = __webpack_require__(433);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the GeneralZoneConfirmPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var GeneralZoneConfirmPage = /** @class */ (function () {
    function GeneralZoneConfirmPage(navCtrl, navParams, alertCtrl, db, anth, screenOrientation, datePicker, keyboard, nativeStorage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.db = db;
        this.anth = anth;
        this.screenOrientation = screenOrientation;
        this.datePicker = datePicker;
        this.keyboard = keyboard;
        this.nativeStorage = nativeStorage;
        this.startShow = true;
        this.endShow = true;
        this.checkPrice = true;
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }
    GeneralZoneConfirmPage.prototype.ngOnInit = function () {
        var _this = this;
        this.nativeStorage.getItem('ln')
            .then((function (data) {
            if (data == "en") {
                _this.ln = "en";
            }
            else {
                _this.ln = "th";
            }
        }), function (error) { return console.error(error); });
        this.ch = this.navParams.get('CH');
        this.zone = this.navParams.get('nameParking');
        this.nameParking = this.navParams.get('zone');
        this.status = this.navParams.get('status');
        this.anth.authState.subscribe(function (data) {
            _this.uid = data['uid'];
            _this.db.object('/' + _this.nameParking + '/' + _this.zone + '/' + _this.ch).valueChanges().subscribe(function (res) {
                if (res['uid'] == _this.uid) {
                    var a_1 = _this.db.object('/' + _this.nameParking).valueChanges().subscribe(function (resP) {
                        _this.priceRange = resP['price-range'];
                        _this.price = resP['price-range'];
                        _this.endShow = false;
                        _this.startShow = false;
                        _this.showBtn = "Cancel Reservation";
                        _this.myDateStart = res['startDateTime'];
                        _this.myDateEnd = res['endDateTime'];
                        _this.statusChange = true;
                        _this.checkPrice = false;
                        var checkStart = new Date(_this.myDateStart);
                        var checkEnd = new Date(_this.myDateEnd);
                        _this.priceShow = _this.price * _this.diff_hours(checkStart, checkEnd);
                        a_1.unsubscribe();
                    });
                }
                else {
                    var a_2 = _this.db.object('/' + _this.nameParking).valueChanges().subscribe(function (resP) {
                        _this.priceRange = resP['price-range'];
                        _this.price = resP['price-range'];
                        _this.priceShow = _this.price;
                        a_2.unsubscribe();
                    });
                    var date = new Date();
                    var houreAdd1 = new Date();
                    houreAdd1.setHours(houreAdd1.getHours() + 2);
                    _this.myDateStart = date.getFullYear() + '/' + date.getMonth() + '/' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes();
                    _this.myDateEnd = date.getFullYear() + '/' + date.getMonth() + '/' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes();
                    _this.showBtn = "Confirm";
                    _this.statusChange = false;
                }
            });
        });
    };
    GeneralZoneConfirmPage.prototype.onClick = function () {
        var _this = this;
        if (this.statusChange == false) {
            var a_3 = this.db.object('/' + this.nameParking + '/' + this.zone + '/' + this.ch).valueChanges().subscribe(function (res) {
                if (res['Park Status'] == 1 || res['Reserve Status'] == 1) {
                    if (_this.ln == "en") {
                        _this.alertCtrl.create({
                            title: "Error",
                            message: "Can not be booked There are people booking before. Please select a new parking area."
                        }).present();
                    }
                    else {
                        _this.alertCtrl.create({
                            title: "ผิดพลาด",
                            message: "ไม่สามารถจองได้ เนื่องจากมีคนจองก่อนหน้าแล้ว โปรดเลือกช่องจอดใหม่"
                        }).present();
                    }
                    _this.navCtrl.pop();
                    a_3.unsubscribe();
                }
                else {
                    var checkStart = new Date(_this.myDateStart);
                    var checkEnd = new Date(_this.myDateEnd);
                    _this.priceShow = _this.price * _this.diff_hours(checkStart, checkEnd);
                    _this.db.object('/' + _this.nameParking + '/' + _this.zone + '/' + _this.ch)
                        .update({ 'Reserve Status': 1, 'uid': _this.uid, 'startDateTime': _this.myDateStart, 'endDateTime': _this.myDateEnd, 'priceStatus': 0 }).then(function () {
                        var itemsRef = _this.db.list('/data_user/' + _this.uid + '/history');
                        var myDate = new Date().toLocaleString();
                        itemsRef.push({ name: _this.nameParking, zone: _this.zone, ch: _this.ch, date: myDate, startDateTime: _this.myDateStart, endDateTime: _this.myDateEnd, 'priceStatus': 0, 'price': _this.priceRange, 'priceShow': _this.priceShow })
                            .then(function (data) {
                            _this.db.object('/' + _this.nameParking + '/' + _this.zone + '/' + _this.ch)
                                .update({ 'idRefHis': data.key })
                                .then(function () {
                                if (_this.ln == "en") {
                                    _this.alertCtrl.create({
                                        title: "Success",
                                        message: "Successful reservation"
                                    }).present();
                                }
                                else {
                                    _this.alertCtrl.create({
                                        title: "สำเร็จ",
                                        message: "จองสำเร็จ"
                                    }).present();
                                }
                                _this.navCtrl.pop();
                                _this.statusChange = true;
                                _this.showBtn = "Cancel Reservation";
                            });
                        });
                    }).catch(function (error) {
                    });
                    a_3.unsubscribe();
                }
            });
        }
        else {
            var a_4 = this.db.object('/' + this.nameParking + '/' + this.zone + '/' + this.ch + '/Park Status').valueChanges().subscribe(function (res) {
                if (res == 1) {
                    if (_this.ln == "en") {
                        _this.alertCtrl.create({
                            title: "Error",
                            message: "Can not be booked Because people parked before. Please select a new parking area"
                        }).present();
                    }
                    else {
                        _this.alertCtrl.create({
                            title: "ผิดพลาด",
                            message: "ไม่สามารถจองได้ เนื่องจากมีคนจอดอยู่ก่อนหน้าแล้ว โปรดเลือกช่องจอดใหม่"
                        }).present();
                    }
                    _this.navCtrl.pop();
                    a_4.unsubscribe();
                }
                else {
                    _this.db.object('/' + _this.nameParking + '/' + _this.zone + '/' + _this.ch)
                        .update({ 'Reserve Status': 0 })
                        .then(function () {
                        _this.db.object('/' + _this.nameParking + '/' + _this.zone + '/' + _this.ch + '/startDateTime').remove();
                        _this.db.object('/' + _this.nameParking + '/' + _this.zone + '/' + _this.ch + '/endDateTime').remove();
                        _this.db.object('/' + _this.nameParking + '/' + _this.zone + '/' + _this.ch + '/uid').remove();
                        _this.db.object('/' + _this.nameParking + '/' + _this.zone + '/' + _this.ch + '/priceStatus').remove()
                            .then(function () {
                            var rm = _this.db.object('/' + _this.nameParking + '/' + _this.zone + '/' + _this.ch + '/idRefHis').valueChanges().subscribe(function (resS) {
                                _this.rmData(resS);
                                rm.unsubscribe();
                            });
                        });
                    }).catch(function (error) {
                    });
                    a_4.unsubscribe();
                }
            });
        }
    };
    GeneralZoneConfirmPage.prototype.rmData = function (data) {
        var _this = this;
        this.db.object('/data_user' + '/' + this.uid + '/history/' + data).remove()
            .then(function () {
            _this.db.object('/' + _this.nameParking + '/' + _this.zone + '/' + _this.ch + '/idRefHis').remove()
                .then(function () {
                if (_this.ln == "en") {
                    _this.alertCtrl.create({
                        title: 'Success',
                        message: "Cancel reservation"
                    }).present();
                }
                else {
                    _this.alertCtrl.create({
                        title: "สำเร็จ",
                        message: "ยกเลิกการจองสำเร็จ"
                    }).present();
                }
                _this.navCtrl.pop();
                _this.statusChange = false;
                _this.showBtn = "Confirm";
            });
        });
    };
    GeneralZoneConfirmPage.prototype.openDatepicker = function () {
        var _this = this;
        this.keyboard.close();
        if (this.startShow == true) {
            this.datePicker.show({
                date: new Date(),
                mode: 'datetime',
                // androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT,
                is24Hour: true
            }).then(function (date) {
                _this.myDateStart = date.getFullYear() + '/' + date.getMonth() + '/' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes();
            }, function (err) { return console.log('Error occurred while getting date: ', err); });
        }
    };
    GeneralZoneConfirmPage.prototype.openDatepickerEnd = function () {
        var _this = this;
        this.keyboard.close();
        if (this.endShow == true) {
            this.datePicker.show({
                date: new Date(),
                mode: 'datetime',
                // androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT,
                is24Hour: true
            }).then(function (date) {
                _this.myDateEnd = date.getFullYear() + '/' + date.getMonth() + '/' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes();
            }, function (err) { return console.log('Error occurred while getting date: ', err); });
        }
    };
    GeneralZoneConfirmPage.prototype.setTime = function () {
        var date = new Date();
        var today = date.getFullYear() + '/' + date.getMonth() + '/' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes();
        var todays = new Date(today);
        var checkStart = new Date(this.myDateStart);
        var checkEnd = new Date(this.myDateEnd);
        this.priceShow = this.price * this.diff_hours(checkStart, checkEnd);
        if (this.priceShow <= 0) {
            this.checkPrice = true;
        }
        else {
            this.checkPrice = false;
        }
    };
    GeneralZoneConfirmPage.prototype.diff_hours = function (dt2, dt1) {
        var diff = (dt2.getTime() - dt1.getTime()) / 1000;
        diff /= (60 * 60);
        return Math.abs(Math.round(diff));
    };
    GeneralZoneConfirmPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["m" /* Component */])({
            selector: 'page-general-zone-confirm',template:/*ion-inline-start:"/Volumes/9rachata/Archive/navi-park/src/pages/general-zone-confirm/general-zone-confirm.html"*/'<!--\n  Generated template for the GeneralZoneConfirmPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="navbar">\n    <ion-title>{{\'Reservation confirmation\' | translate}}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding class="bg">\n\n\n\n   \n\n     <ion-item class="item-content" >\n\n        <ion-icon name="ios-map-outline" item-start color="light" ></ion-icon>\n        <span ion-text color="light">{{nameParking}}</span>\n     </ion-item>\n     <ion-item class="item-content" > \n\n        <ion-icon name="ios-bookmarks-outline" item-start color="light" ></ion-icon>\n        <span ion-text color="light">{{zone}}</span>\n     </ion-item>\n\n     <ion-item class="item-content" >\n        <ion-icon name="ios-car-outline" item-start color="light" ></ion-icon>\n        <span ion-text color="light">{{ch}}</span>\n      \n    </ion-item>\n\n    <ion-item class="item-content" >\n        <ion-label style="color: white" >{{\'Start time\' | translate}}</ion-label>\n        \n        <ion-input disabled="true" type="text" ion-text style="color :white" (ionChange)="setTime()" (click)="openDatepicker()" [(ngModel)]="myDateStart"></ion-input>\n    </ion-item>\n\n    <ion-item class="item-content" >\n        <ion-label style="color: white" >{{\'End time\' | translate}}</ion-label>\n\n        <ion-input disabled="true" type="text" ion-text style="color :white"  (ionChange)="setTime()" (click)="openDatepickerEnd()" [(ngModel)]="myDateEnd"></ion-input>\n    </ion-item>\n\n    <ion-item class="item-content">\n\n      <ion-row>\n        <ion-col >\n          <h1 ion-text color="light">{{\'Price\' | translate}} : </h1>\n        </ion-col>\n        <ion-col >\n          <h1 ion-text color="light" >{{priceShow}} {{\'baht\' | translate}}</h1>\n        </ion-col>\n\n      </ion-row>\n\n    </ion-item>\n\n\n  \n<ion-grid>\n  <ion-row>\n    <ion-col >\n      <button ion-button [disabled]=\'checkPrice\' full block round (click)="onClick()">{{showBtn | translate}}</button>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n\n\n\n</ion-content>\n'/*ion-inline-end:"/Volumes/9rachata/Archive/navi-park/src/pages/general-zone-confirm/general-zone-confirm.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__["AngularFireAuth"],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_screen_orientation__["a" /* ScreenOrientation */],
            __WEBPACK_IMPORTED_MODULE_6__node_modules_ionic_native_date_picker__["a" /* DatePicker */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["i" /* Keyboard */],
            __WEBPACK_IMPORTED_MODULE_0__ionic_native_native_storage__["a" /* NativeStorage */]])
    ], GeneralZoneConfirmPage);
    return GeneralZoneConfirmPage;
}());

//# sourceMappingURL=general-zone-confirm.js.map

/***/ }),

/***/ 272:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeneralViewMapImagePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_native_native_storage__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_screen_orientation__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_Rxjs_rx__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_Rxjs_rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_Rxjs_rx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the GeneralViewMapImagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var GeneralViewMapImagePage = /** @class */ (function () {
    function GeneralViewMapImagePage(navCtrl, navParams, db, screenOrientation, nativeStorage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.db = db;
        this.screenOrientation = screenOrientation;
        this.nativeStorage = nativeStorage;
        this.urlImage = [];
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }
    GeneralViewMapImagePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.observableVar = __WEBPACK_IMPORTED_MODULE_5_Rxjs_rx__["Observable"].interval(2000).subscribe(function () {
            _this.nativeStorage.getItem('ln')
                .then((function (data) {
                if (data == "en") {
                    _this.titleShow = _this.dataReCheck['nameEN'];
                }
                else {
                    _this.titleShow = _this.dataReCheck['nameTH'];
                }
            }), function (error) { return console.error(error); });
        });
    };
    GeneralViewMapImagePage.prototype.ionViewDidLeave = function () {
        this.observableVar.unsubscribe();
    };
    GeneralViewMapImagePage.prototype.ngOnInit = function () {
        var _this = this;
        this.title = this.navParams.get('data');
        this.nativeStorage.getItem('ln')
            .then((function (data) {
            if (data == "en") {
                _this.ln = "en";
            }
            else {
                _this.ln = "th";
            }
        }), function (error) { return console.error(error); });
        this.db.object('/' + this.title).valueChanges().subscribe(function (resF) {
            _this.dataReCheck = resF;
            var spaces = 0;
            _this.urlImage = [];
            var keyMain = Object.keys(resF);
            if (_this.ln == "en") {
                _this.titleShow = resF['nameEN'];
            }
            else {
                _this.titleShow = resF['nameTH'];
            }
            for (var i = 0; i < keyMain.length; i++) {
                if (keyMain[i] != 'A_Total Zone' && keyMain[i] != 'lat' && keyMain[i] != 'long' && keyMain[i] != "image-zone" && keyMain[i] != "opening-time" && keyMain[i] != "price-range" && keyMain[i] != "nameTH" && keyMain[i] != "nameEN") {
                    var raw = {};
                    raw['url'] = resF[keyMain[i]]['URL'];
                    raw['name'] = resF[keyMain[i]]['Name'];
                    _this.urlImage.push(raw);
                }
            }
        });
    };
    GeneralViewMapImagePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["m" /* Component */])({
            selector: 'page-general-view-map-image',template:/*ion-inline-start:"/Volumes/9rachata/Archive/navi-park/src/pages/general-view-map-image/general-view-map-image.html"*/'<!--\n  Generated template for the GeneralViewMapImagePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <ion-navbar color ="navbar">\n      <ion-title>{{titleShow}}</ion-title>\n    </ion-navbar>\n  \n  </ion-header>\n  \n\n\n<ion-content padding class="bg">\n\n\n  <ion-list no-lines>\n    <ion-item   class="item-content" *ngFor="let i  of urlImage">\n      <h2  style="padding-top: 10px; padding-bottom: 10px; text-align: center" ion-text color="light"><b>{{\'ZONE\' | translate}} {{i.name}}</b></h2>\n      <img class="canter" [src]="i.url">\n    </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Volumes/9rachata/Archive/navi-park/src/pages/general-view-map-image/general-view-map-image.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_1__ionic_native_screen_orientation__["a" /* ScreenOrientation */],
            __WEBPACK_IMPORTED_MODULE_0__ionic_native_native_storage__["a" /* NativeStorage */]])
    ], GeneralViewMapImagePage);
    return GeneralViewMapImagePage;
}());

//# sourceMappingURL=general-view-map-image.js.map

/***/ }),

/***/ 273:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeneralHistoryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_native_screen_orientation__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the GeneralHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var GeneralHistoryPage = /** @class */ (function () {
    function GeneralHistoryPage(navCtrl, navParams, db, anth, screenOrientation) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.db = db;
        this.anth = anth;
        this.screenOrientation = screenOrientation;
        this.listData = [];
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }
    GeneralHistoryPage.prototype.ngOnInit = function () {
        var _this = this;
        this.anth.authState.subscribe(function (data) {
            _this.uid = data['uid'];
            var a = _this.db.object('/data_user/' + data['uid'] + '/history').valueChanges().subscribe(function (res) {
                _this.listData = [];
                var key = Object.keys(res);
                if (key.length > 0) {
                    for (var i = 0; i < key.length; i++) {
                        var raw = {};
                        raw['date'] = res[key[i]]['date'];
                        raw['ch'] = res[key[i]]['ch'];
                        raw['name'] = res[key[i]]['name'];
                        raw['zone'] = res[key[i]]['zone'];
                        raw['startDT'] = res[key[i]]['startDateTime'];
                        raw['endDT'] = res[key[i]]['endDateTime'];
                        _this.listData.push(raw);
                    }
                }
                else {
                    a.unsubscribe();
                    return;
                }
                a.unsubscribe();
            });
        });
    };
    GeneralHistoryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["m" /* Component */])({
            selector: 'page-general-history',template:/*ion-inline-start:"/Volumes/9rachata/Archive/navi-park/src/pages/general-history/general-history.html"*/'<!--\n  Generated template for the GeneralHistoryPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color = "navbar">\n    <button ion-button menuToggle icon-only>\n      <ion-icon name="menu"></ion-icon>\n</button>\n\n\n    <ion-title>{{ \'HISTORY\' | translate }}</ion-title>\n</ion-navbar>\n</ion-header>\n\n\n\n<ion-content padding class="bg">\n\n  <ion-list no-lines *ngIf="listData">\n    <ion-item class="item-content" *ngFor="let data of listData" >\n        <h2 ion-text color="light" ><b>{{data.name}}</b></h2>\n\n\n       \n\n\n        <table *ngIf="listData">\n          <tr>\n            <td>\n              <p ion-text color="light" ><b>{{ \'ZONE\' | translate }}</b></p>\n            </td>\n            <td>\n              <p style="padding-left: 5px" ion-text color="light">{{data.zone}}</p>\n            </td>\n          </tr>\n\n\n          <tr>\n            <td>\n              <p ion-text color="light" ><b>{{ \'DATETIME\' | translate }}</b></p>\n            </td>\n            <td>\n              <p style="padding-left: 5px" ion-text color="light">{{data.date}}</p>\n            </td>\n          </tr>\n          <tr>\n              <td>\n                <p ion-text color="light" ><b>{{ \'Start time\' | translate }}</b></p>\n              </td>\n              <td>\n                <p style="padding-left: 5px" ion-text color="light">{{data.startDT}}</p>\n              </td>\n            </tr>\n\n            <tr>\n                <td>\n                  <p ion-text color="light" ><b>{{ \'End time\' | translate }}</b></p>\n                </td>\n                <td>\n                  <p style="padding-left: 5px" ion-text color="light">{{data.endDT}}</p>\n                </td>\n              </tr>\n          <tr>\n            <td>\n              <p ion-text color="light" ><b>{{ \'PARKING LOT\' | translate }}</b></p>\n            </td>\n            <td>\n              <p style="padding-left: 5px" ion-text color="light" >{{data.ch}}</p>\n            </td>\n          </tr>\n\n        </table>\n       \n\n    </ion-item>\n  </ion-list>\n\n</ion-content>\n'/*ion-inline-end:"/Volumes/9rachata/Archive/navi-park/src/pages/general-history/general-history.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_1_angularfire2_auth__["AngularFireAuth"],
            __WEBPACK_IMPORTED_MODULE_0__ionic_native_screen_orientation__["a" /* ScreenOrientation */]])
    ], GeneralHistoryPage);
    return GeneralHistoryPage;
}());

//# sourceMappingURL=general-history.js.map

/***/ }),

/***/ 274:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeneralTabPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__general_home_general_home__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__general_first_general_first__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__news_news__ = __webpack_require__(275);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__general_profile_general_profile__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__node_modules_ngx_translate_core__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__search_favorite_search_favorite__ = __webpack_require__(158);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the GeneralTabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var GeneralTabPage = /** @class */ (function () {
    function GeneralTabPage(navParams, translate) {
        this.translate = translate;
        this.profile = __WEBPACK_IMPORTED_MODULE_3__general_profile_general_profile__["a" /* GeneralProfilePage */];
        this.news = __WEBPACK_IMPORTED_MODULE_2__news_news__["a" /* NewsPage */];
        this.home = __WEBPACK_IMPORTED_MODULE_1__general_first_general_first__["a" /* GeneralFirstPage */];
        this.map = __WEBPACK_IMPORTED_MODULE_0__general_home_general_home__["a" /* GeneralHomePage */];
        this.fav = __WEBPACK_IMPORTED_MODULE_7__search_favorite_search_favorite__["a" /* SearchFavoritePage */];
        this.myIndex = navParams.data.tabIndex || 0;
    }
    GeneralTabPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["m" /* Component */])({
            selector: 'page-general-tab',template:/*ion-inline-start:"/Volumes/9rachata/Archive/navi-park/src/pages/general-tab/general-tab.html"*/'\n\n<ion-tabs   color = "navbar" [selectedIndex]="myIndex">\n    <ion-tab [root]="home" tabTitle="{{\'HOME\' | translate}}" tabIcon="ios-home-outline"></ion-tab>\n    <ion-tab [root]="map" tabTitle="{{\'PARKING\' | translate}}" tabIcon="ios-car-outline"></ion-tab>\n\n    <ion-tab [root]="fav" tabTitle="{{\'FAVORITE\' | translate}}" tabIcon="ios-star-outline"></ion-tab>\n    <ion-tab [root]="news" tabTitle="{{\'NEWS\' | translate}}" tabIcon="ios-list-box-outline"></ion-tab>\n  </ion-tabs>\n\n\n'/*ion-inline-end:"/Volumes/9rachata/Archive/navi-park/src/pages/general-tab/general-tab.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["m" /* NavParams */], __WEBPACK_IMPORTED_MODULE_6__node_modules_ngx_translate_core__["c" /* TranslateService */]])
    ], GeneralTabPage);
    return GeneralTabPage;
}());

//# sourceMappingURL=general-tab.js.map

/***/ }),

/***/ 275:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_native_in_app_browser__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_screen_orientation__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_Rxjs_rx__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_Rxjs_rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_Rxjs_rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__node_modules_ionic_native_native_storage__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var NewsPage = /** @class */ (function () {
    function NewsPage(navCtrl, navParams, loadCtl, db, screenOrientation, nativeStorage, iab) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadCtl = loadCtl;
        this.db = db;
        this.screenOrientation = screenOrientation;
        this.nativeStorage = nativeStorage;
        this.iab = iab;
        this.listNews = [];
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }
    NewsPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.observableVar = __WEBPACK_IMPORTED_MODULE_5_Rxjs_rx__["Observable"].interval(3000).subscribe(function () {
            _this.nativeStorage.getItem('ln')
                .then((function (data) {
                if (data == "en") {
                    var key = Object.keys(_this.dataRecheck);
                    _this.listNews = [];
                    for (var i = 0; i < key.length; i++) {
                        var rawNews = {};
                        rawNews['title'] = _this.dataRecheck[key[i]]['titleEN'];
                        rawNews['date'] = _this.dataRecheck[key[i]]['date'];
                        rawNews['content'] = _this.dataRecheck[key[i]]['contentEN'];
                        _this.listNews.push(rawNews);
                    }
                }
                else {
                    var key = Object.keys(_this.dataRecheck);
                    _this.listNews = [];
                    for (var i = 0; i < key.length; i++) {
                        var rawNews = {};
                        rawNews['title'] = _this.dataRecheck[key[i]]['titleTH'];
                        rawNews['date'] = _this.dataRecheck[key[i]]['date'];
                        rawNews['content'] = _this.dataRecheck[key[i]]['contentTH'];
                        _this.listNews.push(rawNews);
                    }
                }
            }), function (error) { return console.error(error); });
        });
    };
    NewsPage.prototype.ionViewDidLeave = function () {
        this.observableVar.unsubscribe();
    };
    NewsPage.prototype.openUrl = function () {
        // const options: InAppBrowserOptions = {
        //   zoom: 'no',
        //   location: 'no',
        //   toolbar: 'no'
        // };
        var browser = this.iab.create('http://skt-check.com/checkout/', '_self', { location: 'no', toolbar: 'no', zoom: 'no' });
        // window.open('https://www.google.com', '_system');
        // return false;
    };
    NewsPage.prototype.ngOnInit = function () {
        var _this = this;
        var dataLn;
        var dataShow;
        this.nativeStorage.getItem('ln')
            .then((function (data) {
            if (data == "en") {
                dataLn = "en";
                dataShow = "Please wait...";
                var load_1 = _this.loadCtl.create({
                    content: dataShow
                });
                load_1.present();
                _this.db.object('/data/news').valueChanges().subscribe(function (res) {
                    _this.showNews(res, dataLn);
                    load_1.dismiss();
                });
            }
            else {
                dataLn = "th";
                dataShow = 'โปรดรอสักครู่...';
                var load_2 = _this.loadCtl.create({
                    content: dataShow
                });
                load_2.present();
                _this.db.object('/data/news').valueChanges().subscribe(function (res) {
                    _this.showNews(res, dataLn);
                    load_2.dismiss();
                });
            }
        }), function (error) { return console.error(error); });
    };
    NewsPage.prototype.showNews = function (res, setLn) {
        var key = Object.keys(res);
        this.dataRecheck = res;
        this.listNews = [];
        for (var i = 0; i < key.length; i++) {
            var rawNews = {};
            if (setLn == "en") {
                rawNews['title'] = res[key[i]]['titleEN'];
                rawNews['date'] = res[key[i]]['date'];
                rawNews['content'] = res[key[i]]['contentEN'];
            }
            else {
                rawNews['title'] = res[key[i]]['titleTH'];
                rawNews['date'] = res[key[i]]['date'];
                rawNews['content'] = res[key[i]]['contentTH'];
            }
            this.listNews.push(rawNews);
        }
    };
    NewsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["m" /* Component */])({
            selector: 'page-news',template:/*ion-inline-start:"/Volumes/9rachata/Archive/navi-park/src/pages/news/news.html"*/'<!--\n  Generated template for the NewsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <ion-navbar color = "navbar">\n        <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title>{{\'NEWS\' | translate}}</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content class="social-cards bg">\n\n        <ion-card *ngFor="let data of listNews">\n        \n          <ion-item>\n            <ion-avatar item-start>\n                <img src="assets/imgs/park.png">\n            </ion-avatar>\n            <h2>{{data.title}}</h2>\n            <p>{{data.date}}</p>\n          </ion-item>   \n          <ion-card-content>\n            <p>{{data.content}}</p>\n            <p ion-text color="primary" style="font-size: 15px" (click)="openUrl()">{{\'READ MORE\' | translate}}</p>\n          </ion-card-content>\n        \n          \n        \n        </ion-card>\n        </ion-content>\n        \n        <style>\n          .social-cards ion-col {\n            padding: 0;\n          }\n        </style>\n'/*ion-inline-end:"/Volumes/9rachata/Archive/navi-park/src/pages/news/news.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_1__ionic_native_screen_orientation__["a" /* ScreenOrientation */],
            __WEBPACK_IMPORTED_MODULE_6__node_modules_ionic_native_native_storage__["a" /* NativeStorage */],
            __WEBPACK_IMPORTED_MODULE_0__ionic_native_in_app_browser__["a" /* InAppBrowser */]])
    ], NewsPage);
    return NewsPage;
}());

//# sourceMappingURL=news.js.map

/***/ }),

/***/ 276:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__agree_agree__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_screen_orientation__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RegisterPage = /** @class */ (function () {
    function RegisterPage(navCtrl, navParams, auth, alertCtl, loadCtl, adb, screenOrientation) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.alertCtl = alertCtl;
        this.loadCtl = loadCtl;
        this.adb = adb;
        this.screenOrientation = screenOrientation;
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }
    RegisterPage.prototype.onSignUp = function (from) {
        if (from.value.txtPass == from.value.txtREPass) {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_0__agree_agree__["a" /* AgreePage */], { email: from.value.txtEmail,
                pass: from.value.txtPass,
                dfname: from.value.txtFName,
                ddname: from.value.txtDName,
                dphone: from.value.txtPhone,
                dcar: from.value.txtDCarBrand,
                dModelCar: from.value.txtDCarModel
            });
        }
        else {
            this.alertCtl.create({
                title: "Unsuccessful",
                message: "Please double check the passwords"
            }).present();
        }
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["m" /* Component */])({
            selector: 'page-register',template:/*ion-inline-start:"/Volumes/9rachata/Archive/navi-park/src/pages/register/register.html"*/'<!--\n  Generated template for the RegisPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color ="navbar">\n    <ion-title>ลงทะเบียน</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding class="animated fadeIn bg">\n\n  <ion-card>\n\n    <ion-card-content>\n\n     \n    \n      \n      <form  #f="ngForm" (ngSubmit)="onSignUp(f)">\n\n        <ion-item class="borders" no-lines style="margin-bottom: 5px;">\n            <ion-label>\n              \n              Full Name\n            </ion-label>\n            <ion-input \n            STYLE=\'color:#FFFFFF;\'\n              name="txtFName"\n              type="text" \n              ngModel\n\n              required> \n            </ion-input>\n          </ion-item>\n\n\n        <ion-item class="borders" no-lines style="margin-bottom: 5px;">\n            <ion-label >\n              \n              Display Name\n            </ion-label>\n            <ion-input \n              STYLE=\'color:#FFFFFF;\'\n              name="txtDName"\n              type="text" \n              ngModel\n\n              required> \n            </ion-input>\n          </ion-item>\n\n\n        <ion-item class="borders" no-lines style="margin-bottom: 5px;">\n            <ion-label >\n              \n              E-mail\n            </ion-label>\n            <ion-input \n            STYLE=\'color:#FFFFFF;\'\n              name="txtEmail"\n              type="email" \n              ngModel\n              \n              pattern="[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})"\n              required>\n            </ion-input>\n          </ion-item>\n      \n          <ion-item class="borders" no-lines style="margin-bottom: 5px;">\n            <ion-label >\n              \n              Password\n            </ion-label>\n            <ion-input \n            STYLE=\'color:#FFFFFF;\'\n              name="txtPass"\n              type="password" \n              ngModel\n              \n              [minlength]="8"\n              required> \n            </ion-input>\n          </ion-item>\n          <p  ion-text color="danger" style="font-size: 10px; padding-left: 5px;">Please enter at least 8 characters.</p>\n          <ion-item class="borders" no-lines style="margin-bottom: 5px;">\n            <ion-label >\n              \n              Re-Password\n            </ion-label>\n            <ion-input \n            STYLE=\'color:#FFFFFF;\'\n              name="txtREPass"\n              type="password" \n              ngModel\n              \n              [minlength]="8"\n              required> \n            </ion-input>\n          </ion-item>\n\n          <ion-item class="borders" no-lines style="margin-bottom: 5px;">\n              <ion-label >\n                \n                  Telephone Number\n              </ion-label>\n              <ion-input \n              STYLE=\'color:#FFFFFF;\'\n                name="txtPhone"\n                type="number" \n                ngModel\n\n                required> \n              </ion-input>\n            </ion-item>\n            <ion-item class="borders" no-lines style="margin-bottom: 5px;">\n                <ion-label  >\n                    Car brand\n                </ion-label>\n        \n                <ion-input \n              STYLE=\'color:#FFFFFF;\'\n              name="txtDCarBrand"\n              type="text" \n              ngModel\n\n              required> \n            </ion-input>\n            </ion-item>\n            <ion-item class="borders" no-lines style="margin-bottom: 5px;">\n              <ion-label  >\n                Model of the car\n              </ion-label>\n      \n              <ion-input \n            STYLE=\'color:#FFFFFF;\'\n            name="txtDCarModel"\n            type="text" \n            ngModel\n\n            required> \n          </ion-input>\n          </ion-item>\n\n          <ion-item no-lines style="background: transparent" >\n            <div>\n              <button ion-button round large  icon-start block color="primary" tappable  type="submit"  [disabled]="!f.valid">\n                <ion-icon name="ios-create-outline"></ion-icon>\n                  Register\n              </button>\n            </div>\n        \n        \n          </ion-item>\n  </form>\n\n    </ion-card-content>\n  </ion-card>\n\n    \n  </ion-content>\n  \n'/*ion-inline-end:"/Volumes/9rachata/Archive/navi-park/src/pages/register/register.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["j" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2_angularfire2_database__["AngularFireDatabase"],
            __WEBPACK_IMPORTED_MODULE_1__ionic_native_screen_orientation__["a" /* ScreenOrientation */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 286:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 286;

/***/ }),

/***/ 430:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/agree/agree.module": [
		1347,
		20
	],
	"../pages/general-access-to-parking/general-access-to-parking.module": [
		1348,
		19
	],
	"../pages/general-contact/general-contact.module": [
		1349,
		18
	],
	"../pages/general-first/general-first.module": [
		1350,
		17
	],
	"../pages/general-history/general-history.module": [
		1351,
		16
	],
	"../pages/general-home/general-home.module": [
		1352,
		15
	],
	"../pages/general-menu/general-menu.module": [
		1353,
		14
	],
	"../pages/general-peview-parking/general-peview-parking.module": [
		1354,
		13
	],
	"../pages/general-profile/general-profile.module": [
		1355,
		12
	],
	"../pages/general-status-parking/general-status-parking.module": [
		1356,
		11
	],
	"../pages/general-tab/general-tab.module": [
		1357,
		10
	],
	"../pages/general-view-map-image/general-view-map-image.module": [
		1358,
		9
	],
	"../pages/general-view-zone/general-view-zone.module": [
		1359,
		8
	],
	"../pages/general-zone-confirm/general-zone-confirm.module": [
		1360,
		7
	],
	"../pages/login/login.module": [
		1361,
		6
	],
	"../pages/news/news.module": [
		1362,
		5
	],
	"../pages/payment/payment.module": [
		1363,
		4
	],
	"../pages/register/register.module": [
		1364,
		3
	],
	"../pages/search-all/search-all.module": [
		1365,
		2
	],
	"../pages/search-favorite/search-favorite.module": [
		1366,
		1
	],
	"../pages/setting/setting.module": [
		1367,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 430;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 432:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectFindComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pages_search_favorite_search_favorite__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_search_all_search_all__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the SelectFindComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var SelectFindComponent = /** @class */ (function () {
    function SelectFindComponent(viewCtrl, navCtrl, app) {
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.app = app;
    }
    SelectFindComponent.prototype.onAction = function (action) {
        this.viewCtrl.dismiss({ action: action });
        if (action == "all") {
            this.app.getActiveNav().push(__WEBPACK_IMPORTED_MODULE_1__pages_search_all_search_all__["a" /* SearchAllPage */]);
        }
        else {
            this.app.getActiveNav().push(__WEBPACK_IMPORTED_MODULE_0__pages_search_favorite_search_favorite__["a" /* SearchFavoritePage */]);
        }
    };
    SelectFindComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["m" /* Component */])({
            selector: 'select-find',template:/*ion-inline-start:"/Volumes/9rachata/Archive/navi-park/src/components/select-find/select-find.html"*/'<ion-grid>\n  <ion-row>\n    <ion-col >\n      <button ion-button icon-left clear (click)="onAction(\'all\')" >\n        <ion-icon name="ios-list-outline"></ion-icon>\n        {{\'ALL\' | translate}}\n      </button>\n    </ion-col>\n  </ion-row>\n  <ion-row>\n    <ion-col >\n      <button ion-button icon-left clear (click)="onAction(\'favorite\')" >\n        <ion-icon name="ios-star-outline"></ion-icon>\n\n        {{\'FAVORITE\' | translate}}\n        \n      </button>\n    </ion-col>\n  </ion-row>\n</ion-grid>\n\n'/*ion-inline-end:"/Volumes/9rachata/Archive/navi-park/src/components/select-find/select-find.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["q" /* ViewController */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* App */]])
    ], SelectFindComponent);
    return SelectFindComponent;
}());

//# sourceMappingURL=select-find.js.map

/***/ }),

/***/ 598:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ZoomAreaProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ZoomAreaProvider = /** @class */ (function () {
    function ZoomAreaProvider() {
        this.SCROLL_STATE = {
            NORMAL: 'scrollNormal',
            COLAPSED: 'scrollColapsed'
        };
        this._onScroll = new __WEBPACK_IMPORTED_MODULE_1_rxjs__["Subject"]();
        this.onScroll$ = this._onScroll.asObservable();
        this._scrollState = new __WEBPACK_IMPORTED_MODULE_1_rxjs__["Subject"]();
        this.scrollState$ = this._scrollState.asObservable();
        this._centerChanged = new __WEBPACK_IMPORTED_MODULE_1_rxjs__["Subject"]();
        this.centerChanged$ = this._centerChanged.asObservable();
    }
    ZoomAreaProvider.prototype.notifyScroll = function (data) {
        this._onScroll.next(data);
    };
    ZoomAreaProvider.prototype.notifyScrollState = function (data) {
        this._scrollState.next(data);
    };
    ZoomAreaProvider.prototype.setCenter = function (x, y) {
        this._centerChanged.next({ x: x, y: y });
    };
    ZoomAreaProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], ZoomAreaProvider);
    return ZoomAreaProvider;
}());

//# sourceMappingURL=zoom-area.js.map

/***/ }),

/***/ 599:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the SettingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SettingPage = /** @class */ (function () {
    function SettingPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SettingPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SettingPage');
    };
    SettingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-setting',template:/*ion-inline-start:"/Volumes/9rachata/Archive/navi-park/src/pages/setting/setting.html"*/'<!--\n  Generated template for the SettingPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>{{ \'SETTING\' | translate }}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Volumes/9rachata/Archive/navi-park/src/pages/setting/setting.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], SettingPage);
    return SettingPage;
}());

//# sourceMappingURL=setting.js.map

/***/ }),

/***/ 600:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(601);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(605);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 605:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export FIREBASE_CONFIG */
/* unused harmony export createTranslateLoader */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pages_general_access_to_parking_general_access_to_parking__ = __webpack_require__(266);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_general_view_map_image_general_view_map_image__ = __webpack_require__(272);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_agree_agree__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_general_zone_confirm_general_zone_confirm__ = __webpack_require__(271);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_search_favorite_search_favorite__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_general_history_general_history__ = __webpack_require__(273);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_general_menu_general_menu__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_general_profile_general_profile__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_general_tab_general_tab__ = __webpack_require__(274);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_news_news__ = __webpack_require__(275);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_general_first_general_first__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_general_view_zone_general_view_zone__ = __webpack_require__(270);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_progress_bar_progress_bar__ = __webpack_require__(1331);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_general_home_general_home__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_register_register__ = __webpack_require__(276);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_login_login__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__angular_platform_browser__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_splash_screen__ = __webpack_require__(595);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_status_bar__ = __webpack_require__(596);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__app_component__ = __webpack_require__(1332);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__providers_auth_auth__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23_angularfire2__ = __webpack_require__(1333);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23_angularfire2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_23_angularfire2__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24_angularfire2_auth__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24_angularfire2_auth___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_24_angularfire2_auth__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25_angularfire2_database__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_25_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26_angularfire2_firestore__ = __webpack_require__(1334);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26_angularfire2_firestore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_26_angularfire2_firestore__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_general_status_parking_general_status_parking__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__components_select_find_select_find__ = __webpack_require__(432);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__providers_zoom_area_zoom_area__ = __webpack_require__(598);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__components_zoom_area_zoom_area__ = __webpack_require__(1341);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__angular_platform_browser_animations__ = __webpack_require__(1342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_search_all_search_all__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__providers_search_data_search_data__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_general_contact_general_contact__ = __webpack_require__(269);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_general_peview_parking_general_peview_parking__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__ionic_native_launch_navigator__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__ionic_native_barcode_scanner__ = __webpack_require__(594);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__angular_common_http__ = __webpack_require__(1344);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__ngx_translate_core__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__ngx_translate_http_loader__ = __webpack_require__(1345);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__ionic_native_in_app_browser__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__ionic_native_screen_orientation__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__pages_setting_setting__ = __webpack_require__(599);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__ionic_native_native_storage__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__ionic_native_date_picker__ = __webpack_require__(433);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__pages_payment_payment__ = __webpack_require__(268);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






































var FIREBASE_CONFIG = {
    apiKey: "AIzaSyDoK7hK5mhl88jRhVMqjG2YCpWs1nvNlig",
    authDomain: "parking-intelligent.firebaseapp.com",
    databaseURL: "https://parking-intelligent.firebaseio.com",
    projectId: "parking-intelligent",
    storageBucket: "parking-intelligent.appspot.com",
    messagingSenderId: "140803614618"
};



function createTranslateLoader(http) {
    return new __WEBPACK_IMPORTED_MODULE_40__ngx_translate_http_loader__["a" /* TranslateHttpLoader */](http, './assets/i18n/', '.json');
}






var ScreenOrientationMock = /** @class */ (function (_super) {
    __extends(ScreenOrientationMock, _super);
    function ScreenOrientationMock() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ScreenOrientationMock.prototype.lock = function (type) {
        return new Promise(function (resolve, reject) {
            resolve("locked");
        });
    };
    return ScreenOrientationMock;
}(__WEBPACK_IMPORTED_MODULE_42__ionic_native_screen_orientation__["a" /* ScreenOrientation */]));
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_17__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_8__pages_general_tab_general_tab__["a" /* GeneralTabPage */],
                __WEBPACK_IMPORTED_MODULE_21__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_15__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_general_home_general_home__["a" /* GeneralHomePage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_general_status_parking_general_status_parking__["a" /* GeneralStatusParkingPage */],
                __WEBPACK_IMPORTED_MODULE_12__components_progress_bar_progress_bar__["a" /* ProgressBarComponent */],
                __WEBPACK_IMPORTED_MODULE_11__pages_general_view_zone_general_view_zone__["a" /* GeneralViewZonePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_general_first_general_first__["a" /* GeneralFirstPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_news_news__["a" /* NewsPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_general_home_general_home__["a" /* GeneralHomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_general_profile_general_profile__["a" /* GeneralProfilePage */],
                __WEBPACK_IMPORTED_MODULE_28__components_select_find_select_find__["a" /* SelectFindComponent */],
                __WEBPACK_IMPORTED_MODULE_30__components_zoom_area_zoom_area__["a" /* ZoomAreaComponent */],
                __WEBPACK_IMPORTED_MODULE_6__pages_general_menu_general_menu__["a" /* GeneralMenuPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_general_history_general_history__["a" /* GeneralHistoryPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_search_all_search_all__["a" /* SearchAllPage */],
                __WEBPACK_IMPORTED_MODULE_4__pages_search_favorite_search_favorite__["a" /* SearchFavoritePage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_general_contact_general_contact__["a" /* GeneralContactPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_general_peview_parking_general_peview_parking__["a" /* GeneralPeviewParkingPage */],
                __WEBPACK_IMPORTED_MODULE_3__pages_general_zone_confirm_general_zone_confirm__["a" /* GeneralZoneConfirmPage */],
                __WEBPACK_IMPORTED_MODULE_2__pages_agree_agree__["a" /* AgreePage */],
                __WEBPACK_IMPORTED_MODULE_43__pages_setting_setting__["a" /* SettingPage */],
                __WEBPACK_IMPORTED_MODULE_1__pages_general_view_map_image_general_view_map_image__["a" /* GeneralViewMapImagePage */],
                __WEBPACK_IMPORTED_MODULE_0__pages_general_access_to_parking_general_access_to_parking__["a" /* GeneralAccessToParkingPage */],
                __WEBPACK_IMPORTED_MODULE_46__pages_payment_payment__["a" /* PaymentPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_16__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_18_ionic_angular__["g" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_21__app_component__["a" /* MyApp */], {
                    scrollPadding: false,
                    scrollAssist: true,
                    autoFocusAssist: true
                }, {
                    links: [
                        { loadChildren: '../pages/agree/agree.module#AgreePageModule', name: 'AgreePage', segment: 'agree', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/general-access-to-parking/general-access-to-parking.module#GeneralAccessToParkingPageModule', name: 'GeneralAccessToParkingPage', segment: 'general-access-to-parking', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/general-contact/general-contact.module#GeneralContactPageModule', name: 'GeneralContactPage', segment: 'general-contact', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/general-first/general-first.module#GeneralFirstPageModule', name: 'GeneralFirstPage', segment: 'general-first', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/general-history/general-history.module#GeneralHistoryPageModule', name: 'GeneralHistoryPage', segment: 'general-history', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/general-home/general-home.module#GeneralHomePageModule', name: 'GeneralHomePage', segment: 'general-home', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/general-menu/general-menu.module#GeneralMenuPageModule', name: 'GeneralMenuPage', segment: 'general-menu', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/general-peview-parking/general-peview-parking.module#GeneralPeviewParkingPageModule', name: 'GeneralPeviewParkingPage', segment: 'general-peview-parking', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/general-profile/general-profile.module#GeneralProfilePageModule', name: 'GeneralProfilePage', segment: 'general-profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/general-status-parking/general-status-parking.module#GeneralStatusParkingPageModule', name: 'GeneralStatusParkingPage', segment: 'general-status-parking', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/general-tab/general-tab.module#GeneralTabPageModule', name: 'GeneralTabPage', segment: 'general-tab', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/general-view-map-image/general-view-map-image.module#GeneralViewMapImagePageModule', name: 'GeneralViewMapImagePage', segment: 'general-view-map-image', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/general-view-zone/general-view-zone.module#GeneralViewZonePageModule', name: 'GeneralViewZonePage', segment: 'general-view-zone', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/general-zone-confirm/general-zone-confirm.module#GeneralZoneConfirmPageModule', name: 'GeneralZoneConfirmPage', segment: 'general-zone-confirm', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/news/news.module#NewsPageModule', name: 'NewsPage', segment: 'news', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/payment/payment.module#PaymentPageModule', name: 'PaymentPage', segment: 'payment', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/search-all/search-all.module#SearchAllPageModule', name: 'SearchAllPage', segment: 'search-all', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/search-favorite/search-favorite.module#SearchFavoritePageModule', name: 'SearchFavoritePage', segment: 'search-favorite', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/setting/setting.module#SettingPageModule', name: 'SettingPage', segment: 'setting', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_38__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_39__ngx_translate_core__["b" /* TranslateModule */].forRoot({
                    loader: {
                        provide: __WEBPACK_IMPORTED_MODULE_39__ngx_translate_core__["a" /* TranslateLoader */],
                        useFactory: createTranslateLoader,
                        deps: [__WEBPACK_IMPORTED_MODULE_38__angular_common_http__["a" /* HttpClient */]]
                    }
                }),
                __WEBPACK_IMPORTED_MODULE_23_angularfire2__["AngularFireModule"].initializeApp(FIREBASE_CONFIG),
                __WEBPACK_IMPORTED_MODULE_25_angularfire2_database__["AngularFireDatabaseModule"],
                __WEBPACK_IMPORTED_MODULE_24_angularfire2_auth__["AngularFireAuthModule"],
                __WEBPACK_IMPORTED_MODULE_31__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_26_angularfire2_firestore__["AngularFirestoreModule"]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_18_ionic_angular__["e" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_8__pages_general_tab_general_tab__["a" /* GeneralTabPage */],
                __WEBPACK_IMPORTED_MODULE_21__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_15__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_general_home_general_home__["a" /* GeneralHomePage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_general_status_parking_general_status_parking__["a" /* GeneralStatusParkingPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_general_view_zone_general_view_zone__["a" /* GeneralViewZonePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_general_first_general_first__["a" /* GeneralFirstPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_news_news__["a" /* NewsPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_general_home_general_home__["a" /* GeneralHomePage */],
                __WEBPACK_IMPORTED_MODULE_46__pages_payment_payment__["a" /* PaymentPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_general_profile_general_profile__["a" /* GeneralProfilePage */],
                __WEBPACK_IMPORTED_MODULE_28__components_select_find_select_find__["a" /* SelectFindComponent */],
                __WEBPACK_IMPORTED_MODULE_30__components_zoom_area_zoom_area__["a" /* ZoomAreaComponent */],
                __WEBPACK_IMPORTED_MODULE_6__pages_general_menu_general_menu__["a" /* GeneralMenuPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_general_history_general_history__["a" /* GeneralHistoryPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_search_all_search_all__["a" /* SearchAllPage */],
                __WEBPACK_IMPORTED_MODULE_4__pages_search_favorite_search_favorite__["a" /* SearchFavoritePage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_general_contact_general_contact__["a" /* GeneralContactPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_general_peview_parking_general_peview_parking__["a" /* GeneralPeviewParkingPage */],
                __WEBPACK_IMPORTED_MODULE_3__pages_general_zone_confirm_general_zone_confirm__["a" /* GeneralZoneConfirmPage */],
                __WEBPACK_IMPORTED_MODULE_2__pages_agree_agree__["a" /* AgreePage */],
                __WEBPACK_IMPORTED_MODULE_43__pages_setting_setting__["a" /* SettingPage */],
                __WEBPACK_IMPORTED_MODULE_1__pages_general_view_map_image_general_view_map_image__["a" /* GeneralViewMapImagePage */],
                __WEBPACK_IMPORTED_MODULE_0__pages_general_access_to_parking_general_access_to_parking__["a" /* GeneralAccessToParkingPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_19__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_17__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_18_ionic_angular__["f" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_22__providers_auth_auth__["a" /* AuthProvider */],
                __WEBPACK_IMPORTED_MODULE_29__providers_zoom_area_zoom_area__["a" /* ZoomAreaProvider */],
                __WEBPACK_IMPORTED_MODULE_33__providers_search_data_search_data__["a" /* SearchDataProvider */],
                __WEBPACK_IMPORTED_MODULE_36__ionic_native_launch_navigator__["a" /* LaunchNavigator */],
                __WEBPACK_IMPORTED_MODULE_44__ionic_native_native_storage__["a" /* NativeStorage */],
                __WEBPACK_IMPORTED_MODULE_37__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
                __WEBPACK_IMPORTED_MODULE_45__ionic_native_date_picker__["a" /* DatePicker */],
                __WEBPACK_IMPORTED_MODULE_41__ionic_native_in_app_browser__["a" /* InAppBrowser */],
                { provide: __WEBPACK_IMPORTED_MODULE_42__ionic_native_screen_orientation__["a" /* ScreenOrientation */], useClass: ScreenOrientationMock }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ })

},[600]);
//# sourceMappingURL=main.js.map